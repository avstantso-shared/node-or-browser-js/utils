import 'jest';
import { Canceler } from '../../../src';

type Inc = {
  (): number;
  counter: number;
};

function Inc(initial = 0): Inc {
  let counter = initial;

  function inc() {
    return ++counter;
  }
  Object.defineProperties(inc, {
    counter: {
      get() {
        return counter;
      },
    },
  });

  return inc as Inc;
}

function Checker(canceler: Canceler, inc: Inc) {
  return (stage: boolean, expectedIncCounter = 1) => {
    expect(canceler.isCanceled).toBe(stage);

    const eInc = expect(canceler.safe(inc)());
    stage ? eInc.toBeUndefined() : eInc.toBe(inc.counter);

    expect(inc.counter).toBe(expectedIncCounter);
  };
}

async function CheckCanceler(
  canceler: Canceler,
  inc = Inc(),
  check = Checker(canceler, inc)
) {
  check(false);

  const p = new Promise((resolve) => setTimeout(resolve, 500)).then(() => {
    check(true);
  });

  canceler.cancel();

  check(true);

  return p;
}

describe(`Canceler`, () => {
  it('is', () => {
    expect(Canceler.is({})).toBeFalsy();
    expect(Canceler.is.Context({})).toBeFalsy();

    const c = Canceler();

    expect(Canceler.is(c)).toBeTruthy();
    expect(Canceler.is.Context(c)).toBeTruthy();

    expect(Canceler.is(c.context)).toBeFalsy();
    expect(Canceler.is.Context(c.context)).toBeTruthy();
  });

  it('Canceler', async () => CheckCanceler(Canceler()));

  it('Custom', async () => {
    let a = 'on';
    const c = Canceler.Custom(
      () => a !== 'on',
      () => (a = 'off')
    );

    const inc = Inc();
    const check = Checker(c, inc);

    await CheckCanceler(c, inc, check);

    a = 'on';
    check(false, 2);
  });

  it('Merge', async () => {
    const [c1, c2, c3] = Array.from({ length: 3 }, Canceler);
    const cm = Canceler.Merge(c1, c2, c3);

    const inc = Inc();
    const check = Checker(cm, inc);

    check(false);

    c2.cancel();

    check(true);

    expect(c1.isCanceled).toBe(false);
    expect(c2.isCanceled).toBe(true);
    expect(c3.isCanceled).toBe(false);

    cm.cancel();

    expect(c1.isCanceled).toBe(true);
    expect(c2.isCanceled).toBe(true);
    expect(c3.isCanceled).toBe(true);

    expect(Canceler.is.Context(cm)).toBeTruthy();
    expect(Canceler.is(cm)).toBeTruthy();

    const cmc = Canceler.Merge(c1.context, c2.context, c3.context);
    expect(Canceler.is.Context(cmc)).toBeTruthy();
    expect(Canceler.is(cmc)).toBeFalsy();
  });

  it('Derive', async () => {
    const c1 = Canceler();
    const c2 = Canceler.Derive(c1)();
    const c3 = Canceler.Derive(c2)();
    const cd = Canceler.Derive(c3)();

    const inc = Inc();
    const check = Checker(cd, inc);

    check(false);

    c2.cancel();

    check(true);

    expect(c1.isCanceled).toBe(false);
    expect(c2.isCanceled).toBe(true);
    expect(c3.isCanceled).toBe(true);
    expect(cd.isCanceled).toBe(true);
  });

  it('Handler', async () => {
    const c = Canceler();

    const p = new Promise((resolve, reject) => {
      const t = setTimeout(resolve, 500);
      c.subscribe(() => {
        clearTimeout(t);
        reject();
      });
    }).then(
      () => fail('Must be canceled by handler'),
      () => expect(true).toBeTruthy()
    );

    c.cancel();

    return p;
  });
});
