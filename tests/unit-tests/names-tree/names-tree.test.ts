import 'jest';
import { NamesTree } from '../../../src';

import { form, Urls, i18nFrag } from './data';

describe('NamesTree', () => {
  it('simple-const', () => {
    const formTree = NamesTree.Names(form);

    expect(`${formTree.form}`).toBe('form');
    expect(`${formTree.label}`).toBe('label');
    expect(`${formTree.placeholder}`).toBe('placeholder');

    const formDict = formTree.Name;
    expect(formDict).toStrictEqual({
      form: 'form',
      label: 'label',
      placeholder: 'placeholder',
    });
  });

  it('i18n-keys', () => {
    const i18nTree = NamesTree.I18ns(i18nFrag);

    expect(`${i18nTree.schemas}`).toBe('schemas');

    expect(`${i18nTree.schemas.login}`).toBe('schemas.login');

    expect(`${i18nTree.schemas.login.loginOrEmail}`).toBe(
      'schemas.login.loginOrEmail'
    );

    expect(`${i18nTree.schemas.login.loginOrEmail.required}`).toBe(
      'schemas.login.loginOrEmail.required'
    );

    expect(`${i18nTree.schemas.registration.captcha.required}`).toBe(
      'schemas.registration.captcha.required'
    );
  });

  it('urls-splitted', () => {
    const UrlsTree = NamesTree.Urls(Urls);

    const [NAMES, PATHS, URLS] = UrlsTree.splitted;

    expect(`${UrlsTree.profile.settings}`).toBe('settings');
    expect(`${UrlsTree.profile}`).toBe('profile');

    expect(`${NAMES.profile.settings}`).toBe('settings');
    expect(NAMES.profile.settings).toBe('settings');
    expect(`${NAMES.profile}`).toBe('profile');

    expect(`${UrlsTree.Name.profile.settings}`).toBe('settings');
    expect(UrlsTree.Name.profile.settings).toBe('settings');
    expect(`${UrlsTree.Name.profile}`).toBe('profile');

    expect(`${PATHS.profile.settings}`).toBe('profile/settings');
    expect(PATHS.profile.settings).toBe('profile/settings');
    expect(`${PATHS.profile}`).toBe('profile');

    expect(`${UrlsTree.Path.profile.settings}`).toBe('profile/settings');
    expect(UrlsTree.Path.profile.settings).toBe('profile/settings');
    expect(`${UrlsTree.Path.profile}`).toBe('profile');

    expect(`${URLS.profile.settings}`).toBe('/profile/settings');
    expect(URLS.profile.settings).toBe('/profile/settings');
    expect(`${URLS.profile}`).toBe('/profile');

    expect(`${UrlsTree.Url.profile.settings}`).toBe('/profile/settings');
    expect(UrlsTree.Url.profile.settings).toBe('/profile/settings');
    expect(`${UrlsTree.Url.profile}`).toBe('/profile');
  });

  it('urls-merge', () => {
    const UrlsTree = NamesTree.Urls(Urls);

    function profile() {
      return 123;
    }

    const merged = UrlsTree.merge({ profile });

    expect(merged.profile()).toBe(123);

    expect(merged.profile.settings._path).toBe('profile/settings');
    expect(merged.Path.profile.settings).toBe('profile/settings');
  });

  it(`options-prefix`, () => {
    const formDict = NamesTree.Names(form, { prefix: 'xxx-' }).Name;
    expect(formDict).toStrictEqual({
      form: 'xxx-form',
      label: 'xxx-label',
      placeholder: 'xxx-placeholder',
    });
  });
});
