export { form } from './form';
export { Urls } from './urls';

import i18nFrag from './i18nLocalizationFragment.json';

export { i18nFrag };
