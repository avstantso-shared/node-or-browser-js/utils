const s = '';

export const Urls = {
  profile: {
    login: s,
    logout: s,
    settings: s,
    registration: s,
    reset: s,
  },
  admin: {
    roles: s,
    users: s,
  },
};
