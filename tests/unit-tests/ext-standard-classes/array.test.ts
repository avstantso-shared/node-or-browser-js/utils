import 'jest';
import '../../../src';

describe('ReadonlyArray', () => {
  const empty: ReadonlyArray<any> = [];
  const numbers: ReadonlyArray<number> = [0, 1, 2];
  const strings: ReadonlyArray<string> = ['', 'A', '', 'B'];

  it('pack', () => {
    expect(empty.pack()).toStrictEqual([]);

    expect(numbers.pack()).toStrictEqual([1, 2]);

    expect(strings.pack()).toStrictEqual(['A', 'B']);
  });

  it('peek', () => {
    expect(empty.peek()).toBeUndefined();

    expect(numbers.peek()).toBe(2);

    expect(strings.peek()).toBe('B');
  });
});

describe('Array', () => {
  it('pack', () => {
    expect([].pack()).toStrictEqual([]);

    expect([0, 1, 2].pack()).toStrictEqual([1, 2]);

    expect(['', 'A', '', 'B'].pack()).toStrictEqual(['A', 'B']);
  });

  it('peek', () => {
    expect([].peek()).toBeUndefined();

    expect([0, 1, 2].peek()).toBe(2);

    expect(['', 'A', '', 'B'].peek()).toBe('B');
  });
});
