import 'jest';
import '../../../src';

describe('String', () => {
  it('toCapitalized', () => {
    expect(''.toCapitalized()).toBe('');

    expect('TEXT'.toCapitalized()).toBe('TEXT');

    expect('text'.toCapitalized()).toBe('Text');

    expect('tEXT'.toCapitalized()).toBe('TEXT');
  });
});
