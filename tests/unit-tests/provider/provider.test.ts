import 'jest';
import { Equality, Provider, TS } from '../../../src';

const dName = TS.Type.Arr('name', 'string');
const dAge = TS.Type.Arr('age', 'number');
const dReg = TS.Type.Arr('reg', 'Date');
const dToken = TS.Type.Arr('token', 'Buffer');

type DName = typeof dName;
type DAge = typeof dAge;
type DReg = typeof dReg;
type DToken = typeof dToken;

type PName = Provider<DName>;
type PAge = Provider<DAge>;
type PReg = Provider<DReg>;
type PToken = Provider<DToken>;
type D = PName & PAge & PReg & PToken;

type CName = Provider.Compare.Make<DName>;
type CAge = Provider.Compare.Make<DAge>;
type CReg = Provider.Compare.Make<DReg>;
type CToken = Provider.Compare.Make<DToken>;

type EFName = Equality.From<CName>;
type EFAge = Equality.From<CAge>;
type EFReg = Equality.From<CReg>;
type EFToken = Equality.From<CToken>;

const testData: [D, D, D] = [
  {
    name: 'Vasia',
    age: 15,
    reg: new Date(`12.11.2000`),
    token: Buffer.from('1'),
  },
  {
    name: 'Ira',
    age: 19,
    reg: new Date(`12.10.2000`),
    token: Buffer.from('2'),
  },
  {
    name: 'Masha',
    age: 52,
    reg: new Date(`12.12.2000`),
    token: Buffer.from('3'),
  },
];

describe(`Provider`, () => {
  it('Provider', () => {
    const id = 16;

    const p: Provider<'id', 'number'> = { id };

    expect(p.id).toBe(id);
  });

  it('Is', () => {
    const id = 16;
    const now = new Date();
    const buf = Buffer.from('abc');

    const pId: Provider<'id', 'number'> = { id };
    const pNow: Provider<'now', 'Date'> = { now: new Date(now) };
    const pBuf: Provider<'buf', 'Buffer'> = { buf: Buffer.from(buf) };

    const isId = Provider.Is('id', 'number');
    const isNow = Provider.Is('now', 'Date');
    const isBuf = Provider.Is('buf', 'Buffer');

    expect(isId(pId)).toBe(true);
    expect(isId({})).toBe(false);

    expect(isNow(pNow)).toBe(true);
    expect(isNow({})).toBe(false);

    expect(isBuf(pBuf)).toBe(true);
    expect(isBuf({})).toBe(false);
  });

  it('Extract', () => {
    const e = Provider.Extract('module', 'string');

    const v = 'abc';
    const m = { module: v };

    const m1 = e(m);
    const m2 = Provider.Extract('module')(m);

    expect(m1).toBe(v);
    expect(m2).toBe(v);
  });

  it('Compare', () => {
    const [d1, d2, d3] = testData;

    const cName: CName = Provider.Compare(dName);
    const cAge: CAge = Provider.Compare(dAge);
    const cReg: CReg = Provider.Compare(dReg);
    const cToken: CToken = Provider.Compare(dToken);

    expect([d1, d2, d3].sort(cName)).toStrictEqual([d2, d3, d1]);
    expect([d3, d2, d1].sort(cAge)).toStrictEqual([d1, d2, d3]);
    expect([d1, d2, d3].sort(cReg)).toStrictEqual([d2, d1, d3]);
    expect([d3, d2, d1].sort(cToken)).toStrictEqual([d1, d2, d3]);

    expect([null, undefined, d1, NaN].sort(cAge)).toStrictEqual([
      d1,
      null,
      NaN,
      undefined,
    ]);
  });

  describe(`Equality`, () => {
    it('Field', () => {
      const [d1, d2] = testData;

      {
        const eqfName: EFName = Equality.From(Provider.Compare(dName));

        expect(eqfName(d1, d1)).toBe(true);

        expect(eqfName(d1.name, d1.name)).toBe(true);
        expect(eqfName(d1, d1.name)).toBe(true);
        expect(eqfName(d1.name, d1)).toBe(true);

        expect(eqfName(d1.name, d2.name)).toBe(false);
        expect(eqfName(d1, d2.name)).toBe(false);
        expect(eqfName(d1.name, d2)).toBe(false);
      }

      {
        const eqfAge: EFAge = Equality.From(Provider.Compare(dAge));

        expect(eqfAge(d1, d1)).toBe(true);

        expect(eqfAge(d1.age, d1.age)).toBe(true);
        expect(eqfAge(d1, d1.age)).toBe(true);
        expect(eqfAge(d1.age, d1)).toBe(true);

        expect(eqfAge(d1.age, d2.age)).toBe(false);
        expect(eqfAge(d1, d2.age)).toBe(false);
        expect(eqfAge(d1.age, d2)).toBe(false);
      }

      {
        const eqfReg: EFReg = Equality.From(Provider.Compare(dReg));

        expect(eqfReg(d1, d1)).toBe(true);

        expect(eqfReg(d1.reg, d1.reg)).toBe(true);
        expect(eqfReg(d1, d1.reg)).toBe(true);
        expect(eqfReg(d1.reg, d1)).toBe(true);

        expect(eqfReg(d1.reg, d2.reg)).toBe(false);
        expect(eqfReg(d1, d2.reg)).toBe(false);
        expect(eqfReg(d1.reg, d2)).toBe(false);
      }

      {
        const eqfToken: EFToken = Equality.From(Provider.Compare(dToken));

        expect(eqfToken(d1, d1)).toBe(true);

        expect(eqfToken(d1.token, d1.token)).toBe(true);
        expect(eqfToken(d1, d1.token)).toBe(true);
        expect(eqfToken(d1.token, d1)).toBe(true);

        expect(eqfToken(d1.token, d2.token)).toBe(false);
        expect(eqfToken(d1, d2.token)).toBe(false);
        expect(eqfToken(d1.token, d2)).toBe(false);
      }
    });
  });
});
