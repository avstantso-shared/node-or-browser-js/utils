import 'jest';
import { Cases } from '../../../src';

describe(`Cases`, () => {
  const source = 'Oh! Jingle bells, jingle bells Jingle all the way';

  const meta: Record<Cases.Key, string> = {
    camel: 'ohJingleBellsJingleBellsJingleAllTheWay',
    pascal: 'OhJingleBellsJingleBellsJingleAllTheWay',
    snake: 'oh_jingle_bells_jingle_bells_jingle_all_the_way',
    kebab: 'oh-jingle-bells-jingle-bells-jingle-all-the-way',
  };

  Object.entries(meta).forEach(([name, expected]) =>
    it(name, () => {
      expect(Cases[name as Cases.Key](source)).toBe(expected);
    })
  );
});
