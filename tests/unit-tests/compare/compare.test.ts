import 'jest';
import '../../../src';
import Compare = AVStantso.Compare;

describe(`Compare`, () => {
  it(`Combine`, () => {
    function nc(a: { name: string }, b: { name: string }): number {
      return a.name.localeCompare(b.name);
    }

    function mc(a: { module: string }, b: { module: string }): number {
      return a.module.localeCompare(b.module);
    }

    const nmc = Compare.Combine(
      Compare.Extend<{ name: string }>(nc),
      Compare.Extend<{ module: string }>(mc)
    );

    expect(nmc({ name: 'A', module: 'x' }, { name: 'A', module: 'b' })).toBe(1);
  });

  it(`Structures`, () => {
    expect(
      Compare.Structures({ values: [1, 3, 4] }, { values: [1, 3, 4] })
    ).toBe(0);

    expect(
      Compare.Structures({ values: [1, 3, 4] }, { values: [1, 2, 3] })
    ).toBe(1);
    expect(Compare.Structures([1], [1, 2, 3])).toBe(-1);
    expect(Compare.Structures({ values: [1] }, { values: [1, 2, 3] })).toBe(-1);

    expect(Compare.Structures({ main: ['USER'] }, { main: ['ROLE'] })).toBe(1);
  });
});
