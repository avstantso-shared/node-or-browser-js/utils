import 'jest';
import { Enum, Rus } from '../../../src';

describe('Rus', () => {
  describe(Rus.Case._name, () => {
    it('values-to-json', () => {
      expect(JSON.stringify(Rus.Case._values)).toStrictEqual(
        JSON.stringify(Rus.Case._._values.map((v) => Rus.Case.toName(v)))
      );
    });

    it('toString', () => {
      expect(`${Rus.Case.genitive}`).toBe('genitive');
      expect(Rus.Case.genitive.name).toBe('genitive');
      expect(Rus.Case.toName(Rus.Case.genitive)).toBe('genitive');
    });

    it('valueOf', () => {
      expect(+Rus.Case.genitive).toBe(2);
    });

    it('fromValue', () => {
      expect(Rus.Case.fromValue(5)).toBe(Rus.Case.instrumental);
      expect(Rus.Case.fromValue(Rus.Case._.instrumental)).toBe(
        Rus.Case.instrumental
      );
      expect(Rus.Case.fromValue(19, Rus.Case.dative)).toBe(Rus.Case.dative);
      expect(Rus.Case.fromValue(36)).toBeUndefined();
      expect(Rus.Case.fromValue(24, null)).toBeNull();
    });

    it('fromString', () => {
      expect(Rus.Case.fromString('instrumental')).toBe(Rus.Case.instrumental);
      expect(Rus.Case.fromString('1', Rus.Case.dative)).toBe(Rus.Case.dative);
      expect(Rus.Case.fromString('2')).toBeUndefined();
      expect(Rus.Case.fromString('3', null)).toBeNull();

      expect(Rus.Case.fromString('Instrumental')).toBeUndefined();
      expect(Rus.Case.fromString.capitalized('InstrumentaL')).toBeUndefined();
      expect(Rus.Case.fromString.capitalized('Instrumental')).toBe(
        Rus.Case.instrumental
      );
      expect(Rus.Case.fromString.caseInsensitive('InstruMentaL')).toBe(
        Rus.Case.instrumental
      );
    });

    it('switch', () => {
      const dative: Rus.Case = Rus.Case.dative;

      expect(
        Rus.Case.switch(dative, {
          genitive: 'A',
          dative: 'B',
          accusative: 'C',
          instrumental: 'D',
        })
      ).toBe('B');

      expect(
        Rus.Case.switch(dative, {
          genitive: 'A',

          accusative: 'C',
          instrumental: 'D',
        })
      ).toBe(undefined);

      expect(
        Rus.Case.switch(dative, {
          genitive: 'A',

          accusative: 'C',
          instrumental: 'D',

          default: 'X',
        })
      ).toBe('X');

      expect(
        Rus.Case.switch(dative, {
          genitive: 'A',
          'dative|accusative': 'BC',
          instrumental: 'D',
        })
      ).toBe('BC');

      expect(
        Rus.Case.switch(dative, {
          genitive: 'A',
          'dative|accusative|instrumental': 'BCD',
        })
      ).toBe('BCD');

      expect(
        Rus.Case.switch(dative, {
          genitive: 'A',
          'accusative|dative|instrumental': 'CBD',
        })
      ).toBe('CBD');

      expect(
        Rus.Case.switch(dative, {
          genitive: 'A',
          'accusative|instrumental|dative': 'BDC',
        })
      ).toBe('BDC');

      expect(
        dative.switch({
          genitive: 'A',
          'accusative|instrumental|dative': 'BDC',
        })
      ).toBe('BDC');
    });

    it('mapping', () => {
      const template = {
        genitive: (x: number, y: number, n: string) => `${x}${y}${n}`,

        accusative: (a: string) => !!a,

        instrumental: (b: boolean, o: { g: number; m: string[] }) =>
          b ? o.g.toString() : o.m.join(),

        dative: (h: string, u?: string) => `${u || '-'}${h}`,
      };

      const mapping = Rus.Case.Mapping(template);

      const g = mapping(Rus.Case.genitive);
      expect(g(1, 2, '!')).toBe('12!');

      const i = mapping(Rus.Case.instrumental);
      expect(i(true, { g: 14, m: ['A', 'B', 'C'] })).toBe('14');
      expect(i(false, { g: 18, m: ['Ax', 'Bx', 'Cx'] })).toBe('Ax,Bx,Cx');

      const accusative: any = Rus.Case.accusative;
      let c: Rus.Case = accusative;
      const a = mapping(c, Rus.Case.genitive || Rus.Case.instrumental);
      expect(a('1')).toBeTruthy();
      expect(a('')).toBeFalsy();

      const dative: any = Rus.Case.dative;
      c = dative;
      const d = mapping(c, Rus.Case.genitive || Rus.Case.instrumental);
      expect(d('X')).toBe('-X');
      expect(d('X', 'Y')).toBe('YX');
    });

    it('extender', () => {
      const e = Rus.Case._extender(
        (c: Rus.Case) => c,
        (v) => v
      );
      expect(e.genitive).toBe(Rus.Case.genitive);

      const E = Rus.Case._extender.Capitalized(
        (c: Rus.Case) => c,
        (v) => v
      );
      expect(E.Genitive).toBe(Rus.Case.genitive);
    });

    it('or', () => {
      expect(Rus.Case.nominative.or(Rus.Case.dative)).toBe(Rus.Case.dative);
      expect(Rus.Case.nominative.or.genitive()).toBe(Rus.Case.dative);
      expect(Rus.Case.nominative.or.genitive.or(Rus.Case.dative)).toBe(
        Rus.Case.dative
      );

      try {
        Rus.Case.nominative.or.genitive.or(Rus.Case.prepositional);
        fail();
      } catch (e) {
        expect(e.message).toContain(`Use "combinable:true"`);
      }
    });
  });

  describe(Rus.Number._name, () => {
    it('values-to-json', () => {
      expect(JSON.stringify(Rus.Number._values)).toStrictEqual(
        JSON.stringify(Rus.Number._._values.map((v) => Rus.Number.toName(v)))
      );
    });

    it('toString', () => {
      expect(`${Rus.Number.dual}`).toBe('dual');
      expect(Rus.Number.dual.name).toBe('dual');
      expect(Rus.Number.toName(Rus.Number.dual)).toBe('dual');
    });

    it('valueOf', () => {
      expect(+Rus.Number.dual).toBe(2);
    });

    it('fromValue', () => {
      expect(Rus.Number.fromValue(3)).toBe(Rus.Number.plural);
      expect(Rus.Number.fromValue(Rus.Number._.plural)).toBe(Rus.Number.plural);
      expect(Rus.Number.fromValue(19, Rus.Number.dual)).toBe(Rus.Number.dual);
      expect(Rus.Number.fromValue(36)).toBeUndefined();
      expect(Rus.Number.fromValue(24, null)).toBeNull();
    });

    it('fromString', () => {
      expect(Rus.Number.fromString('plural')).toBe(Rus.Number.plural);
      expect(Rus.Number.fromString('1', Rus.Number.dual)).toBe(Rus.Number.dual);
      expect(Rus.Number.fromString('2')).toBeUndefined();
      expect(Rus.Number.fromString('3', null)).toBeNull();
    });

    it('switch', () => {
      const { plural } = Rus.Number;

      expect(
        Rus.Number.switch(plural, {
          singular: 'Z',
          dual: 'Y',
          plural: 'X',
        })
      ).toBe('X');

      const f: () => number = plural.switch({
        singular: () => 18,
        dual: () => 19,
        plural: () => 20,
      });

      expect(f()).toBe(20);
    });

    it('withSubstantive', () => {
      const ws1 = Rus.Number.withSubstantive('день', 'дня', 'дней');
      const ws2 = Rus.Number.withSubstantive({
        singular: 'день',
        dual: 'дня',
        plural: 'дней',
      });

      const singular = [1, 21, 1051];
      const dual = [2, 3, 4, 22, 23, 24, 72, 83, 94];
      const plural = [5, 9, 10, 11, 12, 20, 25, 28, 3030];

      function testEnum(ws: typeof ws1, s: string, values: number[]) {
        values.forEach((n) => expect(`${n} ${ws(n)}`).toBe(`${n} ${s}`));
      }

      [ws1, ws2].forEach((ws) => {
        testEnum(ws, 'день', singular);
        testEnum(ws, 'дня', dual);
        testEnum(ws, 'дней', plural);
      });
    });
  });
});
