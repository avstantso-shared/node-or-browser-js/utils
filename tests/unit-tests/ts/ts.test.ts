import { TS } from '../../../src';

describe(`TS`, () => {
  describe(`Type`, () => {
    it('Definer', () => {
      function checkOne<L extends TS.Type.Literal>(l: L) {
        expect(l).toBe(TS.Type.Literal.string);
      }

      function checkArr<A extends TS.Type.Literal[]>(ll: A) {
        expect(ll).toStrictEqual([
          TS.Type.Literal.Buffer,
          TS.Type.Literal.number,
          TS.Type.Literal.symbol,
        ]);
      }

      const D = TS.Type.Definer<TS.Type.Literal>();

      const s = D(TS.Type.Literal.string);

      checkOne(s);

      const a1 = D.Arr(TS.Type.Literal.Buffer)(TS.Type.Literal.number)(
        TS.Type.Literal.symbol
      ).R;

      const a2 = D.Arr(
        TS.Type.Literal.Buffer,
        TS.Type.Literal.number,
        TS.Type.Literal.symbol
      );

      checkArr(a1);
    });

    it('flat', () => {
      const { flat } = TS.Type;

      const t = {
        x: { a: 'ABC', b: 15 },
        y: { c: { d: true } },
        z: BigInt(18),
      };

      const f0 = flat(t, 0);
      type f0 = AVStantso.CheckType<
        typeof f0,
        {
          x: {
            a: string;
            b: number;
          };
          y: {
            c: {
              d: boolean;
            };
          };
          z: bigint;
        }
      >;
      expect(flat(t, 0)).toStrictEqual(t);

      const f1 = flat(t, 1);
      type f1 = AVStantso.CheckType<
        typeof f1,
        {
          a: string;
          b: number;
          c: {
            d: boolean;
          };
          z: bigint;
        }
      >;
      expect(f1).toStrictEqual({
        a: 'ABC',
        b: 15,
        c: { d: true },
        z: BigInt(18),
      });

      const f2 = flat(t, 2);
      type f2 = AVStantso.CheckType<
        typeof f2,
        {
          a: string;
          b: number;
          d: boolean;
          z: bigint;
        }
      >;
      expect(f2).toStrictEqual({
        a: 'ABC',
        b: 15,
        d: true,
        z: BigInt(18),
      });

      const f4 = flat(t, 4);
      type f4 = AVStantso.CheckType<typeof f4, f2>;
      expect(f4).toStrictEqual({
        a: 'ABC',
        b: 15,
        d: true,
        z: BigInt(18),
      });
    });
  });
});
