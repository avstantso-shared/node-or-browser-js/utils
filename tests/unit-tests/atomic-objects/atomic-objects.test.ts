import 'jest';
import { ValueWrap } from '../../../src';
import AO = AVStantso.AtomicObjects;

describe(`AtomicObjects`, () => {
  it('is', () => {
    expect(AO.is({})).toBe(false);
    expect(AO.is([])).toBe(false);

    expect(AO.is(new Date())).toBe(true);
    expect(AO.is(Buffer.from('123'))).toBe(true);
    expect(AO.is(new ValueWrap(1))).toBe(true);
  });
});
