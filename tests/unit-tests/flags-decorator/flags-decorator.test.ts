import 'jest';
import { FlagsDecorator } from '../../../src';

const propsDef = ['a', 'b', 'c'];

type E = { a?: boolean; b?: boolean; c?: boolean };
type EF = E & { f: number };
type EFlags = E & { flags: number };

describe(`FlagsDecorator`, () => {
  function expectClear(x: E) {
    expect(x.a).toBeUndefined();
    expect(x.b).toBeUndefined();
    expect(x.c).toBeUndefined();
  }

  function expectABC(x: E, a: boolean, b: boolean, c: boolean) {
    expect(x.a).toBe(a);
    expect(x.b).toBe(b);
    expect(x.c).toBe(c);
  }

  function expectSetError<Prop extends keyof E>(
    x: E,
    key: Prop,
    value?: boolean
  ) {
    try {
      x[key] = value;

      fail();
    } catch (e) {
      expect(`${e}`).toBe(
        `TypeError: Cannot set property ${key} of #<Object> which has only a getter`
      );
    }
  }

  describe(`minimal`, () =>
    Object.entries({
      minimal: FlagsDecorator(propsDef),
      'readable-true': FlagsDecorator(propsDef, { readable: true }),
      'opt-field-flags': FlagsDecorator(propsDef, { field: 'flags' }),
      'field-flags': FlagsDecorator(propsDef, 'flags'),
    }).forEach(([kind, { expand, omit }]) =>
      describe(kind, () => {
        it(`read`, () => {
          const x: EFlags = { flags: 6 };

          expectClear(x);

          expand(x);

          expectABC(x, false, true, true);

          const { a, b, c } = x;
          const y = { ...x, a, b, c };

          expectABC(y, false, true, true);

          omit(y);

          expectClear(y);
        });

        it(`write`, () => {
          const x: EFlags = { flags: 6 };

          expand(x);

          expectSetError(x, 'a');
        });
      })
    ));

  describe(`seporate-flags`, () =>
    Object.entries({
      param: FlagsDecorator(propsDef, 3),
      option: FlagsDecorator(propsDef, { flags: 3 }),
    }).forEach(([kind, { expand, omit }]) =>
      describe(kind, () => {
        it(`read`, () => {
          const x: E = {};

          expectClear(x);

          expand(x);

          expectABC(x, true, true, false);

          const { a, b, c } = x;
          const y = { ...x, a, b, c };

          expectABC(y, true, true, false);

          omit(y);

          expectClear(y);
        });

        it(`write`, () => {
          const x: E = {};
          expand(x);

          expectSetError(x, 'a');
        });
      })
    ));

  describe(`custom-field`, () =>
    Object.entries({
      param: FlagsDecorator(propsDef, 'f'),
      option: FlagsDecorator(propsDef, { field: 'f' }),
    }).forEach(([kind, { expand, omit }]) =>
      describe(kind, () => {
        it(`read`, () => {
          const x: EF = { f: 6 };

          expectClear(x);

          expand(x);

          expectABC(x, false, true, true);

          const { a, b, c } = x;
          const y = { ...x, a, b, c };

          expectABC(y, false, true, true);

          omit(y);

          expectClear(y);
        });

        it(`write`, () => {
          const x: EF = { f: 6 };

          expand(x);

          expectSetError(x, 'a');
        });
      })
    ));

  describe(`custom-read`, () => {
    const { expand, omit } = FlagsDecorator(propsDef, {
      readable: () => 1 + 4,
    });

    it(`read`, () => {
      const x: E = {};

      expectClear(x);

      expand(x);

      expectABC(x, true, false, true);

      const { a, b, c } = x;
      const y = { ...x, a, b, c };

      expectABC(y, true, false, true);

      omit(y);

      expectClear(y);
    });

    it(`write`, () => {
      const x: E = {};
      expand(x);

      expectSetError(x, 'a');
    });
  });

  describe(`custom-rw`, () => {
    let flags = 1 + 2 + 4;

    const { expand, omit } = FlagsDecorator(propsDef, {
      readable: () => flags,
      writable: (f) => (flags = f),
    });

    it(`read`, () => {
      const x: E = {};

      expectClear(x);

      expand(x);

      expectABC(x, true, true, true);

      const { a, b, c } = x;
      const y = { ...x, a, b, c };

      expectABC(y, true, true, true);

      omit(y);

      expectClear(y);
    });

    it(`write`, () => {
      const x: E = {};
      expand(x);

      x.a = false;

      expect(flags).toBe(6);
    });
  });

  describe(`simple-rw`, () => {
    const { expand, omit } = FlagsDecorator(propsDef, {
      writable: true,
    });

    it(`read`, () => {
      const x: EFlags = { flags: 6 };

      expectClear(x);

      expand(x);

      expectABC(x, false, true, true);

      const { a, b, c } = x;
      const y = { ...x, a, b, c };

      expectABC(y, false, true, true);

      omit(y);

      expectClear(y);
    });

    it(`write`, () => {
      const x: EFlags = { flags: 6 };
      expand(x);

      x.a = false;
      x.b = false;
      x.c = false;

      expect(x.flags).toBe(0);
    });
  });
});
