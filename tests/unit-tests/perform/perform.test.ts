import 'jest';
import { Perform } from '../../../src';

describe(`Perform`, () => {
  it('Perform', () => {
    expect(Perform(1)).toBe(1);
    expect(Perform(() => 2)).toBe(2);
  });
});
