import 'jest';
import { AsyncTasks } from '../../../src';

describe(`AsyncTasks`, () => {
  const Delay = <T = unknown>(msec: number, value?: T) =>
    new Promise((resolve) => setTimeout(() => resolve(value), msec));

  it('add', async () => {
    const asyncTasks = AsyncTasks();

    asyncTasks.add(Delay(300));
    expect(asyncTasks.length).toBe(1);

    await asyncTasks.end();

    expect(asyncTasks.length).toBe(0);
  });

  it('initial', async () => {
    const asyncTasks = AsyncTasks(Delay(150), Delay(200));

    expect(asyncTasks.length).toBe(2);

    await asyncTasks.end();

    expect(asyncTasks.length).toBe(0);
  });

  it('manual-await', async () => {
    const asyncTasks = AsyncTasks();

    await asyncTasks.add(Delay(300));
    expect(asyncTasks.length).toBe(0);

    await asyncTasks.end();

    expect(asyncTasks.length).toBe(0);
  });
});
