import { Generics } from './generics';
import { Switch as AbstractSwitch } from './switch';
import { TS } from './ts';
import { JS } from './js';
import { Extend } from './extend';
import { ValueWrap } from './value-wrap';
import { Perform } from './perform';

/**
 * @summary Enum value wrapper
 */
export class EnumWrap<
  TTypeMap extends Enum.TypeMap = Enum.TypeMap,
  TValue extends TTypeMap['Value'] = TTypeMap['Value']
> extends ValueWrap<TValue> {
  private _controller: Enum.Controller<TTypeMap>;
  private _name: string;

  constructor(value: TValue) {
    super(value);
    Object.setPrototypeOf(this, EnumWrap.prototype);
  }

  protected get controller() {
    return this._controller;
  }

  protected initialize(controller: Perform<Enum.Controller<TTypeMap>>) {
    const ctrl = Perform(controller);
    this._controller = ctrl;

    const { wrapClass } = ctrl;

    if (wrapClass && !ctrl.isWrap(this))
      throw JS.set.raw(
        new Error(
          [
            `Enum "${ctrl._name}": Invalid wrapClass.`,
            `Perhaps you forgot override constructor and add "Object.setPrototypeOf(this, <YourClass>.prototype);" after "super(value);"`,
          ].join('\r\n')
        ),
        'context',
        { wrapClass }
      );

    this._name = ctrl.toName(this.value);
  }

  public get debugLabel() {
    return `${super.debugLabel}(${this.controller._name})`;
  }

  protected copy(): ValueWrap {
    const c = new EnumWrap<TTypeMap, TValue>(this.value);
    c.initialize(this._controller);
    return c;
  }

  public toString() {
    return this.name;
  }

  public toJSON() {
    return this.controller.toJSONAsValue ? this.value : this.name;
  }

  public get name(): string {
    return this._name;
  }

  public get key(): TTypeMap['Key'] {
    return this.name;
  }

  public is(...values: [TTypeMap['Value'], ...TTypeMap['Value'][]]) {
    return this.controller.is(this.value, ...values);
  }

  public get nameIs(): Enum.Value.Wrap.NameIs {
    const thisName = this.name;

    function f(...names: [string, ...string[]]) {
      return names.some(
        (n) => thisName.toLowerCase() === (n || '').toLowerCase()
      );
    }
    f.caseSensitive = (...names: [string, ...string[]]) =>
      names.some((n) => thisName === n);

    return f;
  }

  public get or(): Enum.Value.Wrap.Ex.Or<TTypeMap> {
    const controller = this.controller;

    function Or(...prevWraps: TTypeMap['Wrap'][]) {
      function or(...values: Enum.Value.Union<TTypeMap>[]): TTypeMap['Wrap'] {
        const wrapsSet = new Set([...prevWraps]);
        for (let i = 0; i < values.length; i++) {
          const v = values[i];

          if (undefined === v) return undefined;
          else if (null === v) return null;
          else wrapsSet.add(controller.fromValue(v));
        }

        let rValue = 0;
        const names = new Set<string>();
        wrapsSet.forEach((w) => {
          rValue |= +w;
          w.name.split('|').forEach((part) => names.add(part));
        });

        const r = controller.fromValue(rValue);
        if (!r)
          throw new Error(
            [
              `Enum "${controller._name}": method "or()" not supports completely`,
              `Use "combinable:true" option on Enum init`,
            ].join('\r\n')
          );

        const rHack = r as EnumWrap<TTypeMap>;

        if (!rHack._name)
          rHack._name = [...names]
            .map(controller.fromString)
            .sort((a, b) => +a - +b)
            .map((w) => w.name)
            .join('|');

        return r;
      }

      {
        const names: string[] = [];
        prevWraps.forEach((w) => {
          names.push(...w.name.split('|'));
        });

        controller._entries.forEach(([k, w]) => {
          const name = String(k);

          if (names.includes(name)) return;

          Object.defineProperty(or, name, {
            get() {
              const _or = Or(...prevWraps, w);

              function r() {
                return _or();
              }
              r.or = _or;

              return r;
            },
          });
        });
      }

      return or;
    }

    return Generics.Cast.To(Or(Generics.Cast.To(this)));
  }

  /**
   * @see Enum.Switch
   */
  public switch<
    TCase,
    TCases extends Enum.Switch.Cases<TTypeMap, TCase> = Enum.Switch.Cases<
      TTypeMap,
      TCase
    >
  >(cases: TCases): TCase {
    return this.controller.switch(this.value, cases);
  }
}

class EnumWrapHack<
  TTypeMap extends Enum.TypeMap = Enum.TypeMap
> extends EnumWrap<TTypeMap> {
  public initialize(
    controller: () => Enum<TTypeMap> & Omit<Enum.Options<TTypeMap>, 'wrapClass'>
  ) {
    super.initialize(controller);
  }

  public get controller() {
    return super.controller;
  }
}

export namespace Enum {
  type _KeyList<
    TEnumType extends object,
    RV extends object = TS.Structure.Reverse<TEnumType>,
    RM extends object = {
      [K in keyof RV as K extends number ? K : never]: RV[K];
    }
  > = TS.Array.Cast<TS.Array.ValueList<RM>, keyof TEnumType>;

  /**
   * @summary Calculate enum keys array
   * @deprecated ⛔ Not worked: Type instantiation is excessively deep and possibly infinite.ts(2589)
   */
  export type KeyList<EnumType extends object> = _KeyList<EnumType>;

  export type KeyMap = Record<any, AVStantso.TS.Key>;

  export namespace TypeMap {
    export namespace Limits {
      export namespace KeysLength {
        /**
         * @summary Empirically selected `Keys` limit for `UnionKeys`. To avoid
         *
         * `The inferred type of this node exceeds the maximum length the compiler will serialize.
         *  An explicit type annotation is needed.ts(7056)`
         */
        export type ForUnionKeys = 6;

        /**
         * @summary Empirically selected `Keys` limit for `Combinations` by `UnionKeys` . To avoid
         *
         * `The inferred type of this node exceeds the maximum length the compiler will serialize.
         *  An explicit type annotation is needed.ts(7056)`
         */
        export type ForCombinations = 5;
      }
    }

    export namespace Abstract {
      export type Base = {
        EnumType?: unknown;
        Key?: AVStantso.TS.Key;
        Value?: unknown;
      };

      export namespace WithKeys {
        export type KeysProvider = {
          Keys?: AVStantso.TS.Key[];
        };

        export type MapUnion = {
          UnionKeys?: AVStantso.TS.Key;
        };
      }

      export type WithKeys = WithKeys.KeysProvider & WithKeys.MapUnion;

      export type WithWraps = {
        Wrap?: Enum.Value.Wrap & Enum.Value.Wrap.Ex.Abstract;
        Wraps?: Record<
          AVStantso.TS.Key,
          Value.Wrap & Enum.Value.Wrap.Ex.Abstract
        >;
      };

      export type Combinations<TKeys extends any[] = undefined> = {
        Combinations?: TKeys extends undefined
          ? boolean
          : AVStantso.TS.Array.Length.IfExeeded<
              TKeys,
              Limits.KeysLength.ForCombinations,
              false,
              boolean
            >;
      };

      export type Custom<TKeys extends any[] = undefined> = Partial<
        Abstract.WithKeys.MapUnion & Abstract.Combinations<TKeys>
      >;
    }

    export type Abstract = Abstract.Base &
      Abstract.WithKeys &
      Abstract.WithWraps &
      Abstract.Combinations;

    export type Custom<TKeys extends any[] = undefined> =
      Abstract.Custom<TKeys> & NodeJS.Dict<any>;

    export namespace For {
      export namespace Value {
        export namespace Wrap {
          export type Keys = 'Key' | 'Value' | 'UnionKeys';
        }

        export namespace Union {
          export type Keys = 'Value' | 'Wrap';
        }
      }
    }

    namespace Combination {
      export type FromEnumType<TEnumType> = {
        EnumType?: TEnumType;
        Key?: keyof TEnumType;
        Value?: TEnumType[keyof TEnumType];
      };

      export type Extension<TCustom extends Custom> = Omit<
        TCustom,
        keyof Abstract.Custom
      >;

      export type FromEnumAndCustom<
        TEnumType,
        TKeys extends FromEnumType<TEnumType>['Key'][],
        TCustom extends Custom<TKeys> = {},
        FET extends FromEnumType<TEnumType> = FromEnumType<TEnumType>,
        Key extends FromEnumType<TEnumType>['Key'] = FromEnumType<TEnumType>['Key'],
        NeedCalcUnionKeys extends boolean = AVStantso.TS.IfIsOverridedKey<
          'UnionKeys',
          Abstract.WithKeys,
          TCustom,
          false,
          true
        >,
        UnionKeys extends AVStantso.TS.Key = NeedCalcUnionKeys extends true
          ? AVStantso.TS.Array.Length.IfExeeded<
              TKeys,
              Limits.KeysLength.ForUnionKeys
            > extends true
            ? string
            : AVStantso.TS.Key.Union<Key, TKeys>
          : TCustom['UnionKeys'],
        TOverride extends Abstract.Custom = AVStantso.TS.Override<
          Abstract.Custom,
          TCustom
        >
      > = FET &
        Omit<TOverride, keyof Abstract.WithKeys.MapUnion> & {
          Keys?: TKeys;
          UnionKeys?: UnionKeys;
        };

      export type ValueWrap<
        TEnumType,
        TKeys extends FromEnumType<TEnumType>['Key'][],
        TCustom extends Custom<TKeys> = {},
        TKey extends keyof TEnumType = undefined,
        TValue extends FromEnumType<TEnumType>['Value'] = FromEnumType<TEnumType>['Value'],
        TFEAC extends FromEnumAndCustom<
          TEnumType,
          TKeys,
          TCustom
        > = FromEnumAndCustom<TEnumType, TKeys, TCustom>,
        TWrap extends Enum.Value.Wrap<TFEAC, TValue> &
          Extension<TCustom> = Enum.Value.Wrap<TFEAC, TValue> &
          Extension<TCustom>
      > = TWrap &
        Enum.Value.Wrap.Ex<
          Omit<Abstract, 'Key' | 'Value' | 'Wrap' | 'Combinations'> & {
            Key?: FromEnumType<TEnumType>['Key'];
            Value?: FromEnumType<TEnumType>['Value'];
            Wrap?: Enum.Value.Wrap<TFEAC, FromEnumType<TEnumType>['Value']>;
            Combinations?: AVStantso.TS.IfDefKey<
              'Combinations',
              Abstract,
              TCustom
            >;
          },
          TKey
        >;

      export type Customized<
        TEnumType,
        TKeys extends FromEnumType<TEnumType>['Key'][],
        TCustom extends Custom<TKeys> = {},
        TWrap extends ValueWrap<TEnumType, TKeys, TCustom> = ValueWrap<
          TEnumType,
          TKeys,
          TCustom
        >,
        TWraps extends {
          [Key in keyof TEnumType]: ValueWrap<
            TEnumType,
            TKeys,
            TCustom,
            Key,
            TEnumType[Key]
          >;
        } = {
          [Key in keyof TEnumType]: ValueWrap<
            TEnumType,
            TKeys,
            TCustom,
            Key,
            TEnumType[Key]
          >;
        }
      > = FromEnumAndCustom<TEnumType, TKeys, TCustom> & {
        Wrap?: TWrap;
        Wraps?: TWraps;
      };
    }

    export type Make<
      TEnumType,
      TKeys extends Combination.FromEnumType<TEnumType>['Key'][],
      TCustom extends Custom<TKeys> = {}
    > = Partial<Combination.Customized<TEnumType, TKeys, TCustom>>;

    export type Default<TEnumType> = Partial<
      Combination.Customized<TEnumType, [], undefined>
    >;
  }

  export type TypeMap = TypeMap.Abstract;

  export namespace Value {
    /**
     * @summary Standard AVStantso.TS Enum type value
     */
    export type Std = string | number;

    export namespace Wrap {
      export type Class<TTypeMap extends Enum.TypeMap> = new (
        value: TTypeMap['Value']
      ) => any;

      export namespace NameIs {
        export type Func = (...names: [string, ...string[]]) => boolean;
      }

      export type NameIs = NameIs.Func & {
        caseSensitive: NameIs.Func;
      };

      export namespace Ex {
        export type Keys = keyof Ex;

        /**
         * @summary All methods must be declared as optional
         */
        export type Abstract = Partial<Record<Keys, unknown>>;

        /**
         * @summary Wrap.Ex requires full TypeMap
         */
        export type Or<
          TTypeMap extends TypeMap,
          TKeysToExclude extends TTypeMap['Key'] = undefined,
          TNextKeys extends TTypeMap['Key'] = Exclude<
            TTypeMap['Key'],
            TKeysToExclude
          >
        > = ((
          ...values: [Union<TTypeMap>, ...Union<TTypeMap>[]]
        ) => TTypeMap['Wrap']) & {
          // Must be optional for assignment:
          //   X: Enum = Enum.A
          [Key in TNextKeys]?: (() => TTypeMap['Wrap']) & {
            or: Or<TTypeMap, TKeysToExclude | Key>;
          };
        };
      }

      /**
       * @summary Wrap.Ex requires full TypeMap. All methods must be declared as optional
       */
      export type Ex<
        TTypeMap extends TypeMap = TypeMap,
        TKey extends TTypeMap['Key'] = undefined
      > = {
        or?: Ex.Or<TTypeMap, TKey>;
      };
    }

    export type Wrap<
      TTypeMap extends Pick<TypeMap, TypeMap.For.Value.Wrap.Keys> = TypeMap,
      TValue extends TTypeMap['Value'] = TTypeMap['Value']
    > = {
      value: TValue;
      name: string;
      key: TTypeMap['Key'];

      is(...values: [TTypeMap['Value'], ...TTypeMap['Value'][]]): boolean;
      nameIs: Wrap.NameIs;

      switch<
        TCase,
        TCases extends Enum.Switch.Cases<TTypeMap, TCase> = Enum.Switch.Cases<
          TTypeMap,
          TCase
        >
      >(
        cases: TCases
      ): TCase;
    };

    export type Union<
      TTypeMap extends Pick<
        TypeMap,
        TypeMap.For.Value.Union.Keys
      > = Enum.TypeMap
    > = TTypeMap['Wrap'] | TTypeMap['Value'];

    export type FromUnion<
      TTypeMap extends Pick<TypeMap, TypeMap.For.Value.Union.Keys>,
      TUnion extends Value.Union<TTypeMap>
    > = TUnion extends TTypeMap['Wrap'] ? TUnion['value'] : TUnion;
  }

  /**
   * @summary Value for external declaration
   */
  export type Value<TTypeMap extends Pick<Enum.TypeMap, 'Wrap'>> = {
    [K in Exclude<
      keyof TTypeMap['Wrap'],
      keyof Value.Wrap.Ex
    >]: TTypeMap['Wrap'][K];
  } & {
    [K in keyof Value.Wrap.Ex]?: TTypeMap['Wrap'][K];
  };

  export type Combinations<
    TTypeMap extends Pick<Enum.TypeMap, 'Combinations' | 'UnionKeys' | 'Wrap'>
  > = TTypeMap['Combinations'] extends true
    ? {
        [K in TTypeMap['UnionKeys']]?: TTypeMap['Wrap'];
      }
    : {};

  export type Options<TTypeMap extends Enum.TypeMap> = {
    wrapClass?: Enum.Value.Wrap.Class<TTypeMap>;
    toJSONAsValue?: boolean;
    combinable?: boolean;
  };

  export type Controller<TTypeMap extends Enum.TypeMap> = Enum<TTypeMap> &
    Enum.Options<TTypeMap>;

  export type Statics<
    TTypeMap extends Enum.TypeMap,
    TValue extends Value.Union<TTypeMap>,
    TName extends string
  > = {
    /**
     * @summary Name specified on initialization
     */
    _name: TName;

    /**
     * @summary [key, value] pairs for all enum values
     */
    _entries: [TTypeMap['Key'], TValue][];

    /**
     * @summary All enum keys typed as Key
     */
    _keys: TTypeMap['Key'][];

    /**
     * @summary All enum keys typed as string
     */
    _names: string[];

    /**
     * @summary All enum values
     */
    _values: TValue[];
  };

  export namespace FromString {
    export type Func<
      TTypeMap extends Pick<TypeMap, TypeMap.For.Value.Union.Keys | 'Key'>
    > = (
      str: TTypeMap['Key'] | string,
      def?: Enum.Value.Union<TTypeMap>
    ) => TTypeMap['Wrap'];
  }

  export type FromString<
    TTypeMap extends Pick<TypeMap, TypeMap.For.Value.Union.Keys | 'Key'>
  > = FromString.Func<TTypeMap> & {
    capitalized: FromString.Func<TTypeMap>;
    uncapitalized: FromString.Func<TTypeMap>;
    caseInsensitive: FromString.Func<TTypeMap>;
  };

  export namespace Is {
    export type Item<TFrom> = (tested: TFrom) => boolean;
  }

  export type Is<TTypeMap extends TypeMap, TFrom> = ((
    tested: TFrom,
    value: Value.Union<TTypeMap>,
    ...values: Value.Union<TTypeMap>[]
  ) => boolean) &
    Record<TTypeMap['Key'], Is.Item<TFrom>>;

  export namespace Switch {
    export type Cases<
      TTypeMap extends Pick<TypeMap, 'Key' | 'UnionKeys'>,
      TCase
    > = AbstractSwitch.Cases<
      TTypeMap['Key'],
      { Case: TCase; KeysUnion: TTypeMap['UnionKeys'] }
    >;
  }

  /**
   * @summary This is analog of switch construction supports values from type enum
   * @example enum E {a, b, c, d, e }
   * const x = switch<number>(E.d, {a: 1, b: 2, "c|d|e": 3, default: 4} );
   * // x = 3
   */
  export type Switch<TTypeMap extends TypeMap> = {
    <
      TCase,
      TCases extends Switch.Cases<TTypeMap, TCase> = Switch.Cases<
        TTypeMap,
        TCase
      >
    >(
      value: Value.Union<TTypeMap>,
      cases: TCases
    ): TCase;
  };

  /**
   * @summary Find in a template of the specified type a property with a name corresponding to enum
   */
  export type Mapping<
    TTypeMap extends TypeMap,
    TTemplate extends object
  > = AVStantso.Mapping<TTypeMap['EnumType'] & object, TTemplate, 'value'>;
}

export type Enum<
  TTypeMap extends Enum.TypeMap,
  TName extends string = string
> = Enum.Statics<TTypeMap, TTypeMap['Wrap'], TName> & {
  isValue(candidate: unknown): candidate is TTypeMap['Value'];
  isWrap(candidate: unknown): candidate is TTypeMap['Wrap'];
  isUnion(candidate: unknown): candidate is Enum.Value.Union<TTypeMap>;

  fromValue(
    value: Enum.Value.Union<TTypeMap>,
    def?: Enum.Value.Union<TTypeMap>
  ): TTypeMap['Wrap'];

  fromString: Enum.FromString<TTypeMap>;

  toName(value: Enum.Value.Union<TTypeMap>): string;

  /**
   * @summary Add fields to passed entity by record with enum key
   * @param original Entity for extend
   * @param extender Addition item by key
   * @returns Extended entity
   */
  _extender: Extend.ByRecord<TTypeMap['Key'], TTypeMap['Wrap']>;

  is: Enum.Is<TTypeMap, Enum.Value.Union<TTypeMap>>;

  stringIs: Enum.Is<TTypeMap, string> & {
    caseSensitive: Enum.Is<TTypeMap, string>;
  };

  includes(value: Enum.Value.Union<TTypeMap>): boolean;

  /**
   * @summary This is analog of switch construction supports values from type enum
   * @example enum E {a, b, c, d, e }
   * const x = switch<number>(E.d, {a: 1, b: 2, "c|d|e": 3, default: 4});
   * // x = 3
   */
  switch: Enum.Switch<TTypeMap>;

  /**
   * @summary Create search function a one property in the template with a name corresponding to the enumeration
   */
  Mapping: <TTemplate extends {}>(
    template: TTemplate
  ) => Enum.Mapping<TTypeMap, TTemplate>;
};

/**
 * @summary Prepare enum wrapping
 */
export function Enum<
  TEnum extends object,
  TName extends string = string,
  TExtention extends {} = {}
>(
  enumDecl: TEnum,
  name: TName,
  extention?: Perform<TExtention, [Enum<Enum.TypeMap.Default<TEnum>, TName>]>
) {
  type Raw<TTypeMap extends Enum.TypeMap> = TEnum &
    Enum.Statics<TTypeMap, TTypeMap['Value'], TName>;

  type ReturnEnum<TTypeMap extends Enum.TypeMap> = Enum<TTypeMap, TName> &
    TTypeMap['Wraps'] &
    Enum.Combinations<TTypeMap> &
    TExtention & {
      /**
       * @summary Initial enum + statics
       */
      _: Raw<TTypeMap>;
    };

  let controller: any;

  /**
   * @summary Done wrapping
   */
  function internal<TTypeMap extends Enum.TypeMap>(
    wrapClassOrOptions?:
      | Enum.Value.Wrap.Class<TTypeMap>
      | Enum.Options<TTypeMap>
  ): ReturnEnum<TTypeMap> {
    //#region types
    type Type = Enum<TTypeMap>;

    type Key = TTypeMap['Key'];
    type UnionKeys = TTypeMap['UnionKeys'];

    type ValueRaw = TTypeMap['Value'];
    type ValueWrap = TTypeMap['Wrap'];
    type ValueUnion = Enum.Value.Union<TTypeMap>;
    //#endregion

    if (controller) throw Error(`Enum "${name}" already wrapped`);

    const enumOptions: Enum.Options<TTypeMap> = JS.is.object<
      Enum.Options<TTypeMap>
    >(wrapClassOrOptions)
      ? wrapClassOrOptions
      : { wrapClass: wrapClassOrOptions };
    const { wrapClass, combinable } = enumOptions;

    //#region getValueOf,  eqValues,  includes
    function getValueOf(value: ValueUnion): ValueRaw {
      return value instanceof EnumWrap ? value.value : value;
    }

    function eqValues(value1: ValueUnion, value2: ValueUnion): boolean {
      return getValueOf(value1) === getValueOf(value2);
    }

    const includes: Type['includes'] = (tested) =>
      values.some((value) => eqValues(tested, value));
    //#endregion

    //#region raw: entries, keys, values
    const _entries = Object.entries(enumDecl).reduce<[Key, ValueRaw][]>(
      (r, [key, value]) => {
        if (/^\d+$/.test(key)) r.push([value, JS.get.raw(enumDecl, value)]);

        return r;
      },
      []
    );
    const _values = _entries.map(([, value]) => value);

    const keys = _entries.map(([key]) => key as Key);

    const _: Raw<TTypeMap> = {
      ...enumDecl,
      _name: name,
      _entries,
      _keys: keys,
      _names: keys as string[],
      _values,
    };
    //#endregion

    //#region isValue, isWrap, isUnion
    const isValue: Type['isValue'] = (
      candidate: unknown
    ): candidate is TTypeMap['Value'] =>
      undefined === candidate ||
      null === candidate ||
      _values.some((v) => typeof candidate === typeof v);

    const isWrap: Type['isWrap'] = (
      candidate: unknown
    ): candidate is TTypeMap['Wrap'] =>
      JS.is.class(EnumWrap, candidate) &&
      (wrapClass
        ? candidate instanceof wrapClass
        : Generics.Cast.To<EnumWrapHack<TTypeMap>>(candidate).controller
            ._name === name);

    const isUnion: Type['isUnion'] = (
      candidate: unknown
    ): candidate is Enum.Value.Union<TTypeMap> =>
      isValue(candidate) || isWrap(candidate);
    //#endregion

    const factory = <TValue extends ValueRaw>(value: TValue): ValueWrap =>
      new (wrapClass || EnumWrap)(value);

    const hack = (wrap: ValueWrap) => wrap as EnumWrapHack<TTypeMap>;

    function initWrap(wrap: ValueWrap): ValueWrap {
      hack(wrap).initialize(() => ({
        ...controller,
        ...enumOptions,
      }));
      return wrap;
    }

    //#region entries, values, wraps
    const wraps: any = {};

    const entries: [Key, ValueWrap][] = _entries.map(([key, value]) => {
      const wrap = factory(value);

      wraps[key] = wrap;

      return [key, wrap];
    });
    const values = entries.map(([, value]) => value);
    //#endregion

    //#region fromValue, fromString, extender, toName
    function fromDefValue(def: ValueUnion): ValueWrap {
      return undefined === def || null === def
        ? Generics.Cast.To(def)
        : fromValue(def);
    }

    const fromValue: Type['fromValue'] = (value, def?) => {
      if (undefined === value || null === value) return fromDefValue(def);

      if (isWrap(value)) return Generics.Cast.To(value);

      let r = values.find((wrap) => wrap.value === value);
      if (!r && combinable) {
        r = initWrap(factory(value));

        values.push(r);
      }

      return r || fromDefValue(def);
    };

    function _fromString(
      str: string | Key,
      def: ValueUnion,
      transformKey?: (key: Key) => string
    ) {
      const [, r] =
        entries.find(
          ([key]) => (transformKey ? transformKey(key) : key) === str
        ) || [];
      return r || fromDefValue(def);
    }

    const fromString: Type['fromString'] = (str, def?) => _fromString(str, def);
    fromString.capitalized = (str, def?) =>
      _fromString(str, def, (key) => String(key).toCapitalized());
    fromString.uncapitalized = (str, def?) =>
      _fromString(str, def, (key) => String(key).toUncapitalized());
    fromString.caseInsensitive = (str, def?) =>
      _fromString(String(str).toLocaleLowerCase(), def, (key) =>
        String(key).toLocaleLowerCase()
      );

    const _extender = Extend.ByRecord(keys, fromString);

    const toName: Type['toName'] = (value: ValueUnion) =>
      Generics.Cast(enumDecl)[getValueOf(value)];
    //#endregion

    //#region is, stringIs
    const is = _extender(
      (
        tested: ValueUnion,
        value: ValueUnion,
        ...values: ValueUnion[]
      ): boolean =>
        [value, ...values].some((value: ValueUnion) => eqValues(tested, value)),
      (value, f) => (tested: ValueUnion) => f(tested, value)
    );

    const stringIsCaseSensitive = _extender(
      (tested: string, value: ValueUnion, ...values: ValueUnion[]): boolean =>
        [value, ...values].some(
          (value: ValueUnion) => tested === toName(value)
        ),
      (value, f) => (tested: string) => f(tested, value)
    );

    function stringIs(
      tested: string,
      value: ValueUnion,
      ...values: ValueUnion[]
    ): boolean {
      return [value, ...values].some(
        (value: ValueUnion) =>
          (tested || '').toLowerCase() === (toName(value) || '').toLowerCase()
      );
    }
    stringIs.caseSensitive = stringIsCaseSensitive;

    const stringIsEx = _extender(
      stringIs,
      (value, f) => (tested: string) => f(tested, value)
    );
    //#endregion

    const _switch: Type['switch'] = Generics.Cast.To(
      AbstractSwitch<Key, { Value: ValueUnion; KeysUnion: UnionKeys }>(toName)
    );

    const Mapping: Type['Mapping'] = avstantso.Mapping(
      'value',
      Generics.Cast(toName)
    );

    const _controller: Type = {
      _name: name,
      _entries: entries,
      _keys: keys,
      _names: keys as string[],
      _values: values,
      isValue,
      isWrap,
      isUnion,
      fromValue,
      fromString,
      toName,
      _extender,
      is,
      stringIs: stringIsEx,
      includes,
      switch: _switch,
      Mapping,
    };

    controller = { _, ...wraps, ..._controller };

    values.forEach(initWrap);

    if (combinable) {
      for (let vs: any[] = [...values], l = vs.length, i = 0; i < l; i++)
        for (let j = i + 1; j < l; j++)
          for (let v = hack(vs[i]), k = j; k < l; k++)
            try {
              v = hack(v.or(vs[k]));

              JS.set(controller, v.name, v);
            } catch (e) {
              e.message = `${name}: ${
                e.message
              } [l=${l}, i=${i}, j=${j}, k=${k}, +v=${+v}]`;
              throw e;
            }
    }

    if (extention)
      Object.entries(Perform(extention, controller)).forEach(([key, value]) =>
        JS.set(controller, key, value)
      );

    return controller;
  }

  return internal;
}
