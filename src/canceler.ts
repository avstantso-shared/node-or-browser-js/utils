import { Generics } from './generics';
import { X } from './X';
import { Stub } from './stub';
import { JS } from './js';

/**
 * @summary Control of cancelable action
 */
export namespace Canceler {
  /**
   * @summary Stub for func/funcs if `isCanceled`
   * @returns Stub for func/funcs. This stub prevent func call if `isCanceled`
   */
  export type Safe = Stub.Overload;

  /**
   * @summary Cancel event handler
   */
  export type Handler = () => unknown;

  /**
   * @summary Context for external usage
   */
  export type Context = {
    /**
     * @summary `get` current `isCanceled` state
     */
    isCanceled: boolean;

    /**
     * @summary Stub for func/funcs if `isCanceled`. Prevent call `undefined`
     * @returns Stub for func/funcs. This stub prevent func call if `isCanceled`
     */
    safe: Safe;

    /**
     * @summary Subscribe to cancel event
     * @param handler Event handler
     */
    subscribe(handler: Handler): void;

    /**
     * @summary Unsubscribe to cancel event
     * @param handler Event handler
     * @returns `true` if success unsubscribed
     */
    unsubscribe(handler: Handler): boolean;
  };

  /**
   * @summary Check `candidate` is `Canceler`/`Canceler.Context`
   * @param candidate Candidate for check
   */
  export type Is = {
    /**
     * @summary Check `candidate` is `Canceler`
     * @param candidate Candidate for check
     */
    (candidate: unknown): candidate is Canceler;

    /**
     * @summary Check `candidate` is `Canceler.Context`
     * @param candidate Candidate for check
     */
    Context(candidate: unknown): candidate is Canceler.Context;
  };

  /**
   * @summary `Canceler` methods who can derived from `Canceler` chain
   */
  export type Derive = {
    /**
     * @summary Create `Canceler` with inner state
     */
    (): Canceler;

    /**
     * @summary Create `Canceler` with custom state
     * @param isCanceled Current get canceled state function
     * @param cancel Cancel action
     */
    Custom(
      isCanceled: () => boolean,
      cancel?: Canceler['cancel'],
      handlers?: Handler[]
    ): Canceler;
  };

  /**
   * @summary Merge `cancelers`/`contexts` to one `Canceler`/`Canceler.Context` by `or` algorithm
   * @param cancelers Cancelers/contexts for merge
   */
  export type Merge = {
    /**
     * @summary Merge `cancelers` to one `Canceler` by `or` algorithm
     * @param cancelers Cancelers for merge
     */
    (...cancelers: [Canceler, Canceler, ...Canceler[]]): Canceler;

    /**
     * @summary Merge `contexts` to one `Canceler.Context` by `or` algorithm
     * @param contexts Contexts for merge
     */
    (
      ...contexts: [Canceler.Context, Canceler.Context, ...Canceler.Context[]]
    ): Canceler.Context;
  };

  /**
   * @summary `Canceler` function overload
   */
  export type Overload = Derive & {
    /**
     * @summary Check `candidate` is `Canceler`/`Canceler.Context`
     * @param candidate Candidate for check
     */
    is: Is;

    /**
     * @summary Merge `cancelers`/`contexts` to one `Canceler`/`Canceler.Context` by `or` algorithm
     * @param cancelers Cancelers/contexts for merge
     */
    Merge: Canceler.Merge;

    /**
     * @summary Derive `Canceler` by `parentContext`
     * @description All methods results was merged with parentContext
     */
    Derive(parentContext: Canceler.Context): Derive;
  };
}

export type Canceler = Canceler.Context & {
  /**
   * @summary Context for external usage
   */
  context: Canceler.Context;

  /**
   * @summary Set `isCanceled` state to `true`
   */
  cancel(): void;
};

function isCancelerContext(candidate: unknown): candidate is Canceler.Context {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    'isCanceled' in candidate &&
    'safe' in candidate
  );
}

function isCanceler(candidate: unknown): candidate is Canceler {
  return isCancelerContext(candidate) && 'cancel' in candidate;
}
isCanceler.Context = isCancelerContext;

const Derive: Canceler.Overload['Derive'] = (parentContext) => {
  const Custom: Canceler.Overload['Custom'] = (
    isCanceled,
    cancel?,
    handlers?
  ) => {
    const _isCanceled = parentContext
      ? () => parentContext.isCanceled || isCanceled()
      : isCanceled;

    const safe: Canceler['safe'] = Stub.Custom((func) =>
      _isCanceled() || !func ? Generics.Cast.To(X) : func
    );

    const subscribe: Canceler['subscribe'] = (handler) => {
      if (!handlers)
        throw Error(`"${subscribe.name}" not supported by this canceler`);

      handlers.push(handler);
    };

    const unsubscribe: Canceler['unsubscribe'] = (handler) => {
      if (!handlers)
        throw Error(`"${unsubscribe.name}" not supported by this canceler`);

      const i = handlers.indexOf(handler);
      if (i < 0) return false;

      handlers.splice(i, 1);

      return true;
    };

    const context: Canceler.Context = {
      get isCanceled() {
        return _isCanceled();
      },
      safe,
      subscribe,
      unsubscribe,
    };

    return {
      get isCanceled() {
        return _isCanceled();
      },
      safe,
      subscribe,
      unsubscribe,
      context,
      cancel: cancel || X,
    };
  };

  function Canceler() {
    let isCanceled = false;
    const handlers: Canceler.Handler[] = [];

    function cancel() {
      isCanceled = true;
      handlers.forEach((h) => h());
    }

    return Custom(() => isCanceled, cancel, handlers);
  }

  Canceler.Custom = Custom;

  return Canceler;
};

const Merge: Canceler.Merge = (...params: any[]): any => {
  let notContextsOnly = false;
  for (let l = params.length, i = 0; i < l; i++) {
    const item = params[i];

    if (!isCancelerContext(item))
      throw Error(`Invalid Canceler/Canceler.Context for merge at index ${i}`);

    notContextsOnly = notContextsOnly || isCanceler(item);
  }

  const r = Derive(undefined).Custom(
    () => params.some(({ isCanceled }) => isCanceled),
    notContextsOnly && (() => params.forEach(({ cancel }) => cancel()))
  );

  return notContextsOnly ? r : r.context;
};

export const Canceler = Derive(undefined) as Canceler.Overload;
Canceler.is = isCanceler;
Canceler.Merge = Merge;
Canceler.Derive = Derive;
