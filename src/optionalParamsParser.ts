export namespace OptionalParamsParser {
  export type ParamsFunc = <TParamsArray extends unknown[]>() => TParamsArray;
}

export interface OptionalParamsParser {
  [name: string]: OptionalParamsParser.ParamsFunc;
}
