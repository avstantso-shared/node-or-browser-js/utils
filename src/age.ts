/**
 * @summary Calculate full years of age
 * @param dob Date of birthsday
 * @param to For the specified date. if undefined, Date.now() used
 * @returns Full years of age
 */
export function fullYear(dob: Date, to?: Date): number {
  const month_diff = (to ? to.getTime() : Date.now()) - dob.getTime();
  const age_dt = new Date(month_diff);
  const year = age_dt.getUTCFullYear();
  return Math.abs(year - 1970);
}
