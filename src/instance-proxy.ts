import { JS } from './js';

export namespace InstanceProxy {
  export type Instance = object | Function;

  export type Wrap<TInstance extends Instance = Instance> = <
    P extends keyof TInstance
  >(
    instance: () => TInstance,
    p: P
  ) => TInstance[P] | any;

  export namespace Config {
    export type Options<TInstance extends Instance = Instance> = {
      original: TInstance;
      wrap?: Wrap<TInstance>;
    };
  }
  export type Config = NodeJS.Dict<Instance | Config.Options>;

  export type Proxy<TInstance extends Instance = Instance> = TInstance &
    Proxy.Control<TInstance>;

  export namespace Proxy {
    export type Control<TInstance extends Instance = Instance> = {
      instance: TInstance;
      setInstance(instance: TInstance): void;
    };
  }

  export type Result<TConfig extends Config> = {
    [K in keyof TConfig]: TConfig[K] extends Config.Options
      ? Proxy<TConfig[K]['original']>
      : Proxy<TConfig[K]>;
  };

  export type Key<TProxy extends Proxy> = TProxy extends Proxy<infer TInstance>
    ? keyof TInstance
    : never;

  /**
   * @summary Create instance proxy with old linking by `setInstance`
   * @param original Original instance
   * @param name Original name
   */
  export type Make = <TInstance extends Instance>(
    original: TInstance,
    name?: string,
    wrap?: Wrap<TInstance>
  ) => Proxy<TInstance>;

  /**
   * @summary Create instances proxies with old linking by `setInstance`
   * @param config Original instances map
   */
  export type Factory = {
    <TConfig extends Config>(
      config: TConfig,
      generalWrap?: Wrap
    ): Result<TConfig>;

    /**
     * @summary Create instance proxy with old linking by `setInstance`
     * @param original Original instance
     * @param name Original name
     */
    Make: Make;

    /**
     * @summary Create instance proxy config options
     * @param original Original instance
     * @param wrap Wrap property callback
     */
    Options<TInstance extends Instance = Instance>(
      original: TInstance,
      wrap?: Wrap<TInstance>
    ): Config.Options<TInstance>;

    /**
     * @summary General behavior of all `InstanceProxy` instances. If `true` — must called `setInstance` for every `InstanceProxy`
     * @default true
     */
    strict: boolean;
  };
}

let strictInstanceProxy = true;

const MakeInstanceProxy: InstanceProxy.Make = (original, name, wrap) => {
  type TInstance = typeof original;

  let instance: TInstance;
  const instanceName =
    name || (JS.is.function(original) ? original.name : undefined);

  function getInstance() {
    if (!instance) {
      if (strictInstanceProxy)
        throw Error(
          `Need instance. Use ${instanceName}.${setInstance.name} for setup instance`
        );

      return original;
    }

    return instance;
  }

  const setInstance = (value: TInstance) => (instance = value);

  const target = JS.is.function(original)
    ? new Function(
        'original',
        `return function ${instanceName}Proxy(...params){ return original(...params); };`
      )(original)
    : {};

  JS.set.raw(target, setInstance.name, setInstance);

  return new Proxy(target as InstanceProxy.Proxy<TInstance>, {
    get: (target, p, receiver) => {
      switch (p) {
        case 'instance':
          return getInstance();
        case setInstance.name:
          return setInstance;
      }

      return wrap
        ? wrap(getInstance, p as keyof TInstance)
        : getInstance()[p as keyof TInstance];
    },
  });
};

export const InstanceProxy = Object.defineProperties(
  ((config: InstanceProxy.Config, generalWrap?: InstanceProxy.Wrap) => {
    const r: any = {};

    Object.entries(config).forEach(([key, value]) => {
      if ('original' in value && value.original) {
        const { original, wrap } = value;
        JS.set(r, key, MakeInstanceProxy(original, key, wrap || generalWrap));
      } else JS.set(r, key, MakeInstanceProxy(value, key, generalWrap));
    });

    return r;
  }) as InstanceProxy.Factory,
  {
    Make: { value: MakeInstanceProxy, writable: false },

    Options: {
      value: (original: any, wrap?: any) => ({ original, wrap }),
      writable: false,
    },

    strict: {
      get: () => strictInstanceProxy,
      set: (value: boolean) => (strictInstanceProxy = value),
    },
  }
);
