// Source:
// https://stackoverflow.com/questions/3426404/create-a-hexadecimal-colour-based-on-a-string-with-javascript

/* eslint-disable no-bitwise */
export function str2Color(str: string, prefix = '#') {
  let hash = 0;
  (str || '').split('').forEach((char) => {
    hash = char.charCodeAt(0) + ((hash << 5) - hash);
  });

  let color = prefix;
  for (let i = 0; i < 3; i++) {
    const value = (hash >> (i * 8)) & 0xff;
    color += value.toString(16).padStart(2, '0');
  }

  return color;
}
/* eslint-enable no-bitwise */
