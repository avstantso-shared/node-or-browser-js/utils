// See also:
// https://stackoverflow.com/questions/136505/searching-for-uuids-in-text-with-regex
// https://github.com/uuidjs/uuid/blob/main/src/regex.js

import { JS } from './js';

export const UUIDLikeRegEx =
  /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i;

export function isUUIDLike(candidate: unknown): candidate is string {
  return JS.is.string(candidate) && UUIDLikeRegEx.test(candidate);
}
