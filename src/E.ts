/**
 * @summary Global catch method for errors.
 *
 * Hendle one error by default handler or combine handlers to one handler
 * @see `AVStantso.Catch`
 */
export const E: AVStantso.Catch = avstantso.Catch;
