/**
 * @summary switch case by key of value or default
 */
export namespace Switch {
  export type StdKeys = AVStantso.Switch.StdKeys;

  export type Options = AVStantso.Switch.Options;

  export type Cases<
    TKey extends AVStantso.TS.Key,
    TOptions extends Options = {}
  > = AVStantso.Switch.Cases<TKey, TOptions>;
}

export const Switch = avstantso.Switch;
