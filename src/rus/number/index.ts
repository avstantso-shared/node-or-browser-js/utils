import { JS } from '../../js';
import { Enum } from '../../enum';

import * as _ from './enum';

/**
 * @summary Число в русском языке
 * @example 1 - "день", 2-3-4 - "дня", 0-5+ - "дней"
 */
export namespace Number {
  export type TypeMap = Enum.TypeMap.Make<
    _.RusNumber.Type,
    _.RusNumber.Key.List
  >;

  /**
   * @summary Формы слова на русском языке в единственном, двойственном и множественном числе
   */
  export type WordForms = Record<TypeMap['Key'], string>;

  export interface WithSubstantive {
    /**
     * @summary Выбор формы существительного по переданному математическому числу
     * @param wordForms Формы слова на русском языке в единственном, двойственном и множественном числе
     * @param num Математическое число
     */
    (wordForms: WordForms): (num: number) => string;

    /**
     * @summary Выбор формы существительного по переданному математическому числу
     * @param singular Форма слова на русском языке в единственном числе
     * @param dual Форма слова на русском языке в двойственном числе
     * @param plural Форма слова на русском языке во множественном числе
     * @param num Математическое число
     */
    (singular: string, dual: string, plural: string): (num: number) => string;
  }
}

const withSubstantive: Number.WithSubstantive = (...wordForms: any[]) => {
  const { singular, dual, plural }: Number.WordForms =
    JS.is.object<Number.WordForms>(wordForms[0])
      ? wordForms[0]
      : { singular: wordForms[0], dual: wordForms[1], plural: wordForms[2] };

  return (num) => {
    const m10 = Math.abs(num) % 10;
    const m100 = Math.abs(num) % 100;

    return !(m10 >= 1 && m10 <= 4) || (m100 >= 11 && m100 <= 14)
      ? plural
      : 1 === m10
      ? singular
      : dual;
  };
};

export type Number = Enum.Value<Number.TypeMap>;

export const Number = Enum(_.RusNumber, 'Rus.Number', {
  withSubstantive,
})<Number.TypeMap>();
