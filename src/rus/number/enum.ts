/**
 * @summary Число в русском языке
 * @example 1 - "день", 2-3-4 - "дня", 0-5+ - "дней"
 */
export enum RusNumber {
  /**
   * @summary Единственное
   * @example "день", "мошка"
   */
  singular = 1,

  /**
   * @summary Двойственное
   * @example "дня", "мошки"
   */
  dual,

  /**
   * @summary Множественное
   * @example "дней", "мошек"
   */
  plural,
}

export namespace RusNumber {
  export type Type = typeof RusNumber;

  export namespace Key {
    export type List = ['singular', 'dual', 'plural'];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}
