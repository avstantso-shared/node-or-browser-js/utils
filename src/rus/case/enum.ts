/**
 * @summary Падеж в русском языке
 */
export enum RusCase {
  /**
   * @summary Именительный
   * @example Кто? Что?
   */
  nominative = 1,

  /**
   * @summary Родительный
   * @example Кого? Чего?
   */
  genitive,

  /**
   * @summary Дательный
   * @example Кому? Чему?
   */
  dative,

  /**
   * @summary Винительный
   * @example Кого? Что?
   */
  accusative,

  /**
   * @summary Творительный
   * @example Кем? Чем?
   */
  instrumental,

  /**
   * @summary Предложный
   * @example О ком? О чём?; В ком? В чём?
   */
  prepositional,
}

export namespace RusCase {
  export type Type = typeof RusCase;

  export namespace Key {
    export type List = [
      'nominative',
      'genitive',
      'dative',
      'accusative',
      'instrumental',
      'prepositional'
    ];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}
