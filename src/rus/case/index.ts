import { Enum } from '../../enum';

import * as _ from './enum';

/**
 * @summary Падеж в русском языке
 */
export namespace Case {
  // @ts-ignore fix CI test TS2589: Type instantiation is excessively deep and possibly infinite
  export type TypeMap = Enum.TypeMap.Make<_.RusCase.Type, _.RusCase.Key.List>;
}

export type Case = Enum.Value<Case.TypeMap>;

// @ts-ignore fix CI test TS2589: Type instantiation is excessively deep and possibly infinite
export const Case = Enum(_.RusCase, 'Rus.Case')<Case.TypeMap>();
