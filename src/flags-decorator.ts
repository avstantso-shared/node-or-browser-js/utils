/* eslint no-bitwise: "off" */
import { JS } from './js';

export namespace FlagsDecorator {
  export type Map = { [x: number]: string };

  export type Readable = boolean | (() => number);

  export type Writable = boolean | ((flags: number) => void);

  export type Options<TFlagsFieldName extends string = string> = {
    field?: TFlagsFieldName;
    flags?: number;
    readable?: Readable;
    writable?: Writable;
  };

  export type Overload = {
    <TMap extends Map, TFlagsFieldName extends string = 'flags'>(
      map: TMap,
      options?: Options<TFlagsFieldName>
    ): FlagsDecorator;
    <TMap extends Map, TFlagsFieldName extends string = 'flags'>(
      map: TMap,
      field?: TFlagsFieldName
    ): FlagsDecorator;
    <TMap extends Map>(map: TMap, flags: number): FlagsDecorator;

    is: typeof isFlagsDecorator;
  };
}

export type FlagsDecorator = {
  /**
   * @summary Add propertiyes by `Object.defineProperty` to `entity`. Mutate `entity`
   */
  expand<T extends object>(entity: T): T;

  /**
   * @summary Remove propertiyes by `delete`. Mutate `entity`
   */
  omit<T extends object>(entity: T): T;
};

function isFlagsDecorator(candidate: unknown): candidate is FlagsDecorator {
  return (
    candidate &&
    JS.is.object<FlagsDecorator>(candidate) &&
    JS.is.function(candidate.expand) &&
    JS.is.function(candidate.omit)
  );
}

export const FlagsDecorator: FlagsDecorator.Overload = (
  map: any,
  param: any
) => {
  const clearMap = Object.entries(map).reduce((r, [key, name]) => {
    const flag = Number.parseInt(key, 10);
    if (!Number.isNaN(flag)) r.push([flag, name as string]);

    return r;
  }, [] as [number, string][]);

  function expand<T extends object>(entity: T): T {
    const options: FlagsDecorator.Options = JS.is.object(param)
      ? param
      : undefined;
    let initialFlags = JS.is.number(param) ? param : options?.flags;
    const field = (JS.is.string(param) ? param : options?.field) || 'flags';

    const read: () => number =
      JS.is.number(initialFlags) && !options?.readable
        ? () => initialFlags
        : JS.is.function(options?.readable)
        ? options.readable
        : () => JS.get.raw(entity, field);

    const write = !options?.writable
      ? undefined
      : JS.is.function(options.writable)
      ? options.writable
      : (flags: number) => JS.set.raw(entity, field, flags);

    clearMap.forEach(([flag, name]) =>
      Object.defineProperty(entity, name as keyof T, {
        get: () => (read() & (1 << flag)) > 0,
        ...(write
          ? {
              set: (value: any) => {
                write(!!value ? read() | (1 << flag) : read() & ~(1 << flag));
              },
            }
          : {}),
      })
    );

    return entity;
  }

  function omit<T extends object>(entity: T): T {
    clearMap.forEach(([, name]) => {
      delete entity[name as keyof T];
    });

    return entity;
  }

  return { expand, omit };
};

FlagsDecorator.is = isFlagsDecorator;
