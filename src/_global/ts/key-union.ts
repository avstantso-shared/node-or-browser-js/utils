namespace AVStantso.TS.Key {
  type Concat<TPefix extends string, TKey extends Key> = `${TPefix extends ''
    ? ''
    : `${TPefix}|`}${Exclude<TKey, symbol>}`;

  /**
   * @deprecated `ArrNode` more efficient
   * @example
   * type ABCDEFG = KeyFromArray<LettersArray<7>>; // limit by depth for Node
   */
  namespace Node {
    export type Limit = 7;
  }
  type Node<
    TKey extends Exclude<Key, symbol>,
    TDepthLimit extends number = undefined,
    TPefix extends string = '',
    TKeysToExclude extends TKey = never,
    TDepth extends number = 0
  > = {
    [K in Exclude<TKey, TKeysToExclude> as Concat<TPefix, K>]: Exclude<
      TKey,
      TKeysToExclude | K
    > extends never
      ? never
      : TDepthLimit extends TDepth
      ? never
      : Node<
          TKey,
          TDepthLimit,
          Concat<TPefix, K>,
          TKeysToExclude | K,
          Increment<TDepth>
        >;
  };

  /**
   * @deprecated `ArrNode` more efficient
   * @example
   * type ABCDEFG = KeyFromArray<LettersArray<8>>; // limit by depth for TopNode
   */
  namespace TopNode {
    export type Limit = 8;
  }
  type TopNode<
    TKey extends Exclude<Key, symbol>,
    TKeysList extends TKey[],
    TDepthLimit extends number = undefined,
    I extends number = 0,
    TKeysToExclude extends TKey = never
  > = TKeysList[I] extends undefined
    ? unknown
    : {
        [K in TKeysList[I]]: Exclude<TKey, TKeysToExclude | K> extends never
          ? never
          : Node<TKey, TDepthLimit, `${K}`, TKeysToExclude | K, 1>;
      } & TopNode<
        TKey,
        TKeysList,
        TDepthLimit,
        Increment<I>,
        TKeysToExclude | TKeysList[I]
      >;

  /**
   * @example
   * type ABCDEFGHIJKLMNOP = KeyFromArray<LettersArray<16>>; // limit by depth for ArrNode
   */
  namespace ArrNode {
    export type Limit = 16;
  }
  type ArrNode<
    TKey extends Key,
    TKeysList extends Key[],
    TDepthLimit extends number = 99,
    R = unknown,
    D extends number = 0,
    P extends string = '',
    I extends number = 0,
    K extends Extract<TKeysList[I], TKey> = Extract<TKeysList[I], TKey>
  > = K extends undefined // =Array.IfEnd<Arr, I>
    ? R
    : D extends TDepthLimit
    ? R
    : {
        [Key in K as Concat<P, K>]: ArrNode<
          TKey,
          TKeysList,
          TDepthLimit,
          never,
          Increment<D>,
          Concat<P, K>,
          Increment<I>
        >;
      } & ArrNode<TKey, TKeysList, TDepthLimit, unknown, 0, P, Increment<I>>;

  namespace Calc {
    type K<T> = T extends never ? never : keyof T;

    type S<T> = K<T & { [Kn in K<T> as S<T[Kn]>]: 0 }>;

    export type Keys<
      TKey extends Key,
      TKeysList extends Key[],
      TDepthLimit extends number = 99,
      N extends ArrNode<TKey, TKeysList, TDepthLimit> = ArrNode<
        TKey,
        TKeysList,
        TDepthLimit
      >
    > = K<{
      [K0 in K<N> as S<N[K0]>]: 0;
    }>;
  }

  export namespace Union {
    /**
     * @summary List with all keys length limit. Avoid ts error
     *
     *  `Type instantiation is excessively deep and possibly infinite.ts(2589)`
     */
    export type KeysListLengthLimit = ArrNode.Limit;
  }

  /**
   * @summary Set of keys combinations with `|` separator.
   *
   * ⛔ Max supported `TKeysList` length is `Union.KeysListLengthLimit=15` in current implementation ⛔
   * @param TKey Key type, for example `'A' | 'B' | 'C'`
   * @param TKeysList List with all keys, for example `['A', 'B', 'C']`
   * @param TDepthLimit Max number of keys in one item, `0 | 1` — doesn't make sense
   * @example
   * type n0 = Union<never, []>; // never
   * type n1 = Union<'A', ['A']>; // never
   *
   * type ABC = 'A' | 'B' | 'C';
   * type U = Union<ABC, ['A', 'B', 'C']> = "A|B" | "A|C" | "A|B|C" | "B|C"
   * type U2 = Union<ABC, ['A', 'B', 'C'], 2> = "A|B" | "A|C" | "B|C"
   */
  export type Union<
    TKey extends Key,
    TKeysList extends Key[],
    TDepthLimit extends number = Union.KeysListLengthLimit
  > =
    // =Array.Length.IfExeeded<TKeysList, Union.KeysListLengthLimit>
    TKeysList[Union.KeysListLengthLimit] extends undefined
      ? Calc.Keys<TKey, TKeysList, TDepthLimit>
      : string; // Avoid ts error "Type instantiation is excessively deep and possibly infinite.ts(2589)"
}
