namespace AVStantso.TS.Array {
  //#region Create
  type _Create<
    L extends number,
    Item,
    R extends Item[] = [],
    I extends number = 0
  > = I extends L ? R : _Create<L, Item, [...R, Item], Increment<I>>;

  /**
   * @summary Create array of `Item` with specified length `L`
   * @param L Length
   * @param Item Item type
   * @returns `[Item × L]`
   * @example
   * type n = Create<never, any>; // never
   * type u = Create<undefined, any>; // never
   *
   * type a5x3 = Create<5, 3>; // [3, 3, 3, 3, 3]
   * type a2xABC = Create<2, 'ABC'>; // ["ABC", "ABC"]
   */
  export type Create<L extends number, Item> = L extends undefined
    ? never
    : _Create<L, Item>;
  //#endregion

  //#region Numbers
  type _Numbers<
    L extends number,
    R extends number[] = [],
    I extends number = 0
  > = L extends I ? R : _Numbers<L, [...R, I], Increment<I>>;

  /**
   * @summary Create numeric literals array.
   * @param L Length
   * @returns Numeric literals array
   * @example
   * type n = Numbers<never>; // never
   * type u = Numbers<undefined>; // never
   *
   * type a0 = Numbers<0>; // []
   * type a3 = Numbers<3>; // [0, 1, 2]
   */
  export type Numbers<L extends number> = L extends undefined
    ? never
    : _Numbers<L>;
  //#endregion

  type _Letters<
    L extends number,
    R extends string[] = [],
    I extends number = 0
  > = L extends I ? R : _Letters<L, [...R, TS.Letters[I]], Increment<I>>;

  /**
   * @summary Create letters literals array. `undefined` items not supported!
   * @param L Length
   * @returns Letters literals array
   * @example
   * type n = Letters<never>; // never
   * type u = Letters<undefined>; // never
   *
   * type a0 = Letters<0>; // []
   * type a3 = Letters<3>; // ["A", "B", "C"]
   */
  export type Letters<L extends number> = L extends undefined
    ? never
    : _Letters<L>;

  // : Sub<AVStantso.TS.Letters, 0,  Decrement<L>>;

  //Array.Sub<ASCII, 65, 90>;
}
