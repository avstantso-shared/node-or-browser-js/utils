namespace AVStantso.TS.Array {
  //#region FromKey
  namespace _FromKey {
    namespace Object {
      export type Sub<
        Key extends TS.Key,
        K extends Key,
        R extends any[],
        E extends Exclude<Key, K> = Exclude<Key, K>,
        O extends Object<E, R> = Object<E, R>
      > = [E] extends [never] ? Sort<R> : { [K1 in keyof O]: O[K1] };
    }

    export type Object<Key extends TS.Key, R extends any[] = []> = {
      [K in Key]: Object.Sub<Key, K, [...R, K]>;
    };

    export type ToFlat<O extends {}> = {
      [K0 in keyof O as '_']: O[K0] extends any[] ? O[K0] : ToFlat<O[K0]>;
    }['_'];
  }

  type _FromKey<
    Key extends TS.Key,
    O = _FromKey.Object<Key>,
    F = _FromKey.ToFlat<O>
  > = F;

  /**
   * @summary Calculate array of keys by key
   * @param Key Key union
   * @returns Array of keys sorted by alphabet with ascending order
   * @example
   * type k = FromKey<'b' | 'a' | 'c'>; // ["a", "b", "c"]
   */
  export type FromKey<Key extends TS.Key> = _FromKey<Key>;
  //#endregion

  //#region KeyList
  type _KeyList<
    T extends object,
    Keys extends number,
    R extends any[] = [],
    I extends number = 0
  > = [Keys] extends [never]
    ? R
    : [Extract<I, keyof T>] extends [never]
    ? _KeyList<T, Keys, R, Increment<I>>
    : _KeyList<T, Exclude<Keys, I>, [...R, I], Increment<I>>;

  type _KeyListCheck<
    T extends object,
    Keys extends number = Extract<keyof T, number>
  > = [Keys] extends [never] ? _FromKey<keyof T> : _KeyList<T, Keys>;

  /**
   * @summary Create array of `keyof T`
   * @param T Structural object
   * @returns Array of keys sorted by alphabet with ascending order
   * @see `FromKey`
   * @example
   * type n = KeyList<never>; // never
   * type u = KeyList<undefined>; // undefined
   * type e = KeyList<{}>; // []
   * type a = KeyList<[1, 2, 3]>; // never
   * type nk = KeyList<{ 1: 'A'; 2: 'B'; 3: 'C' }>; // [1, 2, 3]
   * type sk = KeyList<{ A: '1' }>; // ["A"]
   */
  export type KeyList<T extends object> = T extends undefined
    ? undefined
    : [keyof T] extends [never]
    ? []
    : IfStructure<T> extends true
    ? _KeyListCheck<T>
    : never;
  //#endregion

  //#region ValueList
  type _ValueList<
    T extends object,
    KeyListOfT extends (keyof T)[],
    R extends any[] = [],
    I extends number = 0
  > = IfEnd<KeyListOfT, I> extends true
    ? R
    : _ValueList<T, KeyListOfT, [...R, T[KeyListOfT[I]]], Increment<I>>;

  /**
   * @summary Create array of `T[keyof T]`
   * @param T Structural object (or array without effect)
   * @returns Array of values sorted by keys by alphabet with ascending order
   * @see `FromKey`
   * @example
   * type n = ValueList<never>; // never
   * type u = ValueList<undefined>; // undefined
   * type e = ValueList<{}>; // []
   * type a = ValueList<[1, 2, 3]>; // [1, 2, 3]
   * type nk = ValueList<{ 1: 'A'; 2: 'B'; 3: 'C' }>; // ["A", "B", "C"]
   * type ss = ValueList<{ A: '1' }>; // ["1"]
   */
  export type ValueList<
    T extends object,
    KeyListOfT extends (keyof T)[] = KeyList<T>
  > = T extends undefined
    ? undefined
    : [keyof T] extends [never]
    ? []
    : T extends any[]
    ? T
    : IfStructure<T> extends true
    ? _ValueList<T, KeyListOfT>
    : never;
  //#endregion
}
