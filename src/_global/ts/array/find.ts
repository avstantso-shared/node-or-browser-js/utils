namespace AVStantso.TS.Array {
  /**
   * @summary Find index of type in array type.
   *
   * ⛔ `undefined` items not supported ⛔
   * @param Arr Tested array type
   * @param Item Tested item type
   * @param I Start index
   * @returns Number literal or `undefined` if not found
   * type n0 = IndexOf<never, any, any>; // never
   * type n1 = IndexOf<any, never, any>; // never
   * type n2 = IndexOf<any, any, never>; // never
   *
   * type u0 = IndexOf<undefined, any, any>; // never
   * type u1 = IndexOf<any, undefined, any>; // never
   * type u2 = IndexOf<any, any, undefined>; // never
   *
   * type et = IndexOf<[1, 2, 3], 2>; // 1
   * type ef = IndexOf<[1, 2, 3], 4>; // undefined
   */
  export type IndexOf<
    Arr extends any[],
    Item,
    I extends number = 0
  > = Arr extends undefined
    ? never
    : Item extends undefined
    ? never
    : I extends undefined
    ? never
    : IfEnd<Arr, I> extends true
    ? undefined
    : Arr[I] extends Item
    ? I
    : IndexOf<Arr, Item, Increment<I>>;

  /**
   * @summary Test array type has item.
   *
   * ⛔ `undefined` items not supported ⛔
   * @param Arr Tested array type
   * @param Item Tested item type
   * @param IfTrue Result, if `Arr` has `Item`
   * @param IfFalse Result, if `Arr` NOT has `Item`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type n0 = IfHasItem<never, any>; // never
   * type n1 = IfHasItem<any, never>; // never
   *
   * type u0 = IfHasItem<undefined, any>; // never
   * type u1 = IfHasItem<any, undefined>; // never
   *
   * type e = IfHasItem<[1, 2, 3], 2>; // true
   * type n = IfHasItem<[1, 2, 3], 4>; // false
   */
  export type IfHasItem<
    Arr extends any[],
    Item,
    IfTrue = true,
    IfFalse = false
  > = Arr extends undefined
    ? never
    : Item extends undefined
    ? never
    : IndexOf<Arr, Item> extends undefined
    ? IfFalse
    : IfTrue;

  //#region IfEach
  type _IfEach<Arr extends any[], Item, I extends number = 0> = IfEnd<
    Arr,
    I
  > extends true
    ? I extends 0
      ? false
      : true
    : Arr[I] extends Item
    ? _IfEach<Arr, Item, Increment<I>>
    : false;

  /**
   * @summary Test array type each item extends `Item`.
   *
   * ⛔ `undefined` items not supported ⛔
   * @param Arr Tested array type
   * @param Item Tested item type
   * @param IfTrue Result, if each item of `Arr` extends `Item`
   * @param IfFalse Result, if NOT each item of `Arr` extends `Item`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type n0 = IfEach<never, any>; // never
   * type n1 = IfEach<any, never>; // never
   *
   * type u0 = IfEach<undefined, any>; // never
   * type u1 = IfEach<any, undefined>; // never
   *
   * type t = IfEach<[2, 2, 2], 2>; // true
   * type f = IfEach<[1, 2, 3], 3>; // false
   */
  export type IfEach<
    Arr extends any[],
    Item,
    IfTrue = true,
    IfFalse = false
  > = Arr extends undefined
    ? never
    : Item extends undefined
    ? never
    : _IfEach<Arr, Item> extends true
    ? IfTrue
    : IfFalse;

  //#endregion

  //#region CountOf
  type _CountOf<
    Arr extends any[],
    Item,
    R extends number = 0,
    I extends number = 0
  > = IfEnd<Arr, I> extends true
    ? R
    : _CountOf<Arr, Item, Arr[I] extends Item ? Increment<R> : R, Increment<I>>;

  /**
   * @summary Calculate count of items who extends `Item` in array `Arr`.
   *
   * ⛔ `undefined` items not supported ⛔
   * @param Arr Tested array type
   * @param Item Tested item type
   * @returns Count of items who extends `Item` in array
   * @example
   * type n0 = CountOf<never, any>; // never
   * type n1 = CountOf<any, never>; // never
   *
   * type u0 = CountOf<undefined, any>; // never
   * type u1 = CountOf<any, undefined>; // never
   *
   * type r0 = CountOf<[1, 2, 3], 4>; // 0
   * type r1 = CountOf<[1, 2, 3], 3>; // 1
   * type r3 = CountOf<[2, 2, 2], 2>; // 3
   * type z0 = CountOf<[], 4>; // 0
   */
  export type CountOf<Arr extends any[], Item> = Arr extends undefined
    ? never
    : Item extends undefined
    ? never
    : _CountOf<Arr, Item>;
  //#endregion
}
