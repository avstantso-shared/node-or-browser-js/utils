namespace AVStantso.TS.Array {
  //#region KeyFrom
  type _KeyFrom<
    Arr extends Key[],
    R extends Key = never,
    I extends number = 0
  > = IfEnd<Arr, I> extends true ? R : _KeyFrom<Arr, R | Arr[I], Increment<I>>;

  /**
   * @summary Calculate key by array of keys.
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Arr Array of keys
   * @returns Key that includes all array items
   * @example
   * type n = KeyFrom<never>;
   * type u = KeyFrom<undefined>;
   *
   * type k0 = KeyFrom<[]>;
   * type k1 = KeyFrom<['a']>;
   * type k2 = KeyFrom<['a', 1]>;
   */
  export type KeyFrom<Arr extends Key[]> = _KeyFrom<Arr>;
  //#endregion

  type _MapFrom<
    Keys extends Key[],
    Value extends number,
    KeyToKey extends boolean,
    R extends {} = unknown,
    I extends number = 0
  > = IfEnd<Keys, I> extends true
    ? { [K in keyof R]: R[K] }
    : _MapFrom<
        Keys,
        Increment<Value>,
        KeyToKey,
        R &
          (KeyToKey extends true
            ? { [K in Keys[I]]: Value }
            : { [K in Value]: Keys[I] }),
        Increment<I>
      >;

  /**
   * @summary Create key-value map by list of keys and first value
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Keys Array of keys
   * @param FirstValue Value for first item
   * @returns Key-value map structural object
   * @example
   * type n = KeyValueMapFrom<never>; // never
   * type u = KeyValueMapFrom<undefined>; // never
   *
   * type k0 = KeyValueMapFrom<[]>; // {}
   * type k1 = KeyValueMapFrom<['a']>; // {a: 0;}
   * type k2 = KeyValueMapFrom<['a', 1]>; // {a: 0; 1: 1;}
   *
   * type kn = KeyValueMapFrom<['a', 1], never>; // {a: never; 1: never;}
   * type ku = KeyValueMapFrom<['a', 1], undefined>; // {a: undefined; 1: undefined;}
   */
  export type KeyValueMapFrom<
    Keys extends Key[],
    FirstValue extends number = 0
  > = Keys extends undefined ? never : _MapFrom<Keys, FirstValue, true>;

  /**
   * @summary Create value-key map by list of keys and first value
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Keys Array of keys
   * @param FirstValue Value for first item. If is `undefined` or `never` — returns `never`
   * @returns Value-key map structural object
   * @see KeyValueMapFrom
   * @example
   * type n = ValueKeyMapFrom<never>; // never
   * type u = ValueKeyMapFrom<undefined>; // never
   *
   * type k0 = ValueKeyMapFrom<[]>; // {}
   * type k1 = ValueKeyMapFrom<['a']>; // {0: "a";}
   * type k2 = ValueKeyMapFrom<['a', 1]>; // {0: "a"; 1: 1;}
   *
   * type kn = ValueKeyMapFrom<['a', 1], never>; // never
   * type ku = ValueKeyMapFrom<['a', 1], undefined>; // never
   */
  export type ValueKeyMapFrom<
    Keys extends Key[],
    FirstValue extends number = 0
  > = Keys extends undefined
    ? never
    : FirstValue extends undefined
    ? never
    : _MapFrom<Keys, FirstValue, false>;
}
