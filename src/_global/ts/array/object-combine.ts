namespace AVStantso.TS.Array {
  type _ObjectCombine<
    A extends object[],
    R extends object = {},
    I extends number = 0
  > = IfEnd<A, I> extends true
    ? { [K in keyof R]: R[K] }
    : _ObjectCombine<A, R & A[I], Increment<I>>;

  /**
   * @summary Combine array of structural object parts
   * @param A Array to combine
   * @returns Structural object or `never`
   * @example
   * type n = ObjectCombine<never>; // never
   * type u = ObjectCombine<undefined>; // undefined
   *
   * type s = ObjectCombine<[{ a: number }, { b: string }, { c: { x: bigint } }]>; // {a: number; b: string; c: {x: bigint;};}
   *
   * type a1 = ObjectCombine<[]>; // {}
   */
  export type ObjectCombine<A extends object[]> = A extends undefined
    ? A
    : _ObjectCombine<A>;
}
