namespace AVStantso.TS.Array {
  /**
   * @summary Create array type with addition if not exists.
   *
   * ⛔ `undefined` items not supported ⛔
   * @param Arr Array type
   * @param Item Item to add
   * @returns New array type with addition
   * @example
   * type n0 = AddUnique<never, any>; // never
   * type n1 = AddUnique<any, never>; // never
   *
   * type u0 = AddUnique<undefined, any>; // never
   * type u1 = AddUnique<any, undefined>; // never
   *
   * type t = AddUnique<[2, 3], 1>; // [2, 3, 1]
   * type f = AddUnique<[1, 2, 3], 1>; // [1, 2, 3]
   */
  export type AddUnique<Arr extends any[], Item> = Arr extends undefined
    ? never
    : Item extends undefined
    ? never
    : IfHasItem<Arr, Item> extends false
    ? [...Arr, Item]
    : Arr;

  //#region MergeUnique
  type _MergeUnique<
    Arr1 extends any[],
    Arr2 extends any[],
    I extends number = 0
  > = IfEnd<Arr2, I> extends true
    ? Arr1
    : _MergeUnique<
        IfHasItem<Arr1, Arr2[I]> extends true ? Arr1 : [...Arr1, Arr2[I]],
        Arr2,
        Increment<I>
      >;

  /**
   * @summary Create merged array type from two arrays without duplicates from `Arr2`.
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Arr1 Array type
   * @param Arr2 Array type
   * @returns New merged array without duplicates from `Arr2`
   * @example
   * type n0 = MergeUnique<never, any>; // never
   * type n1 = MergeUnique<any, never>; // never
   *
   * type u0 = MergeUnique<undefined, any>; // never
   * type u1 = MergeUnique<any, undefined>; // never
   *
   * type A = MergeUnique<[6, 1, 4], [1, 2, 3, 1, 4, 2, 5]>; // [6, 1, 4, 2, 3, 5]
   */
  export type MergeUnique<
    Arr1 extends any[],
    Arr2 extends any[]
  > = Arr1 extends undefined
    ? never
    : Arr2 extends undefined
    ? never
    : _MergeUnique<Arr1, Arr2>;
  //#endregion

  //#region FilterUnique
  type _FilterUnique<
    Arr extends any[],
    R extends any[] = [],
    I extends number = 0
  > = IfEnd<Arr, I> extends true
    ? R
    : _FilterUnique<
        Arr,
        IfHasItem<R, Arr[I]> extends true ? R : [...R, Arr[I]],
        Increment<I>
      >;

  /**
   * @summary Create array type from array without duplicates. `undefined` items not supported!
   * @param Arr Array type
   * @returns New array without duplicates
   * @example
   * type n = FilterUnique<never>; // never
   * type u = FilterUnique< undefined>; // never
   *
   * type A = FilterUnique<[1, 2, 3, 1, 4, 2, 5]>; // [1, 2, 3, 4, 5]
   */
  export type FilterUnique<Arr extends any[]> = Arr extends undefined
    ? never
    : _FilterUnique<Arr>;
  //#endregion

  //#region Sub
  type _Sub<
    Arr extends any[],
    Start extends number,
    End extends number,
    I extends number = 0,
    R extends any[] = [],
    Ignore extends boolean = I extends Start ? false : true
  > = IfEnd<Arr, I> extends true
    ? R
    : I extends End
    ? R
    : Ignore extends true
    ? _Sub<Arr, Start, End, Increment<I>, R>
    : _Sub<Arr, Start, End, Increment<I>, [...R, Arr[I]], false>;

  type _SubCheck<
    Arr extends any[],
    Start extends number,
    End extends number,
    S extends number = Start extends undefined
      ? 0
      : Numeric.IsNegative<Start, 0, Start>,
    E extends number = End extends undefined
      ? undefined
      : GT<S, End, never, End>
  > = [E] extends [never] ? never : _Sub<Arr, S, End>;

  /**
   * @summary Create sub array type.
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Arr Array type
   * @param Start Start index for copy. `undefined` or negative — from start of array
   * @param End End index for copy. `undefined` — to end of array
   * @returns Array type with copied items types or `never` if `Start > End`
   * @example
   * type n0 = Sub<never, any>; // never
   * type n1 = Sub<any, never>; // never
   * type n2 = Sub<any, any, never>; // never
   * type u = Sub<undefined, any>; // never
   *
   * type A = Sub<[1, 2, 3, 4, 5, 6], 2, 4>; // [3, 4, 5]
   * type S = Sub<[1, 2, 3, 4, 5, 6], undefined, undefined>; // [1, 2, 3, 4, 5, 6]
   *
   * type negaiveS = Sub<[1, 2, 3, 4, 5, 6], -2, 4>; // [1, 2, 3, 4, 5]
   * type EltS = Sub<[1, 2, 3, 4, 5, 6], 4, 2>; // never
   */
  export type Sub<
    Arr extends any[],
    Start extends number = 0,
    End extends number = undefined
  > = Arr extends undefined
    ? never
    : [Start] extends [never]
    ? never
    : [End] extends [never]
    ? never
    : _SubCheck<Arr, Start, Increment<End>>;

  //#endregion

  //#region Remove
  // @ts-ignore Type instantiation is excessively deep and possibly infinite.
  type _Remove<
    Arr extends any[],
    Start extends number,
    End extends number,
    S extends number = Start extends undefined
      ? 0
      : Numeric.IsNegative<Start, 0, Start>,
    E extends number = End extends undefined
      ? undefined
      : GT<S, End, never, End>
  > = [E] extends [never]
    ? never
    : E extends undefined
    ? Sub<Arr, 0, Decrement<S>>
    : // @ts-ignore Type instantiation is excessively deep and possibly infinite.
      [...Sub<Arr, 0, Decrement<S>>, ...Sub<Arr, Increment<E>>];

  /**
   * @summary Create new array type witout removed section.
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Arr Array type
   * @param Start Start index for remove. `undefined` or negative — from start of array
   * @param End End index for remove. `undefined` — to end of array
   * @returns Array type with copied items types, except removed items or `never` if `Start > End`
   * @example
   * type n0 = Remove<never, any>; // never
   * type n1 = Remove<any, never>; // never
   * type n2 = Remove<any, any, never>; // never
   * type u = Remove<undefined, any>; // never
   *
   * type A = Remove<[1, 2, 3, 4, 5, 6], 2, 4>; // [1, 2, 6]
   * type S = Remove<[1, 2, 3, 4, 5, 6], undefined, undefined>; // []
   *
   * type negaiveS = Remove<[1, 2, 3, 4, 5, 6], -2, 4>; // [6]
   * type EltS = Remove<[1, 2, 3, 4, 5, 6], 4, 2>; // never
   */
  export type Remove<
    Arr extends any[],
    Start extends number,
    End extends number = undefined
  > = Arr extends undefined
    ? never
    : [Start] extends [never]
    ? never
    : [End] extends [never]
    ? never
    : _Remove<Arr, Start extends undefined ? 0 : Start, End>;
  //#endregion

  //#region Cast
  type _Cast<
    Arr extends any[],
    Item,
    R extends Item[] = [],
    I extends number = 0
  > = IfEnd<Arr, I> extends true
    ? R
    : _Cast<Arr, Item, [...R, Extract<Arr[I], Item>], Increment<I>>;

  /**
   * @summary Cast all `Arr` items to `Item` type.
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Arr Array to cast
   * @param Item Item type to cast. Defaults `any`
   * @returns Array of `Extract<Arr[I], Item>`
   * @example
   * type n = Cast<never, any>; // never
   * type u = Cast<undefined, any>; // never
   *
   * type a0 = Cast<[1, 2, 3]>; // [1, 2, 3]
   * type a1 = Cast<[1, 2, 3], number>; // [1, 2, 3]
   * type a2 = Cast<[1, 2, 3], string>; //[never, never, never]
   */
  export type Cast<Arr extends any[], Item = any> = Arr extends undefined
    ? never
    : _Cast<Arr, Item>;
  //#endregion
}
