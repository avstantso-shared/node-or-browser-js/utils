namespace AVStantso.TS.Array {
  type _MinMax<
    Arr extends string[] | number[],
    IsMin extends boolean,
    ReturnIdx extends boolean,
    R extends number = 0,
    I extends number = 1
  > = IfEnd<Arr, I> extends true
    ? IfEnd<Arr, 0> extends true
      ? undefined
      : ReturnIdx extends true
      ? R
      : Arr[R]
    : _MinMax<
        Arr,
        IsMin,
        ReturnIdx,
        IsMin extends true
          ? LT<Arr[I], Arr[R], I, R>
          : GT<Arr[I], Arr[R], I, R>,
        Increment<I>
      >;

  //#region Min
  export namespace Min {
    /**
     * @summary Find array minimal item index.
     *
     * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
     * @param Arr Array to find
     * @returns Minimal item index. `undefined` for empty array
     * @example
     * type n = Min.Index<never>; // never
     * type u = Min.Index<undefined>; // never
     *
     * type z = Min.Index<[]>; // undefined
     *
     * type n0 = Min.Index<[1]>; // 0
     * type n1 = Min.Index<[2, 1]>; // 1
     *
     * type s0 = Min.Index<['a']>; // 0
     * type s1 = Min.Index<['b', 'a']>; // 1
     */
    export type Index<Arr extends string[] | number[]> = Arr extends undefined
      ? never
      : _MinMax<Arr, true, true>;
  }

  /**
   * @summary Find array minimal item.
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Arr Array to find
   * @returns Minimal item. `undefined` for empty array
   * @example
   * type n = Min<never>; // never
   * type u = Min<undefined>; // never
   *
   * type z = Min<[]>; // undefined
   *
   * type n0 = Min<[1]>; // 1
   * type n1 = Min<[2, 1]>; // 1
   *
   * type s0 = Min<['a']>; // "a"
   * type s1 = Min<['b', 'a']>; // "a"
   */
  export type Min<Arr extends string[] | number[]> = Arr extends undefined
    ? never
    : _MinMax<Arr, true, false>;
  //#endregion

  //#region Max
  export namespace Max {
    /**
     * @summary Find array maximal item index.
     *
     * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
     * @param Arr Array to find
     * @returns Maximal item index. `undefined` for empty array
     * @example
     * type n = Max.Index<never>; // never
     * type u = Max.Index<undefined>; // never
     *
     * type z = Max.Index<[]>; // undefined
     *
     * type r0 = Max.Index<[1]>; // 0
     * type r1 = Max.Index<[2, 1]>; // 0
     *
     * type s0 = Max.Index<['a']>; // 0
     * type s1 = Max.Index<['b', 'a']>; // 0
     */
    export type Index<Arr extends string[] | number[]> = Arr extends undefined
      ? never
      : _MinMax<Arr, false, true>;
  }

  /**
   * @summary Find array maximal item.
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Arr Array to find
   * @returns Maximal item. `undefined` for empty array
   * @example
   * type n = Max<never>; // never
   * type u = Max<undefined>; // never
   *
   * type z = Max<[]>; // undefined
   *
   * type r0 = Max<[1]>; // 1
   * type r1 = Max<[2, 1]>; // 2
   *
   * type s0 = Max<['a']>; // "a"
   * type s1 = Max<['b', 'a']>; // "b"
   */
  export type Max<Arr extends string[] | number[]> = Arr extends undefined
    ? never
    : _MinMax<Arr, false, false>;
  //#endregion

  //#region Sort
  type _Sort<
    Arr extends any[],
    Asc extends boolean,
    L extends number,
    R extends any[] = [],
    RL extends number = 0,
    M extends number = _MinMax<Arr, Asc, true>
  > = RL extends L
    ? R
    : // @ts-ignore: Type instantiation is excessively deep and possibly infinite.ts(2589)
      _Sort<Remove<Arr, M, M>, Asc, L, [...R, Arr[M]], Increment<RL>>;

  // @ts-ignore: Type instantiation is excessively deep and possibly infinite.ts(2589)
  type _SortCheck<
    Arr extends any[],
    Asc extends boolean,
    L extends number = Length<Arr>
  > = Arr extends undefined
    ? never
    : LTE<L, 1> extends true
    ? Arr
    : _Sort<Arr, Asc, L>;

  /**
   * @summary Sort array items.
   *
   * If `Asc` is `true` — ascending, else — descending
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Arr Array to sort
   * @param Asc Is ascending sort order
   * @example
   * type n = Sort<never>; // never
   * type u = Sort<undefined>; // never
   *
   * type dn = [1, 4, 5, 3, 2, 78, 1];
   * type n_asc = Sort<dn>; // [1, 1, 2, 3, 4, 5, 78]
   * type n_desc = Sort<dn, false>; // [78, 5, 4, 3, 2, 1, 1]
   *
   * type ds = ['abc', 'eb', '', '1', 'ab', ' '];
   * type s_asc = Sort<ds>; // ["", " ", "1", "ab", "abc", "eb"]
   * type s_desc = Sort<ds, false>; // ["eb", "abc", "ab", "1", " ", ""]
   */
  export type Sort<
    Arr extends string[] | number[],
    Asc extends boolean = true
  > =
    // @ts-ignore: Type instantiation is excessively deep and possibly infinite.ts(2589)
    _SortCheck<Arr, Asc>;

  export namespace Sort {
    /**
     * @summary Sort array items descending.
     *
     * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
     * @param Arr Array to sort
     * @example
     * type n = Sort.Desc<never>; // never
     * type u = Sort.Desc<undefined>; // never
     *
     * type dn = [1, 4, 5, 3, 2, 78, 1];
     * type n_desc = Sort.Desc<dn>; // [78, 5, 4, 3, 2, 1, 1]
     *
     * type ds = ['abc', 'eb', '', '1', 'ab', ' '];
     * type s_desc = Sort.Desc<ds>; // ["eb", "abc", "ab", "1", " ", ""]
     */
    export type Desc<Arr extends string[] | number[]> = _SortCheck<Arr, false>;
  }
  //#endregion
}
