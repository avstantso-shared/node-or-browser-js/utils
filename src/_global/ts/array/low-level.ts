namespace AVStantso.TS.Array {
  /**
   * @summary Test array `I`-item out of bounds.
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Arr Tested array type
   * @param I Index
   * @param IfTrue Result, if `Arr` NOT  has `I` item
   * @param IfFalse Result, if `Arr` has `I` item
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type n0 = IfEnd<never, any>; // never
   * type n1 = IfEnd<any, never>; // never
   *
   * type u0 = IfEnd<undefined, any>; // never
   * type u1 = IfEnd<any, undefined>; // never
   *
   * type t = IfEnd<[], 0>; // true
   * type f = IfEnd<[1], 0>; // false
   */
  export type IfEnd<
    Arr extends any[],
    I extends number,
    IfTrue = true,
    IfFalse = false
  > = Arr extends undefined
    ? never
    : I extends undefined
    ? never
    : Arr[I] extends undefined
    ? IfTrue
    : IfFalse;

  //#region Length
  type _Length<
    Arr extends any[],
    I extends number = 0
  > = Arr[I] extends undefined ? I : _Length<Arr, Increment<I>>;

  /**
   * @summary Array type length.
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Arr Array type
   * @returns Length of `Arr` type
   * @example
   * type n = Length<never>; // never
   * type u = Length<undefined>; // never
   *
   * type r0 = Length<[]>; // 0
   * type r1 = Length<['A']>; // 1
   * type r2 = Length<['A' , 1]>; // 2
   */
  export type Length<Arr extends any[]> = Arr extends undefined
    ? never
    : _Length<Arr>;

  export namespace Length {
    /**
     * @summary Test array type length.
     *
     * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
     * @param Arr Tested array type
     * @param L Length limit
     * @param IfTrue Result, if `Arr` length exeeded `L`
     * @param IfFalse Result, if `Arr` length NOT exeeded `L`
     * @returns `IfFalse` or `IfTrue` param
     * @example
     * type n0 = Length.IfExeeded<never, any>; // never
     * type n1 = Length.IfExeeded<any, never>; // never
     *
     * type u0 = Length.IfExeeded<undefined, any>; // never
     * type u1 = Length.IfExeeded<any, undefined>; // never
     *
     * type r0f = Length.IfExeeded<[], 0>; // false
     * type r1t = Length.IfExeeded<['A'], 0>; // true
     * type r1f = Length.IfExeeded<['A'], 1>; // false
     * type r2t = Length.IfExeeded<['A', 1], 1>; // true
     * type r2f = Length.IfExeeded<['A', 1], 600>; // false
     */
    export type IfExeeded<
      Arr extends any[],
      L extends number,
      IfTrue = true,
      IfFalse = false
    > = Arr extends undefined
      ? never
      : L extends undefined
      ? never
      : Arr[L] extends undefined
      ? IfFalse
      : IfTrue;
  }
  //#endregion
}
