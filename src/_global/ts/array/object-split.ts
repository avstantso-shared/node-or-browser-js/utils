namespace AVStantso.TS.Array {
  type _ObjectSplit<
    O extends object,
    Keys extends (keyof O)[],
    R extends any[] = [],
    I extends number = 0
  > = IfEnd<Keys, I> extends true
    ? R
    : _ObjectSplit<
        O,
        Keys,
        [...R, { [K in Keys[I]]: O[Keys[I]] }],
        Increment<I>
      >;

  /**
   * @summary Split structural object to array of objects with one field in each
   * @param O Object to split
   * @returns Array of objects for each key or `never` if not structural object passed as `O`
   * @example
   * type n = ObjectSplit<never>; // never
   * type u = ObjectSplit<undefined>; // undefined
   *
   * type s = ObjectSplit<{ a: number; b: string; c: { x: bigint } }>; // [{a: number;}, {b: string;}, {c: {x: bigint;};}]
   *
   * type a1 = ObjectSplit<[]>; // never
   * type a2 = ObjectSplit<[1, 2, 3]>; // never
   */
  export type ObjectSplit<O extends object> = O extends undefined
    ? undefined
    : IfStructure<O> extends true
    ? _ObjectSplit<O, KeyList<O>>
    : never;
}
