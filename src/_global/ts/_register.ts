namespace AVStantso {
  /**
   * @summary TypeScript helpers
   */
  export namespace TS {
    /**
     * @summary TypeScript helpers for arrays
     */
    export namespace Array {}

    /**
     * @summary TypeScript helpers for string
     */
    export namespace String {}
  }

  export namespace Code {
    /**
     * @summary TypeScript helpers utility
     */
    export interface TS {}
  }

  export interface Code {
    /**
     * @summary TypeScript helpers utility
     */
    TS: Code.TS;
  }

  export const TS = avstantso._reg('TS', {});
}
