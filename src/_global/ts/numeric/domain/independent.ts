namespace AVStantso.TS.Numeric {
  /**
   * @summary Test a number is a positive number
   * @param N Tested number
   * @param IfTrue Result, if `N` is a positive number
   * @param IfFalse Result, if `N` is a negative number or `0` or `undefined`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type p = IsPositive<1>; // true
   * type z = IsPositive<0>; // false
   * type n = IsPositive<-1>; // false
   */
  export type IsPositive<
    N extends number,
    IfTrue = true,
    IfFalse = false
  > = N extends undefined
    ? IfFalse
    : N extends 0
    ? IfFalse
    : `${N}` extends `-${infer S}`
    ? IfFalse
    : IfTrue;

  /**
   * @summary Test a number is a negative number
   * @param N Tested number
   * @param IfTrue Result, if `N` is a negative number
   * @param IfFalse Result, if `N` is a positive number or `0` or `undefined`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type p = IsNegative<1>; // false
   * type z = IsNegative<0>; // false
   * type n = IsNegative<-1>; // true
   */
  export type IsNegative<
    N extends number,
    IfTrue = true,
    IfFalse = false
  > = N extends undefined
    ? IfFalse
    : N extends 0
    ? IfFalse
    : `${N}` extends `-${infer S}`
    ? IfTrue
    : IfFalse;

  /**
   * @summary With `N` param returns sign of `N`.
   *
   * Without `N` param returns sign union `-1 | 0 | 1`
   * @param N Number
   * @example
   * type s = Sign; // -1 | 0 | 1
   * type p = Sign<10>; // 1
   * type z = Sign<0>; // 0
   * type n = Sign<-10>; // -1
   */
  export type Sign<N extends number = never> = [N] extends [never]
    ? -1 | 0 | 1
    : N extends 0
    ? 0
    : IsNegative<N> extends false
    ? 1
    : -1;

  /**
   * @summary Digits
   */
  export namespace Digits {
    /**
     * @summary Is odd digit map
     */
    export type IsOdd = {
      '0': false;
      '1': true;
      '2': false;
      '3': true;
      '4': false;
      '5': true;
      '6': false;
      '7': true;
      '8': false;
      '9': true;
    };
  }

  type Digits = keyof Digits.IsOdd;

  type _IsOdd<S extends string> = S extends `${infer C}${infer E}`
    ? E extends ''
      ? Digits.IsOdd[Extract<C, keyof Digits.IsOdd>]
      : _IsOdd<E>
    : never;

  /**
   * @summary Test a number is a odd number
   * @param N Tested number
   * @param IfTrue Result, if `N` is a odd number
   * @param IfFalse Result, if `N` is a even number or `0` or `undefined`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type o = IsOdd<11>; // true
   * type e = IsOdd<32>; // false
   */
  export type IsOdd<
    N extends number,
    IfTrue = true,
    IfFalse = false
  > = N extends undefined
    ? IfFalse
    : (`${N}` extends `-${infer S}` ? _IsOdd<S> : _IsOdd<`${N}`>) extends true
    ? IfTrue
    : IfFalse;

  /**
   * @summary Test a number is a even number
   * @param N Tested number
   * @param IfTrue Result, if `N` is a even number
   * @param IfFalse Result, if `N` is a odd number or `0` or `undefined`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type o = IsEven<11>; // false
   * type e = IsEven<32>; // true
   */
  export type IsEven<
    N extends number,
    IfTrue = true,
    IfFalse = false
  > = N extends undefined
    ? IfFalse
    : (`${N}` extends `-${infer S}` ? _IsOdd<S> : _IsOdd<`${N}`>) extends true
    ? IfFalse
    : IfTrue;

  type _IsNumber<S extends string> = S extends ''
    ? false
    : S extends `${infer C}${infer E}`
    ? C extends Digits
      ? E extends ''
        ? true
        : _IsNumber<E>
      : false
    : never;

  /**
   * @summary Test a string is a number in string
   * @param S Tested string
   * @param IfTrue Result, if `S` is a number in string
   * @param IfFalse Result, if `S` is NOT a number in string or `undefined`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type pn = IsNumber<'1000'>; // true
   * type s = IsNumber<'abc'>; // false
   * type nn = IsNumber<'-1'>; // true
   * type nnn = IsNumber<'--1'>; // false
   */
  export type IsNumber<
    S extends string,
    IfTrue = true,
    IfFalse = false
  > = S extends undefined
    ? IfFalse
    : (`${S}` extends `-${infer V}` ? _IsNumber<V> : _IsNumber<S>) extends true
    ? IfTrue
    : IfFalse;
}
