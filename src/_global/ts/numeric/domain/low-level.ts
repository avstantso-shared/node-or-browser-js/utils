namespace AVStantso.TS {
  /**
   * @summary Numeric types and "functions"
   */
  export namespace Numeric {
    /**
     * @summary Domain of a functions of `Numeric` type
     */
    export namespace Domain {
      /**
       * @summary Numeric literals `1-max(Domain.Positive)`.
       *
       * Used in `Numeric` type "functions"
       *
       * For full ASCII need `255`
       */
      export namespace Positive {
        /**
         * @summary Get negative of absolute value of numeric literal.
         *
         * Negative numbers is ignored.
         */
        export type ToNegative<
          N extends number,
          R extends number = Transformation.Get<N>['n']
        > = [R] extends [never] ? number : R;
      }

      /**
       * @summary Numeric literals `-1-(-max(Domain.Negative))`.
       *
       * Used in `Numeric` type "functions"
       */
      export namespace Negative {
        /**
         * @summary Get absolute value of negative numeric literal.
         *
         * Positive numbers is ignored.
         *
         * ⚠ Is low-level type! Use `Numeric.Abs` instead
         */
        export type ToPositive<
          N extends number,
          R extends number = `${N}` extends `-${infer P}`
            ? Transformation.Get<P>['p']
            : never
        > = [R] extends [never] ? number : R;
      }

      /**
       * @summary `Positive ⇔ Negative` transformation map.
       *
       * Used in `Numeric` type "functions"
       */
      export namespace Transformation {
        /**
         * @summary Get transformation info by key `K`
         */
        export type Get<K extends number | string> = Transformation[Extract<
          `${K}`,
          keyof Transformation
        >];
      }

      /**
       * @summary Power map.
       *
       * Used in `Numeric.Power` type "functions"
       */
      export namespace Power {}

      type _Parse<
        S extends string,
        R extends number = `${S}` extends `-${infer V}`
          ? Transformation.Get<V>['n']
          : Transformation.Get<S>['p']
      > = [R] extends [never] ? number : R;

      /**
       * @summary Parse string `S` for extract number from it
       * @example
       * type pn = Parse<'1000'>; // 1000
       * type s = IsNumber<'abc'>; // never
       * type nn = IsNumber<'-1'>; // -1
       * type nnn = IsNumber<'--1'>; // never
       * type m = IsNumber<'1000000000000000'>; // number
       */
      export type Parse<S extends string> = S extends undefined
        ? undefined
        : IsNumber<S> extends false
        ? never
        : S extends '0'
        ? 0
        : _Parse<S>;
    }
  }
}
