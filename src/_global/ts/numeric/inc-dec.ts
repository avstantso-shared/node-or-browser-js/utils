namespace AVStantso.TS {
  /**
   * @summary Increment numeric literal inside `Numeric.Domain`
   *
   * ⚠ Result defined on `Numeric.Domain`
   * @see Source: https://stackoverflow.com/questions/54243431/how-can-i-produce-an-incremented-version-of-a-numeric-literal-type-in-typescript
   * @example
   * type p = Increment<1>; // 2
   * type z = Increment<0>; // 1
   * type n = Increment<-1>; // 0
   */
  export type Increment<N extends number> = N extends undefined
    ? undefined
    : Numeric.IsNegative<N> extends false
    ? [
        ...Numeric.Domain.Positive,
        ...number[] // bail out with number
      ][N]
    : [
        1,
        0,
        ...Numeric.Domain.Negative,
        ...number[] // bail out with number
      ][Numeric.Domain.Negative.ToPositive<N>];

  /**
   * @summary Decrement numeric literal inside `Numeric.Domain`
   *
   * ⚠ Result defined on `Numeric.Domain`
   * @example
   * type p = Decrement<1>; // 0
   * type z = Decrement<0>; // -1
   * type n = Decrement<-1>; // -2
   * @see Increment
   */
  export type Decrement<N extends number> = N extends undefined
    ? undefined
    : Numeric.IsNegative<N> extends false
    ? [
        -1,
        0,
        ...Numeric.Domain.Positive,
        ...number[] // bail out with number
      ][N]
    : [
        ...Numeric.Domain.Negative,
        ...number[] // bail out with number
      ][Numeric.Domain.Negative.ToPositive<N>];
}
