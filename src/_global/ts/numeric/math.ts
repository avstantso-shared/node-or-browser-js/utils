namespace AVStantso.TS.Numeric {
  /**
   * @summary Absolute value of numeric literal.
   *
   * ⚠ Result defined on `Numeric.Domain`
   * @example
   * type p = Abs<1>; // 1
   * type z = Abs<0>; // 0
   * type n = Abs<-1>; // 1
   */
  export type Abs<N extends number> = IsNegative<N> extends false
    ? N
    : Domain.Negative.ToPositive<N>;

  /**
   * @summary Invert value of numeric literal. `Positive ⇔ Negative`
   *
   * ⚠ Result defined on `Numeric.Domain`
   * @example
   * type p = Invert<1>; // -1
   * type z = Invert<0>; // 0
   * type n = Invert<-1>; // 1
   */
  export type Invert<N extends number> = IsNegative<N> extends false
    ? Domain.Positive.ToNegative<N>
    : Domain.Negative.ToPositive<N>;

  /**
   * @summary Calculate `X + Y`.
   *
   * ⚠ Result defined on `Numeric.Domain`
   * @example
   * type p = Summ<8, 3>; // 11
   * type z = Summ<0, 3>; // 3
   * type n = Summ<-8, 3>; // -5
   */
  export type Summ<X extends number, Y extends number> = X extends 0
    ? Y
    : Y extends 0
    ? X
    : IsNegative<Y> extends true
    ? Summ<Decrement<X>, Increment<Y>>
    : Summ<Increment<X>, Decrement<Y>>;

  /**
   * @summary Calculate `X - Y`.
   *
   * ⚠ Result defined on `Numeric.Domain`
   * @example
   * type p = Diff<8, 3>; // 5
   * type z = Diff<0, 3>; // -3
   * type n = Diff<-8, 3>; // -11
   */
  export type Diff<X extends number, Y extends number> = Summ<X, Invert<Y>>;

  type _Multiply<
    X extends number,
    Y extends number,
    R extends number = 0
  > = Y extends 0
    ? R
    : // @ts-ignore Type instantiation is excessively deep and possibly infinite.ts(2589)
      _Multiply<X, Decrement<Y>, Summ<X, R>>;

  /**
   * @summary Calculate `X * Y`.
   *
   * ⚠ Result defined on `[-998, 998]`.
   *
   * ⚠ Out of this range: `Type instantiation is excessively deep and possibly infinite.ts(2589)`
   * @example
   * type p = Multiply<8, 3>; // 24
   * type z = Multiply<0, 3>; // 0
   * type n = Multiply<-9, 3>; // -27
   */
  export type Multiply<X extends number, Y extends number> = X extends 0
    ? 0
    : Y extends 0
    ? 0
    : IsNegative<X> extends true
    ? IsNegative<Y> extends true
      ? _Multiply<Abs<X>, Abs<Y>>
      : _Multiply<X, Y>
    : _Multiply<Y, X>;

  type _DivResult<
    S extends Sign,
    Q extends number,
    M extends number
  > = S extends -1 ? { Q: Invert<Q>; M: Invert<M> } : { Q: Q; M: M };

  type _Div<
    X extends number,
    Y extends number,
    S extends Sign,
    Q extends number = 0
  > = X extends Y
    ? _DivResult<S, Increment<Q>, 0>
    : LT<X, Y> extends true
    ? _DivResult<S, Q, X>
    : // @ts-ignore Type instantiation is excessively deep and possibly infinite.ts(2589)
      _Div<Diff<X, Y>, Y, S, Increment<Q>>;

  /**
   * @summary Calculate `X / Y`.
   *
   * ⚠ Result defined on `[-998, 998]`.
   *
   * ⚠ Out of this range: `Type instantiation is excessively deep and possibly infinite.ts(2589)`
   * @example
   * type x = Div<8, 0>; // never
   * type z = Div<0, 3>; // 0
   *
   * type p_1_1 = Div<1, 1>; // {Q: 1; M: 0;}
   * type p_1_2 = Div<2, 1>; // {Q: 2; M: 0;}
   * type p_1_3 = Div<3, 1>; // {Q: 3; M: 0;}
   *
   * type p_8_1 = Div<8, 1>; // {Q: 8; M: 0;}
   * type p_8_2 = Div<8, 2>; // {Q: 4; M: 0;}
   * type p_8_3 = Div<8, 3>; // {Q: 2; M: 2;}
   *
   * type n_m34_7 = Div<-34, 7>; // {Q: -4; M: -6;}
   * type n_34_m7 = Div<-34, 7>; // {Q: -4; M: -6;}
   * type n_m34_m7 = Div<-34, -7>; // {Q: 4; M: 6;}
   */
  export type Div<X extends number, Y extends number> = X extends 0
    ? 0
    : Y extends 0
    ? never
    : IsNegative<X> extends true
    ? IsNegative<Y> extends true
      ? _Div<Abs<X>, Abs<Y>, 1>
      : _Div<Abs<X>, Abs<Y>, -1>
    : _Div<X, Y, 1>;

  namespace _Power {
    type _Calc<
      X extends number,
      E extends number,
      R extends number = X
    > = E extends 1
      ? R
      : // @ts-ignore Type instantiation is excessively deep and possibly infinite.ts(2589)
        _Calc<X, Decrement<E>, Multiply<R, X>>;

    /**
     * @summary Calculate `X ^ E`. If `E` is negotive — `never`
     *
     * ⚠ Result defined on `[-998, 998]`.
     *
     * ⚠ Out of this range: `Type instantiation is excessively deep and possibly infinite.ts(2589)`
     * @deprecated Not effective
     */
    export type Calc<X extends number, E extends number> = X extends 0
      ? 0
      : E extends 0
      ? 1
      : IsNegative<E> extends true
      ? never
      : _Calc<X, Abs<E>>;

    type _FromMap<
      X extends number,
      E extends number,
      XK extends Extract<`${Abs<X>}`, keyof Domain.Power> = Extract<
        `${Abs<X>}`,
        keyof Domain.Power
      >,
      XD extends Domain.Power[XK] = Domain.Power[XK],
      EK extends Extract<`${E}`, keyof XD> = Extract<`${E}`, keyof XD>,
      ED extends XD[EK] = XD[EK]
    > = IsNegative<X> extends true
      ? IsOdd<E> extends true
        ? Invert<Extract<ED, number>>
        : ED
      : ED;
    // {
    //   N: IsNegative<X>;
    //   O: IsOdd<E>;
    //   NO: IsNegative<X> & IsOdd<E>;
    //   IED: Invert<Extract<ED, number>>;
    //   ED: ED;
    // };

    // IsNegative<X> & IsOdd<E> extends true
    //   ? Invert<Extract<ED, number>>
    //   : ED;

    /**
     * @summary Calculate `X ^ E`. If `E` is negotive — `never`
     *
     * ⚠ Result defined on `Numeric.Domain`.
     *
     * ⚠ Out of this range: `Type instantiation is excessively deep and possibly infinite.ts(2589)`
     */
    export type FromMap<X extends number, E extends number> = X extends 0
      ? 0
      : E extends 0
      ? 1
      : E extends 1
      ? X
      : _FromMap<X, E>;
  }

  /**
   * @summary Calculate `X ^ E`. If `E` is negotive — `never`
   *
   * ⚠ Result defined on `Numeric.Domain`.
   * @example
   * type zx = Power<0, 10>; // 0
   * type ze = Power<10, 0>; // 1
   *
   * type p2_1 = Power<2, 1>; // 2
   * type p2_2 = Power<2, 2>; // 4
   * type p2_9 = Power<2, 9>; // 512
   * type p2_12 = Power<2, 12>; // 4096
   *
   * type p3_1 = Power<3, 1>; // 3
   * type p3_2 = Power<3, 2>; // 9
   * type p3_6 = Power<3, 6>; // 729
   * type p3_7 = Power<3, 7>; // 2187
   *
   * type n2_1 = Power<-2, 1>; // -2
   * type n2_2 = Power<-2, 2>; // 4
   * type n2_9 = Power<-2, 9>; // -512
   * type n2_12 = Power<-2, 12>; // 4096
   *
   * type n3_1 = Power<-3, 1>; // -3
   * type n3_2 = Power<-3, 2>; // 9
   * type n3_6 = Power<-3, 6>; // 729
   * type n3_7 = Power<-3, 7>; // -2187
   */
  export type Power<X extends number, E extends number> = _Power.FromMap<X, E>;

  export namespace Power {
    /**
     * @summary Select map of powers of `X`
     * @example
     * type s = Select<2>;
     * type in3 = s[3]; // 8
     */
    export type Select<X extends number> = {
      '0': 1;
      '1': X;
    } & Domain.Power[Extract<`${X}`, keyof Domain.Power>];
  }
}
