namespace AVStantso.TS {
  type Args<
    A extends number | string = number | string,
    B extends number | string = number | string,
    T = unknown,
    F = unknown,
    R extends {
      A: number | string;
      B: number | string;
      T: unknown;
      F: unknown;
    } = { A: A; B: B; T: T; F: F }
  > = R;

  namespace Number {
    export type _LT<Ag extends Args, I extends number = 0> = I extends Ag['A']
      ? Ag['T']
      : I extends Ag['B']
      ? Ag['F']
      : _LT<Ag, Increment<I>>;
  }

  namespace String {
    export type _LT<Ag extends Args> = Ag['A'] extends `${infer AC}${infer AE}`
      ? Ag['B'] extends `${infer BC}${infer BE}`
        ? AC extends BC
          ? _LT<Args<AE, BE, Ag['T'], Ag['F']>>
          : Number._LT<Args<ASCII.Code<AC>, ASCII.Code<BC>, Ag['T'], Ag['F']>>
        : Ag['F']
      : Ag['T'];
  }

  type _LT<Ag extends Args, Eq extends boolean> = Ag['A'] extends Ag['B']
    ? Eq extends true
      ? Ag['T']
      : Ag['F']
    : Ag['A'] extends number
    ? Ag['B'] extends string
      ? never
      : Number._LT<Ag>
    : Ag['B'] extends number
    ? never
    : String._LT<Ag>;

  /**
   * @summary Test `A < B`
   * @param A argument for comparision
   * @param B argument for comparision
   * @param IfTrue Result, if `A < B`
   * @param IfFalse Result, if `A >= B`
   * @returns `IfFalse` or `IfTrue` param
   */
  export type LT<
    A extends number | string,
    B extends number | string,
    IfTrue = true,
    IfFalse = false
  > = _LT<Args<A, B, IfTrue, IfFalse>, false>;

  /**
   * @summary Test `A <= B`
   * @param A argument for comparision
   * @param B argument for comparision
   * @param IfTrue Result, if `A <= B`
   * @param IfFalse Result, if `A > B`
   * @returns `IfFalse` or `IfTrue` param
   */
  export type LTE<
    A extends number | string,
    B extends number | string,
    IfTrue = true,
    IfFalse = false
  > = _LT<Args<A, B, IfTrue, IfFalse>, true>;

  /**
   * @summary Test `A > B`
   * @param A argument for comparision
   * @param B argument for comparision
   * @param IfTrue Result, if `A > B`
   * @param IfFalse Result, if `A <= B`
   * @returns `IfFalse` or `IfTrue` param
   */
  export type GT<
    A extends number | string,
    B extends number | string,
    IfTrue = true,
    IfFalse = false
  > = _LT<Args<B, A, IfTrue, IfFalse>, false>;

  /**
   * @summary Test `A >= B`
   * @param A argument for comparision
   * @param B argument for comparision
   * @param IfTrue Result, if `A >= B`
   * @param IfFalse Result, if `A < B`
   * @returns `IfFalse` or `IfTrue` param
   */
  export type GTE<
    A extends number | string,
    B extends number | string,
    IfTrue = true,
    IfFalse = false
  > = _LT<Args<B, A, IfTrue, IfFalse>, true>;
}
