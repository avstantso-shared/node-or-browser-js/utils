namespace AVStantso.TS {
  /**
   * @summary Object key type
   */
  export type Key = string | number | symbol;
}
