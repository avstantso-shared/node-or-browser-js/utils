namespace AVStantso.TS {
  /**
   * @summary English letters
   */
  export namespace Letters {
    export type Upper = Array.Sub<ASCII, 65, 90>;
    export type Lower = Array.Sub<ASCII, 97, 122>;
  }

  export type Letters<IsUpper extends boolean = true> = IsUpper extends true
    ? Letters.Upper
    : Letters.Lower;
}
