namespace AVStantso.TS {
  /**
   * @summary If `Condition` is `true` returns `IfTrue` else `IfFalse`
   * @param Condition Condition
   * @param IfTrue Result, if `Condition` is `true`
   * @param IfFalse Result, if `Condition` is NOT `true`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type nv = If<never>; // never
   * type u = If<undefined>; // false
   * type nl = If<null>; // false
   * type f = If<false>; // false
   * type t = If<true>; // true
   * type a = If<any>; // boolean
   */
  export type If<
    Condition extends boolean,
    IfTrue = true,
    IfFalse = false
  > = Condition extends undefined
    ? IfFalse
    : Condition extends true
    ? IfTrue
    : IfFalse;

  export namespace If {
    /**
     * @summary If `Left` extends `Right` returns `IfTrue` else `IfFalse`
     * @param Left Left value
     * @param Right Right value
     * @param IfTrue Result, if `Left` extends `Right`
     * @param IfFalse Result, if `Left` NOT extends `Right`
     * @returns `IfFalse` or `IfTrue` param
     */
    export type Extends<
      Left,
      Right,
      IfTrue = true,
      IfFalse = false
    > = Left extends Right ? IfTrue : IfFalse;
  }

  /**
   * @summary If all `Conditions` is `true` returns `IfTrue` else `IfFalse`
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Conditions Conditions array
   * @param IfTrue Result, if all `Conditions` is `true`
   * @param IfFalse Result, if any item of `Conditions` is NOT `true`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type n = And<never>; // never
   * type u = And<undefined>; // never
   * type f = And<[true, true, false]>; // false
   * type t = And<[true, true, true]>; // true
   */
  export type And<
    Conditions extends boolean[],
    IfTrue = true,
    IfFalse = false
  > = Array.IfEach<Conditions, true, IfTrue, IfFalse>;

  /**
   * @summary If one or more `Conditions` is `true` returns `IfTrue` else `IfFalse`
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Conditions Conditions array
   * @param IfTrue Result, if any item of `Conditions` is `true`
   * @param IfFalse Result, if all `Conditions` is NOT `true`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type n = Or<never>; // never
   * type u = Or<undefined>; // never
   * type f = Or<[false, false, false]>; // false
   * type t = Or<[true, true, false]>; // true
   */
  export type Or<
    Conditions extends boolean[],
    IfTrue = true,
    IfFalse = false
  > = Array.IfHasItem<Conditions, true, IfTrue, IfFalse>;

  /**
   * @summary If only one item of `Conditions` is `true` returns `IfTrue` else `IfFalse`
   *
   * ⛔ First `undefined` item (if exists) will be recognized as end of array ⛔
   * @param Conditions Conditions array
   * @param IfTrue Result, if only one item of `Conditions` is `true`
   * @param IfFalse Result, if each item of `Conditions` is NOT `true` or more one item of `Conditions` is `true`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type n = Xor<never>; // never
   * type u = Xor<undefined>; // never
   * type fff = Xor<[false, false, false]>; // false
   * type ttf = Xor<[true, true, false]>; // false
   * type tff = Xor<[true, false, false]>; // true
   * type ftf = Xor<[false, true, false]>; // true
   * type fft = Xor<[false, false, true]>; // true
   * type ttt = Xor<[true, true, false]>; // false
   */
  export type Xor<
    Conditions extends boolean[],
    IfTrue = true,
    IfFalse = false
  > = Conditions extends undefined
    ? never
    : Array.CountOf<Conditions, true> extends 1
    ? IfTrue
    : IfFalse;
}
