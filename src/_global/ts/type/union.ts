namespace AVStantso.TS.Type {
  type _Union<
    TOrDef,
    TLiteral extends Union.Constraint<TOrDef>,
    Debug extends boolean,
    L extends Literal = TOrDef extends Def
      ? IfDefKey<'L', Def, TOrDef, never>
      : TLiteral,
    O = TOrDef extends Def ? IfDefKey<'T', Def, TOrDef> : TOrDef,
    R = HasString<O, O, O | L>
  > = Debug extends true
    ? { TOrDef: TOrDef; TLiteral: TLiteral; L: L; O: O; R: R }
    : R;

  /**
   * @summary Type union `T` with `Type.Literal`.
   * With type `string` literals NOT supported
   * @example
   * Union<never>; // never
   *
   * // ⛔ literals NOT supported:
   * Union<string | 'bigint'>; // string
   *
   * Union<number, 'string' | 'bigint'>; // number | "string" | "bigint"
   *
   * Union<{ T: Function; L: 'boolean' }>; // "boolean" | Function
   *
   * Union<object>; // object | "string" | "number" | "bigint" | "boolean" | "symbol" | "undefined" | "object" | "function"
   */
  export type Union<
    TOrDef,
    TLiteral extends Union.Constraint<TOrDef> = Union.Constraint<TOrDef>
  > = _Union<TOrDef, TLiteral, false>;

  export namespace Union {
    /**
     * @summary Constraint for `TS.Type.Union` second type parameter
     */
    export type Constraint<TOrDef> = Def.Is<TOrDef, never, Literal>;
  }
}
