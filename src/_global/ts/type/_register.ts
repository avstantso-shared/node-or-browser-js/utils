namespace AVStantso {
  export namespace Code {
    export namespace TS {
      export namespace Type {
        export namespace Mapper {
          /**
           * @summary Mapper result. Powered by chaining
           * @param TAllowDefaults If 'true' — allow put default values into map
           * @param T Initial type arrays array
           * @param K Key constraint
           * @param L Literal constraint
           * @param D Default value
           */
          export type Result<
            TAllowDefaults extends boolean,
            T extends AVStantso.TS.Type.Arr[]
          > = Iteration<TAllowDefaults, T> & {
            /**
             * @summary Result map of type definitions
             */
            map: AVStantso.TS.Type.Arr.ToMap<T, TAllowDefaults>;
          };

          /**
           * @summary Create type definition map by items. Powered by chaining
           * @param TAllowDefaults If 'true' — allow put default values into map
           * @param T Initial type arrays array
           * @param K Key constraint
           * @param L Literal constraint
           * @param key Key value
           * @param type Literal type value
           */
          export type Iteration<
            TAllowDefaults extends boolean = false,
            T extends AVStantso.TS.Type.Arr[] = []
          > = TAllowDefaults extends true
            ? <
                K extends AVStantso.TS.Key,
                L extends AVStantso.TS.Type.Literal,
                D extends AVStantso.TS.Type.Resolve<L> = undefined
              >(
                key: K,
                type: L,
                def?: D
              ) => Result<
                TAllowDefaults,
                [...T, AVStantso.TS.Type.Arr<K, L, D>]
              >
            : <K extends AVStantso.TS.Key, L extends AVStantso.TS.Type.Literal>(
                key: K,
                type: L
              ) => Result<TAllowDefaults, [...T, AVStantso.TS.Type.Arr<K, L>]>;
        }

        /**
         * @summary Create type definition array `[<key>, <type literal>]`
         */
        export type Arr = {
          /**
           * @summary Create type definition array `[<key>, <type literal>]`
           * @param K Key constraint
           * @param L Literal constraint
           * @param key Key value
           * @param type Literal type value
           */
          <K extends AVStantso.TS.Key, L extends AVStantso.TS.Type.Literal>(
            key: K,
            type: L
          ): AVStantso.TS.Type.Arr<K, L>;

          /**
           * @summary Parse params like `[K, L]` or `[[K, L]]`
           *
           * `⚠ Without strict control`
           */
          Parse(params: any[]): AVStantso.TS.Type.Arr;
        };

        /**
         * @summary Create type definition map by items. Powered by chaining
         */
        export type Mapper = Mapper.Iteration & {
          /**
           * @summary Create type definition map by items. Can include default values. Powered by chaining
           */
          Def: Mapper.Iteration<true>;
        };

        export namespace Definer {
          /**
           * @summary Definer base type constraint
           */
          export type BaseType = AVStantso.TS.Key;

          /**
           * @summary Definer for base type `T`
           * @param T Base type
           * @param G Derived type
           * @param value Value
           * @returns Value `as` type `G`
           */
          export type Func<T> = <G extends T>(value: G) => G;

          /**
           * @summary Create array definition by items. Powered by chaining
           * @param T Base item type
           * @param A Initial type array
           * @param G Item constraint
           * @param value Item value
           */
          export namespace Arr {
            /**
             * @summary Definer array result. Powered by chaining
             * @param T Base item type
             * @param A Initial type array
             * @param G Item constraint
             */
            export type Result<
              T extends BaseType,
              A extends T[] = []
            > = Iteration<T, A> & {
              /**
               * @summary Result array of type definitions
               */
              R: A;
            };

            /**
             * @summary Create array definition by items. Powered by chaining
             * @param T Base item type
             * @param A Initial type array
             * @param G Item constraint
             * @param value Item value
             */
            export type Iteration<T extends BaseType, A extends T[] = []> = <
              G extends T
            >(
              value: G
            ) => Result<T, [...A, G]>;
          }
        }

        /**
         * @summary Definer for base type `T`
         * @param T Base type
         * @param G Derived type
         * @param value Value
         * @returns Value `as` type `G`
         */
        export interface Definer<T extends Definer.BaseType>
          extends Definer.Func<T> {
          /**
           * @summary Create array definition by items. Powered by chaining
           * @param T Base item type
           * @param A Initial type array
           * @param G Item constraint
           * @param value Item value
           */
          Arr: Definer.Arr.Iteration<T> & {
            <
              G0 extends T,
              G1 extends Exclude<T, G0>,
              G2 extends Exclude<T, G0 | G1> = undefined,
              G3 extends Exclude<T, G0 | G1 | G2> = undefined,
              G4 extends Exclude<T, G0 | G1 | G2 | G3> = undefined,
              G5 extends Exclude<T, G0 | G1 | G2 | G3 | G4> = undefined,
              G6 extends Exclude<T, G0 | G1 | G2 | G3 | G4 | G5> = undefined,
              G7 extends Exclude<
                T,
                G0 | G1 | G2 | G3 | G4 | G5 | G6
              > = undefined,
              G8 extends Exclude<
                T,
                G0 | G1 | G2 | G3 | G4 | G5 | G6 | G7
              > = undefined,
              G9 extends Exclude<
                T,
                G0 | G1 | G2 | G3 | G4 | G5 | G6 | G7 | G8
              > = undefined
            >(
              v0: G0,
              v1: G1,
              v2?: G2,
              v3?: G3,
              v4?: G4,
              v5?: G5,
              v6?: G6,
              v7?: G7,
              v8?: G8,
              v9?: G9
            ): AVStantso.TS.Array.FilterUnique<
              [G0, G1, G2, G3, G4, G5, G6, G7, G8, G9]
            >;
          };
        }

        /**
         * @summary Map const of `AVStantso.TS.Type.Literal`
         */
        export interface Literal {} // ⇓ Described below ⇓ //
      }

      /**
       * @summary `AVStantso.TS.Type` utility
       */
      export interface Type {
        /**
         * @summary Map const of `AVStantso.TS.Type.Literal`
         */
        Literal: Type.Literal;

        /**
         * @summary Create keyed type definition structure
         * @param K Key constraint
         * @param L Literal constraint
         * @param key Key value
         * @param type Literal type value
         */
        KeyDef<K extends AVStantso.TS.Key, L extends AVStantso.TS.Type.Literal>(
          key: K,
          type: L
        ): AVStantso.TS.Type.KeyDef.Make<K, L>;

        /**
         * @summary Create type definition array `[<key>, <type literal>]`
         * @param K Key constraint
         * @param L Literal constraint
         * @param key Key value
         * @param type Literal type value
         */
        Arr: Type.Arr;

        /**
         * @summary Create type definition map by items. Powered by chaining
         */
        Mapper: Type.Mapper;

        /**
         * @summary Make definer for base type `T`
         * @param T Base type
         * @param template Source of `T`
         * @returns Typed definer for `T` derived values
         */
        Definer<T extends Type.Definer.BaseType>(template?: T): Type.Definer<T>;

        /**
         * @summary Extract default entity from map created by `Mapper`
         * @param TMap Type definition map type
         * @param map Type definition map
         * @returns Default entity from `map`
         */
        Default<TMap extends AVStantso.TS.Type.Arr.Map<true>>(
          map: TMap
        ): { [K in keyof TMap]: TMap[K]['def'] };

        /**
         * @summary Is `value` a resolved `type` of `L`
         * @param type Type literal
         * @param value Tested value
         */
        isLiteralValue<L extends AVStantso.TS.Type.Literal>(
          type: L,
          value: unknown
        ): value is AVStantso.TS.Type.Resolve<L>;

        /**
         * @summary Flat `structure` into `depth`
         * @param structure Structure to flat
         * @param depth Depth of flat
         */
        flat<T extends object, Depth extends number = 1>(
          structure: T,
          depth?: Depth
        ): AVStantso.TS.Flat<T, Depth>;

        /**
         * @summary Empty value for `Literal`
         * @param type Type literal
         */
        emptyLiteralValue<L extends AVStantso.TS.Type.Literal>(
          type: L
        ): AVStantso.TS.Type.Resolve<L>;
      }
    }

    export interface TS {
      /**
       * @summary `AVStantso.TS.Type` utility
       */
      Type: TS.Type;
    }
  }

  avstantso._reg.TS('Type', ({ Generics }) => {
    const KeyDef = (key: string, type: string) => ({ K: key, L: type });

    const Arr = (key: string, type: string) => [key, type];
    Arr.Parse = (params: any[]) =>
      Array.isArray(params[0]) ? params[0] : params;

    function mapper(allowDef?: boolean, arr?: any[]) {
      function m(key: string, type: string) {
        return mapper(allowDef, [...(arr || []), [key, type]]);
      }

      Object.defineProperties(m, {
        map: {
          get() {
            return arr?.reduce(
              (r, [key, type, def]) =>
                (r[key] = allowDef ? { type, def } : type) && r,
              {}
            );
          },
        },
        ...(arr
          ? {}
          : {
              Def: {
                value() {
                  return mapper(true);
                },
              },
            }),
      });

      return m;
    }

    function Definer<T extends AVStantso.TS.Key>(template?: T) {
      const D = <G extends T>(value: G) => value;

      function A(arr?: any[]) {
        function a(...params: any[]) {
          const next = [...(arr || []), ...params];

          if (params.length > 1) return next;

          return A(next);
        }

        Object.defineProperties(a, {
          R: {
            get: () => arr,
          },
        });

        return a;
      }

      D.Arr = A();

      return D;
    }

    function Default(map: AVStantso.TS.Type.Arr.Map<true>) {
      const r: any = {};

      Object.entries(map).forEach(([key, { def }]) => (r[key] = def));

      return r;
    }

    function isLiteralValue<L extends AVStantso.TS.Type.Literal>(
      type: L,
      value: unknown
    ): value is AVStantso.TS.Type.Resolve<L> {
      const L = avstantso.TS.Type.Literal;

      const t = typeof value;

      return L.object === t
        ? L.Date === type
          ? value instanceof Date
          : L.Buffer === type
          ? value instanceof Buffer
          : t === type
        : t === type;
    }

    function flat(structure: any, depth: number = 1) {
      if (depth <= 0 || !['object', 'function'].includes(typeof structure))
        return structure;

      const r: any = {};

      Object.entries(structure).forEach(([sk, sv]) => {
        if (!['object', 'function'].includes(typeof sv)) {
          r[sk] = sv;
          return;
        }

        const c = flat(sv, depth - 1);
        Object.entries(c).forEach(([ck, cv]) => {
          r[ck] = cv;
        });
      });

      return r;
    }

    function emptyLiteralValue(type: AVStantso.TS.Type.Literal): any {
      const L = avstantso.TS.Type.Literal;

      switch (type) {
        case L.string:
          return '';
        case L.symbol:
          return Symbol(0);
        case L.number:
          return 0;
        case L.bigint:
          return 0;
        case L.boolean:
          return false;
        case L.Date:
          return new Date();
        case L.Buffer:
          return Buffer.from('');
        default:
          return undefined;
      }
    }

    return Generics.Cast.To({
      KeyDef,
      Arr,
      Mapper: mapper(),
      Definer,
      Default,
      isLiteralValue,
      flat,
      emptyLiteralValue,
    });
  });

  // use avstantso.TS.Type.Mapper in definition
  export namespace Code.TS.Type {
    const Map = avstantso.TS.Type.Mapper('string', 'string')(
      'number',
      'number'
    )('bigint', 'bigint')('boolean', 'boolean')('symbol', 'symbol')(
      'undefined',
      'undefined'
    )('object', 'object')('function', 'function')('Date', 'Date')(
      'Buffer',
      'Buffer'
    ).map;

    avstantso._reg.TS.Type('Literal', Map);

    type Map = typeof Map;

    export interface Literal extends Map {}
  }
}
