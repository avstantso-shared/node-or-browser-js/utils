namespace AVStantso.TS.Type {
  /**
   * @summary Key or type definition array or keyed type definition structure
   */
  export type KeyArrDef = Key | Arr | KeyDef;

  export namespace KeyArrDef {
    /**
     * @summary Constraint for `TS.Type.KeyArrDef` next type parameter
     * @param TKeyArrDef Type def for constraint
     * @param TIfSatisfy Type if constraint is satisfied
     */
    export type Constraint<
      TKeyArrDef extends KeyArrDef,
      TIfSatisfy = unknown
    > = TKeyArrDef extends object ? never : TIfSatisfy;
  }
}
