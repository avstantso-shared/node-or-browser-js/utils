namespace AVStantso {
  export namespace TS {
    export namespace Type {
      /**
       * @summary Literal for available types for value
       */
      export type Literal = keyof Literal.Map;

      export namespace Literal {
        /**
         * @summary Map for available types for value
         */
        export interface Map extends AtomicObjects {
          string: string;
          number: number;
          bigint: bigint;
          boolean: boolean;
          symbol: symbol;
          undefined: undefined;
          object: object;
          function: Function;
        }

        /**
         * @summary Map for number-like types.
         *
         * ⚠ Key is significant, value ignored
         */
        export interface NumberLike {
          number: 0;
          bigint: 0;
          Date: 0;
        }

        /**
         * @summary If `L` is number-like returns `IfTrue` else `IfFalse`
         * @param L Tested literal
         * @param IfTrue Result, if `L` is number-like
         * @param IfFalse Result, if `L` NOT is number-like
         * @returns `IfFalse` or `IfTrue` param
         * @example
         * type n = CheckType<IsNumberLike<never>, never>;
         * type u = CheckType<IsNumberLike<undefined>, undefined>;
         *
         * type t = CheckType<IsNumberLike<'number'>, true>;
         * type f = CheckType<IsNumberLike<'object'>, false>;
         */
        export type IsNumberLike<
          L extends Literal,
          IfTrue = true,
          IfFalse = false
        > = L extends undefined
          ? undefined
          : [Extract<L, keyof NumberLike>] extends [never]
          ? IfFalse
          : IfTrue;

        /**
         * @summary Map for string-like types.
         *
         * ⚠ Key is significant, value ignored
         */
        export interface StringLike {
          string: 0;
          Buffer: 0;
        }

        /**
         * @summary If `L` is string-like returns `IfTrue` else `IfFalse`
         * @param L Tested literal
         * @param IfTrue Result, if `L` is string-like
         * @param IfFalse Result, if `L` NOT is string-like
         * @returns `IfFalse` or `IfTrue` param
         * @example
         * type n = CheckType<IsStringLike<never>, never>;
         * type u = CheckType<IsStringLike<undefined>, undefined>;
         *
         * type t = CheckType<IsStringLike<'string'>, true>;
         * type f = CheckType<IsStringLike<'object'>, false>;
         */
        export type IsStringLike<
          L extends Literal,
          IfTrue = true,
          IfFalse = false
        > = L extends undefined
          ? undefined
          : [Extract<L, keyof StringLike>] extends [never]
          ? IfFalse
          : IfTrue;

        /**
         * @summary Literal for available types for value as `array`
         */
        export type List = [
          'string',
          'number',
          'bigint',
          'boolean',
          'symbol',
          'undefined',
          'object',
          'function',

          'Date',
          'Buffer'
        ];
      }
    }
  }
}
