namespace AVStantso.TS.Type {
  /**
   * @summary Type definition structure
   */
  export type Def =
    | {
        /**
         * @summary Literal
         */
        L: Literal;

        /**
         * @summary Type
         */
        T?: unknown;
      }
    | {
        L?: Literal;
        T: unknown;
      };

  export namespace Def {
    /**
     * @summary Make type definition structure
     */
    export type Make<T, L extends Literal = undefined> = L extends undefined
      ? { T: T }
      : { L: L; T: T };

    /**
     * @summary Get type definition structure from type union or structure, and literal
     */
    export type From<T, L extends Literal> = T extends Def
      ? Make<T['T'], IfDefKey<'L', Def, T> | (L extends undefined ? never : L)>
      : Make<T, L>;

    export namespace Or {
      /**
       * @summary Type definition structure or literal
       */
      export type L = Def | Literal;
    }

    /**
     * @summary Test type is `TS.Type.Def`
     * @param TOrDef Tested `TS.Type.Def`
     * @param IfTrue Result, if `TOrDef` is `TS.Type.Arr`
     * @param IfFalse Result, if `TOrDef` NOT is `TS.Type.Arr`
     * @returns `IfFalse` or `IfTrue` param
     */
    export type Is<TOrDef, IfTrue = true, IfFalse = false> = TOrDef extends Def
      ? IfTrue
      : IfFalse;
  }
}
