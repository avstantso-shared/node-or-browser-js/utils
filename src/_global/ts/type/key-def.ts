namespace AVStantso.TS.Type {
  /**
   * @summary Keyed type definition structure
   */
  export type KeyDef = {
    /**
     * @summary Key
     */
    K: Key;
  } & Def;

  export namespace KeyDef {
    /**
     * @summary Keyed type definition abstract structure
     */
    export type Abstract = { K: Key } & Def;

    type _Make<
      K extends Key,
      DL extends Def.Or.L,
      TforL extends Make.Constraint<DL>,
      Debug extends boolean,
      T = DL extends Def
        ? Extract<keyof DL, 'T'> extends never
          ? {}
          : { T: DL['T'] }
        : [TforL] extends [never]
        ? {}
        : { T: TforL },
      L = DL extends Def
        ? Extract<keyof DL, 'L'> extends never
          ? {}
          : { L: DL['L'] }
        : { L: DL },
      KD = { K: K } & T & L,
      R = Debug extends true
        ? { K: K; DL: DL; T: T; L: L; KD: KD }
        : { [K in keyof KD]: KD[K] }
    > = R;

    /**
     * @summary Make keyed type definition structure by `Key` and `Def`
     */
    export type Make<
      K extends Key,
      DL extends Def.Or.L,
      T extends Make.Constraint<DL> = never
    > = _Make<K, DL, T, false>;

    export namespace Make {
      /**
       * @summary  Constraint for last params in kake keyed type definition structure by `Key` and `Def`
       */
      export type Constraint<DL extends Def.Or.L> = DL extends Def
        ? never
        : unknown;
    }

    //#region From
    type _From<
      TKA extends Key | Arr,
      T extends KeyArrDef.Constraint<TKA>,
      Debug extends boolean,
      K extends Key = TKA extends Arr ? TKA[0] : TKA,
      P = TKA extends Arr ? TKA[1] : T,
      L = {} extends P ? never : Extract<Literal, P>,
      O = Exclude<P, Literal>,
      D =
        | (L extends never ? never : { L: L })
        | (O extends never ? never : { T: O }),
      R = { K: K } & D
    > = Debug extends true
      ? {
          TKeyOrArr: TKA;
          TProperty: T;
          K: K;
          L: L;
          O: O;
          D: D;
          R: R;
        }
      : R;

    /**
     * @summary Get keyed type definition structure from `TS.Type.KeyArrDef` and property type `T`
     */
    export type From<
      TKAD extends KeyArrDef,
      T extends KeyArrDef.Constraint<TKAD> = KeyArrDef.Constraint<TKAD>
    > = TKAD extends KeyDef ? TKAD : _From<Exclude<TKAD, KeyDef>, T, false>;
    //#endregion

    /**
     * @summary Merge keyed type definition structure with type field `T`
     */
    export type Merge<KD extends KeyDef, T = unknown> = Omit<KD, 'T'> &
      (Extract<'T', keyof KD> extends never ? { T: T } : { T: KD['T'] | T });
  }
}
