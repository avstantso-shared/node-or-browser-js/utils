namespace AVStantso.TS.Type {
  type _Not<
    T extends Type | Literal,
    Debug extends boolean,
    L = HasString<T, never, Extract<Literal, T>>,
    O = Exclude<T, Literal>,
    R = L | true extends true
      ? Exclude<Type, O>
      : O | true extends true
      ? Exclude<Literal, L>
      : never
  > = Debug extends true ? { T: T; L: L; O: O; R: R } : R;

  /**
   * @summary Available types for value (`Type`) xor `Type.Literal`, exclude specified.
   * If `T` union includes `string` — literals is ignored,
   * else if types and literals is mixed — returns `never`
   * @example
   * Not<boolean | 'boolean'>; // never
   *
   * Not<string>; // number | bigint | boolean | symbol | object | Function
   *
   * // ⚠ literals is ignored:
   * Not<string | 'boolean'>; // number | bigint | boolean | symbol | object | Function
   *
   * Not<'boolean' | 'string'>; //"number" | "bigint" | "symbol" | "undefined" | "object" | "function"
   */
  export type Not<T extends Type | Literal> = _Not<T, false>;

  export namespace Not {
    /**
     * @summary Available types for value, exclude `Function`
     */
    export type Function = Exclude<Type, globalThis.Function>;

    /**
     * @summary Available types for value, exclude `object`
     */
    export type Object = Exclude<Type, object>;

    /**
     * @summary Available types for value, exclude `string`
     */
    export type String = Exclude<Type, string>;
  }
}
