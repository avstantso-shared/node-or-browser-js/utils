namespace AVStantso.TS.Type {
  type SSUE = 'some string unused elsewhere';

  /**
   * @summary 'T' union includes `string`, not a set of literals only or other types
   * @param T Tested type union
   * @param IfTrue Result, if `T` includes `string`
   * @param IfFalse Result, if `T` NOT includes `string`
   * @returns `IfFalse` or `IfTrue` param
   */
  export type HasString<T, IfTrue = true, IfFalse = false> = {} extends T
    ? IfFalse
    : SSUE extends T
    ? IfTrue
    : IfFalse;
}
