namespace AVStantso.TS {
  /**
   * @summary Available types for value
   */
  export type Type = Type.Literal.Map[Type.Literal];

  export namespace Type {
    /**
     * @summary Available types for value as `array`
     */
    export type List = [
      string,
      number,
      bigint,
      boolean,
      symbol,
      undefined,
      object,
      Function
    ];
  }
}
