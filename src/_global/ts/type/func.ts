namespace AVStantso.TS.Type {
  /**
   * @summary Function generic type
   */
  export type Func<R, TArgs extends any[] = never> = (...args: TArgs) => R;

  export namespace Func {
    /**
     * @summary Any function type.
     *
     * Same as constraint for standard types `Parameters` / `ReturnType`
     */
    export type Any = (...args: any[]) => any;

    /**
     * @summary Nested function generic type
     * @param Depth nesting depth
     */
    export type Nested<Depth extends number = 1> = Depth extends 0
      ? Any
      : (...args: any[]) => Nested<Decrement<Depth>>;
  }

  /**
   * @summary Procedure generic type
   */
  export type Proc<TArgs extends any[] = never> = (...args: TArgs) => void;
}
