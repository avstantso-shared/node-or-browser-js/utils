namespace AVStantso.TS.Type {
  /**
   * @summary Type definition array `[<key>, <type literal>]`
   * @param K Key constraint
   * @param L Literal constraint
   * @param D Default value
   */
  export type Arr<
    K extends Key = Key,
    L extends Literal = Literal,
    D extends Resolve<L> = any
  > = [K, L, D?];

  export namespace Arr {
    /**
     * @summary Type definition map by array of definition arrays
     */
    export namespace Map {
      /**
       * @summary Type definition map item with default value
       * @param L Literal constraint
       * @param D Default value
       */
      export type DefRec<
        L extends Literal = Literal,
        D extends Resolve<L> = any
      > = { type: L; def: D };

      /**
       * @summary Type definition map item depend on `TAllowDefaults`
       * @param TAllowDefaults If 'true' — allow put default values into map
       * @param L Literal constraint
       * @param D Default value
       */
      export type Item<
        TAllowDefaults extends boolean,
        L extends Literal = Literal,
        D extends Resolve<L> = any
      > = TAllowDefaults extends true ? DefRec<L, D> : L;
    }

    export type Map<TAllowDefaults extends boolean> = NodeJS.Dict<
      Map.Item<TAllowDefaults>
    >;

    type _ToMap<
      TArrOfArr extends Arr[],
      TAllowDefaults extends boolean,
      R extends {} = {},
      I extends number = 0
    > = AVStantso.TS.Array.IfEnd<TArrOfArr, I> extends true
      ? { [K in keyof R]: R[K] }
      : _ToMap<
          TArrOfArr,
          TAllowDefaults,
          R & {
            [K in TArrOfArr[I][0]]: Map.Item<
              TAllowDefaults,
              TArrOfArr[I][1],
              TArrOfArr[I][2]
            >;
          },
          AVStantso.TS.Increment<I>
        >;

    /**
     * @summary Create type definition map by array of definition arrays
     * @param TArrOfArr Array of definition arrays
     * @param TAllowDefaults If 'true' — allow put default values into map
     */
    export type ToMap<
      TArrOfArr extends Arr[],
      TAllowDefaults extends boolean = false
    > = _ToMap<TArrOfArr, TAllowDefaults>;
  }
}
