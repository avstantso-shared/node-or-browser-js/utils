namespace AVStantso.TS.Type {
  type _Resolve<
    T,
    Debug extends boolean,
    L = {} extends T ? never : Extract<Literal, T>,
    O = Exclude<T, Literal>,
    R = ([L] extends [never] ? L : Literal.Map[Extract<L, Literal>]) | O
  > = Debug extends true ? { T: T; L: L; O: O; R: R } : R;

  namespace _Resolve {
    export type Switch<T, Debug extends boolean> = T extends Def
      ?
          | (T extends { T: unknown }
              ? HasString<T['T']> extends true
                ? Exclude<T['T'], Literal>
                : _Resolve<T['T'], Debug>
              : never)
          | (T extends { L: Literal } ? _Resolve<T['L'], Debug> : never)
      : //_Resolve<T['T'], Debug> | _Resolve<T['L'], Debug>
      HasString<T> extends true
      ? Exclude<T, Literal>
      : _Resolve<T, Debug>;
  }

  /**
   * @summary Resolve `Type.Literal` in type union `T` as its satisfied types.
   * With type `string` literals NOT supported
   * @example
   * Resolve<never>; // never
   *
   * // ⛔ literals NOT supported:
   * Resolve<string | 'function'>; // string
   *
   * Resolve<'bigint' | 'string'>; // string | bigint
   *
   * Resolve<bigint>; // bigint
   *
   * Resolve<'bigint' | 'string' | boolean>; // string | bigint | boolean
   *
   * Resolve<{ T: Function; L: 'boolean' }>; // boolean | Function
   *
   * Resolve<{}>; // {}
   *
   * Resolve<{ T: string; L: 'string' }>; // string
   *
   * Resolve<{ T: string; L: 'function' }>; // string | Function
   *
   * Resolve<{ L: 'function' }>; // Function
   *
   * Resolve<{ T: Function }>; // Function
   */
  export type Resolve<T> = _Resolve.Switch<T, false>;

  export namespace Resolve {
    /**
     * @summary Resolve fields of object structure with `Type.Literal` in type union `T` fields as its satisfied types.
     * @see Resolve
     */
    export type Structure<T> = IfStructure<T> extends true
      ? { [K in keyof T]: Resolve<T[K]> }
      : never;
  }
}
