namespace AVStantso.TS {
  //#region IfStructure
  /**
   * @summary If `T` is structure object. Not extends `Structure.NotRules` (`Array`, `Date`, `Buffer`)
   * @example
   * type n = IfStructure<number>; // false
   * type s = IfStructure<string>; // false
   * type o = IfStructure<{}>; // true
   * type ob = IfStructure<object>; // true
   * type a = IfStructure<[]>; // false
   * type d = IfStructure<Date>; // false
   * type b = IfStructure<Buffer>; // false
   */
  export type IfStructure<
    T,
    IfTrue = true,
    IfFalse = false
  > = T extends undefined
    ? IfFalse
    : T extends object
    ? T extends Structure.NotRules
      ? IfFalse
      : IfTrue
    : IfFalse;
  //#endregion

  /**
   * @summary Constraint: `T` is structure object. Not extends `Structure.NotRules` (`Array`, `Date`, `Buffer`)
   * @see `IfStructure`
   */
  export type Structure<T extends object> = IfStructure<T, T, never>;

  export namespace Structure {
    //#region NotRules, MapOfNotRules
    /**
     * @summary Used as rules source in `IfStructure`.
     *
     * If extends one of values — it is not a structure
     */
    export interface MapOfNotRules extends AtomicObjects {
      Array: any[];
    }

    /**
     * @summary Used as rules `IfStructure`.
     */
    export type NotRules = MapOfNotRules[keyof MapOfNotRules];
    //#endregion

    //#region Separate
    type _Separate<
      T extends object,
      P extends object = {
        [K in keyof T as IfStructure<T[K], never, K>]: T[K];
      },
      S extends object = {
        [K in keyof T as IfStructure<T[K], K, never>]: T[K];
      },
      R extends [P, S] = [P, S]
    > = R;

    /**
     * @summary Separate structure object to `[<Plain>, <Structural>]`
     */
    export type Separate<T> = IfStructure<T> extends true
      ? _Separate<T & object>
      : never;
    //#endregion

    /**
     * @summary Reverse structure object to map value-key. Non keyable values is ignored
     * @example
     * type r = Reverse<{x: 1, y: 'ABC', z: {a: true}}>
     * type r = {
     *   1: "x";
     *   ABC: "y";
     * }
     */
    export type Reverse<T> = IfStructure<T> extends true
      ? { [K in keyof T as Extract<T[K], Key>]: K }
      : never;
  }
}
