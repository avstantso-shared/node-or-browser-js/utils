namespace AVStantso.TS.String {
  //#region Length
  type _Length<
    S extends string,
    R extends number = 0
  > = S extends `${infer C}${infer E}` ? _Length<E, Increment<R>> : R;

  /**
   * @summary Calc `string` literal length
   * @param S Tested `string` literal
   * @returns `string` literal length
   * @example
   * type n = CheckType<Length<never>, never>;
   * type u = CheckType<Length<undefined>, undefined>;
   *
   * type l0 = CheckType<Length<''>, 0>;
   * type l1 = CheckType<Length<'A'>, 1>;
   * type l2 = CheckType<Length<'AB'>, 2>;
   */
  export type Length<S extends string> = S extends undefined
    ? undefined
    : _Length<S>;
  //#endregion

  //#region IsLengthBetween
  type _IsLengthBetween<
    S extends string,
    Min extends number,
    Max extends number,
    L extends number = _Length<S>
  > = LT<L, Min> extends true ? false : GT<L, Max> extends true ? false : true;

  /**
   * @summary Is `string` literal length in `[Min, Max]`.
   *
   * Shortcut for `GTE<Length<S>, Min> & LTE<Length<S>, Max>`
   * @param S Tested `string` literal
   * @param Min Minimal length
   * @param Max Maximal length
   * @param IfTrue Result, if `S` length in `[Min, Max]`
   * @param IfFalse Result, if ``S` length NOT in `[Min, Max]`
   * @returns `IfFalse` or `IfTrue` param
   * @example
   * type n0 = CheckType<IsLengthBetween<never, number, number>, never>;
   * type n1 = CheckType<IsLengthBetween<string, never, number>, never>;
   * type n2 = CheckType<IsLengthBetween<string, number, never>, never>;
   * type n3 = CheckType<IsLengthBetween<string, 2, 1>, never>;
   *
   * type u0 = CheckType<IsLengthBetween<string, undefined, number>, undefined>;
   * type u1 = CheckType<IsLengthBetween<string, undefined, number>, undefined>;
   * type u2 = CheckType<IsLengthBetween<string, number, undefined>, undefined>;
   *
   * type t0 = CheckType<IsLengthBetween<'', 0>, true>;
   * type f0 = CheckType<IsLengthBetween<'', 1>, false>;
   *
   * type t1 = CheckType<IsLengthBetween<'A', 1, 2>, true>;
   * type f1 = CheckType<IsLengthBetween<'A', 2>, false>;
   *
   * type t2_02 = CheckType<IsLengthBetween<'AB', 0, 2>, true>;
   * type t2_12 = CheckType<IsLengthBetween<'AB', 1, 2>, true>;
   * type t2_22 = CheckType<IsLengthBetween<'AB', 2, 2>, true>;
   * type t2_23 = CheckType<IsLengthBetween<'AB', 2, 3>, true>;
   * type f2_1 = CheckType<IsLengthBetween<'AB', 1>, false>;
   */
  export type IsLengthBetween<
    S extends string,
    Min extends number,
    Max extends number = Min,
    IfTrue = true,
    IfFalse = false
  > = S extends undefined
    ? undefined
    : Min extends undefined
    ? undefined
    : Max extends undefined
    ? undefined
    : GT<Min, Max> extends true
    ? never
    : _IsLengthBetween<S, Min, Max> extends true
    ? IfTrue
    : IfFalse;
  //#endregion

  //#region SplitToChars
  type _SplitToChars<
    S extends string,
    R extends string[] = []
  > = S extends `${infer C}${infer E}` ? _SplitToChars<E, [...R, C]> : R;

  /**
   * @summary Split `string` literal to chars array
   */
  export type SplitToChars<S extends string> = _SplitToChars<S>;
  //#endregion

  //#region Join
  type _Join<
    A extends (string | number | bigint | boolean)[],
    R extends string = '',
    I extends number = 0
  > = Array.IfEnd<A, I> extends true
    ? R
    : _Join<A, `${R}${A[I]}`, Increment<I>>;

  /**
   * @summary Join literals array to `string`
   */
  export type Join<A extends (string | number | bigint | boolean)[]> = _Join<A>;
  //#endregion

  //#region Repeat
  type _Repeat<
    S extends string | number | bigint | boolean,
    N extends number,
    R extends string = ''
  > = N extends 0 ? R : _Repeat<S, Decrement<N>, `${R}${S}`>;

  /**
   * @summary Repeat string posible `S` by `N` times
   */
  export type Repeat<
    S extends string | number | bigint | boolean,
    N extends number
  > = S extends undefined
    ? undefined
    : N extends undefined
    ? undefined
    : LT<N, 0> extends true
    ? undefined
    : _Repeat<S, N>;
  //#endregion

  //#region Replace
  type _Replace<
    S extends string,
    M extends string,
    T extends string,
    N extends number,
    R extends string = ''
  > = N extends 0
    ? `${R}${S}`
    : S extends `${infer C}${infer E}`
    ? C extends M
      ? _Replace<E, M, T, Decrement<N>, `${R}${T}`>
      : _Replace<E, M, T, N, `${R}${C}`>
    : R;

  /**
   * @summary Replace in string `S` all characters matched to `M` to `T` string `N` times or less
   * @example
   * type r1 = Replace<'a-b_c_d', '_' | '-'>; //abcd
   * type r2 = Replace<'123456_246', '2' | '4' | '6', '#', 3>; //1#3#5#_246
   */
  export type Replace<
    S extends string,
    M extends string,
    T extends string = '',
    N extends number = 4000
  > = LTE<N, 0> extends true ? S : _Replace<S, M, T, N>;
  //#endregion

  //#region Includes
  type _Includes<
    S extends string,
    P extends string,
    IfTrue,
    IfFalse,
    R extends string = '',
    PP extends string = P
  > = P extends R
    ? IfTrue
    : S extends `${infer SC}${infer SE}`
    ? PP extends `${infer PC}${infer PE}`
      ? SC extends PC
        ? _Includes<SE, P, IfTrue, IfFalse, `${R}${SC}`, PE>
        : _Includes<SE, P, IfTrue, IfFalse>
      : IfFalse
    : IfFalse;

  /**
   * @summary `S` includes `P`
   */
  export type Includes<
    S extends string,
    P extends string,
    IfTrue = true,
    IfFalse = false
  > = _Includes<S, P, IfTrue, IfFalse>;
  //#endregion

  /**
   * @summary String cases
   * @example
   * type w =
   *   'Oh! Jingle bells, jingle bells Jingle all the way';
   * type c = Camel<w>;     // ohJingleBellsJingleBellsJingleAllTheWay
   * type p = Pascal<w>;    // OhJingleBellsJingleBellsJingleAllTheWay
   * type s = Snake<w>;     // oh_jingle_bells_jingle_bells_jingle_all_the_way
   * type S = Snake.Up<w>;  // OH_JINGLE_BELLS_JINGLE_BELLS_JINGLE_ALL_THE_WAY
   * type k = Kebab<w>;     // oh-jingle-bells-jingle-bells-jingle-all-the-way
   */
  export namespace Case {
    /**
     * @summary String cases settings
     */
    export namespace Settings {
      /**
       * @summary String cases global removes settings
       */
      export namespace Removes {
        /**
         * @summary String cases global removes map for keys. Values ignored
         */
        export interface Map {
          ' ': 0;
          '-': 0;
          _: 0;
          ',': 0;
          '.': 0;
          '!': 0;
        }
      }

      export type Removes = keyof Removes.Map;
    }

    type _CamelPascal<
      S extends string,
      RM extends string,
      U extends boolean = false,
      R extends string = ''
    > = S extends `${infer C}${infer E}`
      ? C extends RM
        ? _CamelPascal<E, RM, true, R>
        : _CamelPascal<
            E,
            RM,
            false,
            `${R}${U extends true ? Uppercase<C> : Lowercase<C>}`
          >
      : R;

    type _SnakeKebab<
      S extends string,
      Sep extends string,
      RM extends string,
      U extends boolean = false,
      R extends string = ''
    > = S extends `${infer C}${infer E}`
      ? C extends RM
        ? _SnakeKebab<E, Sep, RM, true, R>
        : _SnakeKebab<
            E,
            Sep,
            RM,
            false,
            `${R}${U extends true
              ? Sep
              : C extends Uppercase<C>
              ? Sep
              : ''}${Lowercase<C>}`
          >
      : R;

    /**
     * @summary Camel case
     */
    export type Camel<
      S extends string,
      R extends string = Settings.Removes
    > = _CamelPascal<S, R>;

    /**
     * @summary Pascal case
     */
    export type Pascal<
      S extends string,
      R extends string = Settings.Removes
    > = _CamelPascal<S, R, true>;

    /**
     * @summary Snake case
     */
    export type Snake<
      S extends string,
      R extends string = Settings.Removes
    > = _SnakeKebab<S, '_', R>;

    export namespace Snake {
      /**
       * @summary Snake UPPER_CASE
       */
      export type Up<
        S extends string,
        R extends string = Settings.Removes
      > = Uppercase<_SnakeKebab<S, '_', R>>;
    }

    /**
     * @summary Kebab case
     */
    export type Kebab<
      S extends string,
      R extends string = Settings.Removes
    > = _SnakeKebab<S, '-', R>;
  }

  export type Case<
    Type extends 'c' | 'p' | 's' | 'S' | 'k',
    S extends string,
    R extends string = Case.Settings.Removes
  > = Type extends 'c'
    ? Case.Camel<S, R>
    : Type extends 'p'
    ? Case.Pascal<S, R>
    : Type extends 's'
    ? Case.Snake<S, R>
    : Type extends 'S'
    ? Case.Snake.Up<S, R>
    : Type extends 'k'
    ? Case.Kebab<S, R>
    : never;
}
