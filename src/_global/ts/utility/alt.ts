namespace AVStantso.TS {
  type _AltItem<
    T extends object[],
    I extends number,
    R extends object = {},
    J extends number = 0
  > = Array.IfEnd<T, J> extends true
    ? { [K in keyof R]: R[K] }
    : _AltItem<
        T,
        I,
        R &
          (J extends I
            ? { [K in keyof T[J]]: T[J][K] }
            : { [K in keyof T[J]]?: never }),
        Increment<J>
      >;

  type _Alt<
    T extends object[],
    R extends object = never,
    I extends number = 0
  > = Array.IfEnd<T, I> extends true
    ? R
    : _Alt<T, R | _AltItem<T, I>, Increment<I>>;

  /**
   * @summary Create object with `[Alt]ernative` fields, but not everybody fields simultaneously
   * @param T If `object` — each field will be alternative, if `array` each array item will be alternative
   * @return Object with alternatives
   * @example
   * type n = Alt<never>;
   * type u = Alt<undefined>;
   *
   * type o = { a: number; b: string; c: { x: bigint } };
   * type xo = Alt<o>;
   * type xo = {
   *     a: number;
   *     b?: never;
   *     c?: never;
   * } | {
   *     a?: never;
   *     b: string;
   *     c?: never;
   * } | {
   *     a?: never;
   *     b?: never;
   *     c: {
   *         x: bigint;
   *     };
   * }
   *
   * type a = [{a: number; b: string}, {c: { x: bigint }, d: unknown}];
   * type xa = Alt<a>;
   * type xa = {
   *     a: number;
   *     b: string;
   *     c?: never;
   *     d?: never;
   * } | {
   *     a?: never;
   *     b?: never;
   *     c: {
   *         x: bigint;
   *     };
   *     d: unknown;
   * }
   */
  export type Alt<T extends object | object[]> = T extends undefined
    ? undefined
    : T extends object[]
    ? _Alt<T>
    : _Alt<Array.ObjectSplit<T>>;
}
