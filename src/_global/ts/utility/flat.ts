namespace AVStantso.TS {
  type _Flat<
    Depth extends number,
    P,
    S,
    D = { [K in keyof S as keyof S[K]]: S[K] },
    C = P & { [K in keyof D]: D[K][Extract<K, keyof D[K]>] },
    R = Flat<C, Decrement<Depth>>
  > = {
    [K in keyof R]: R[K];
  };

  /**
   * @summary Flatting object structure to `Depth`
   * @param T Type for flatting
   * @param Depth Depth of flatting. If `0`, flatting hasn't effect
   * @return If `T` is object structure — flatted structure of `T`, else unchanged `T`
   * @example
   * type n = Flat<never>; // never
   * type u = Flat<undefined>; // undefined
   *
   * type t = { x: { a: string; b: number }; y: { c: { d: boolean } }; z: bigint };
   * type f0 = Flat<t, 0>;
   * type f0 = Flat<t, undefined>;
   * type f0 = {
   *   x: {
   *       a: string;
   *       b: number;
   *   };
   *   y: {
   *       c: {
   *           d: boolean;
   *       };
   *   };
   *   z: bigint;
   * }
   *
   * type f1 = Flat<t>;
   * type f1 = {
   *   z: bigint;
   *   a: string;
   *   b: number;
   *   c: {
   *       d: boolean;
   *   };
   * }
   *
   * type f2 = Flat<t, 2>;
   * type f2 = {
   *   z: bigint;
   *   a: string;
   *   b: number;
   *   d: boolean;
   * }
   */
  export type Flat<T, Depth extends number = 1> = T extends undefined
    ? undefined
    : Depth extends undefined
    ? T
    : Depth extends 0
    ? T
    : Structure.Separate<T> extends [infer P, infer S]
    ? _Flat<Depth, P, S>
    : T;
}
