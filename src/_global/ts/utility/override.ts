namespace AVStantso.TS {
  /**
   * @summary Override `TBase` properties by `TExtended` properties if `TExtended[K]` is NOT `undefined` or `never` for all `TBase` keys
   * @example
   * type A = {
   *   x?: number;
   *   y?: number;
   *   z?: number;
   * };
   *
   * type B = {
   *   x?: 1 | 2 | 3;
   *   y?: 12
   * };
   *
   * type R = Override<A, B>; // { x?: 1 | 2 | 3; y?: 12; z?: number;}
   */
  export type Override<
    TBase extends {} | Function,
    TExtended extends TBase,
    TOptional extends boolean = false
  > = TOptional extends true
    ? {
        [K in keyof TBase]?: IfDefKey<K, TBase, TExtended>;
      }
    : {
        [K in keyof TBase]: IfDefKey<K, TBase, TExtended>;
      };

  /**
   * @summary Test `TBase` was overrided by `TCurrent`
   * @example
   * type A = IfIsOverrided<number, 1 | 2 | 3>; // true
   * type B = IfIsOverrided<number, number>; // false
   * type C = IfIsOverrided<unknown, unknown>; // false
   * type D = IfIsOverrided<unknown, {}>; // true
   */
  export type IfIsOverrided<
    TBase,
    TCurrent extends TBase,
    IfTrue = true,
    IfFalse = false
  > = TBase extends TCurrent
    ? TCurrent extends object
      ? TCurrent extends TBase
        ? IfTrue
        : IfFalse
      : IfFalse
    : IfTrue;

  /**
   * @summary Test `TBase[Key]` was overrided by `TCurrent[Key]`
   */
  export type IfIsOverridedKey<
    TKey extends keyof TBase,
    TBase,
    TCurrent extends TBase,
    IfTrue = true,
    IfFalse = false
  > = IfIsOverrided<TBase[TKey], TCurrent[TKey], IfTrue, IfFalse>;

  /**
   * @summary Select `TExtended` type if `TBase` and `TCurrent` is equals
   * @example
   * type A = IfNotOverrided<number, 1 | 2 | 3, 2>; // 1 | 2 | 3
   * type B = IfNotOverrided<number, number, 4>; // 4
   * type C =  IfNotOverrided<unknown, unknown, 'A!'>; // "A!"
   */
  export type IfNotOverrided<
    TBase,
    TCurrent extends TBase,
    TExtended extends TBase
  > = IfIsOverrided<TBase, TCurrent, TCurrent, TExtended>;

  /**
   * @summary Select `TExtended[TKey]` if `TBase[TKey]` and `TCurrent[TKey]` is equals
   */
  export type IfNotOverridedKey<
    TKey extends keyof TBase,
    TBase,
    TCurrent extends TBase,
    TExtended extends TBase
  > = IfDef<
    TBase[TKey],
    IfNotOverrided<TBase[TKey], TCurrent[TKey], TExtended[TKey]>
  >;

  /**
   * @summary Override `TCurrent` properties by `TExtended` properties if `TCurrent[K]` equals `TBase[K]` for all `TBase` keys
   * type A = {
   *   a?: string;
   *   b?: string;
   *   x?: number;
   *   y?: number;
   *   z?: number;
   * };
   *
   * type B = {
   *   x?: 1 | 2 | 3;
   *   y?: 12;
   * };
   *
   * type C = {
   *   b: 'b' | 'bb';
   *   x?: 4;
   *   z?: 72;
   * };
   *
   * type R = OverrideIfNot<A, B, C>; // { a?: string; b?: "b" | "bb"; x?: 1 | 2 | 3; y?: 12; z?: 72;}
   */
  export type OverrideIfNot<
    TBase extends {} | Function,
    TCurrent extends TBase,
    TExtended extends TBase,
    TOptional extends boolean = false
  > = TOptional extends true
    ? {
        [K in keyof TBase]?: IfNotOverridedKey<K, TBase, TCurrent, TExtended>;
      }
    : {
        [K in keyof TBase]: IfNotOverridedKey<K, TBase, TCurrent, TExtended>;
      };
}
