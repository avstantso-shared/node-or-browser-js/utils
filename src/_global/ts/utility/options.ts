namespace AVStantso.TS {
  /**
   * @summary Options for type declaration
   * @param TTypeMap All available options
   * @param TDefaults Default values for options
   */
  export type Options<
    TTypeMap extends {},
    TDefaults extends Partial<TTypeMap> = undefined
  > = { TypeMap: Partial<TTypeMap>; Defaults: Partial<TDefaults> };

  export namespace Options {
    /**
     * @summary Structure of options for type declaration
     */
    export type Structure = { TypeMap: {}; Defaults: {} };

    /**
     * @summary Get option from options or default
     */
    export type Get<
      TOptionsStructure extends Options.Structure,
      TOptions extends TOptionsStructure['TypeMap'],
      TKey extends keyof TOptionsStructure['TypeMap'],
      TDefault extends TOptionsStructure['TypeMap'][TKey] = TOptionsStructure['Defaults'][TKey] extends TOptionsStructure['TypeMap'][TKey]
        ? TOptionsStructure['Defaults'][TKey]
        : undefined
    > = TOptions extends undefined
      ? TDefault
      : TOptions[TKey] extends undefined
      ? TDefault
      : TOptions[TKey];

    /**
     * @summary Apply defaults from structure
     */
    export type Apply<
      TOptionsStructure extends Options.Structure,
      TOptions extends TOptionsStructure['TypeMap']
    > = {
      [K in keyof TOptionsStructure['TypeMap']]: Get<
        TOptionsStructure,
        TOptions,
        K
      >;
    };

    /**
     * @summary Check `TOptions` satisfy the `TOptionsStructure`
     */
    export type Check<
      TOptionsStructure extends Options.Structure,
      TOptions extends TOptionsStructure['TypeMap']
    > = TOptions;
  }
}
