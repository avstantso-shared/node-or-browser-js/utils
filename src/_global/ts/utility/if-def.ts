namespace AVStantso.TS {
  /**
   * @summary Select `TExtended` type or `TBase` if `TExtended` is `undefined` or `TElse`
   */
  export type IfDef<
    TBase,
    TExtended extends TBase,
    TElse = TBase
  > = TExtended extends TBase ? TExtended : TElse;

  /**
   * @summary Select `TExtended[TKey]` type or `TBase[TKey]` if `TExtended[TKey]` is `undefined` or `TElse`
   * @example
   * type A = {
   *   x?: number;
   *   y?: number;
   * };
   *
   * type B = {
   *   x?: 1 | 2 | 3;
   * };
   *
   * type C = {
   *   x?: IfDefKey<'x', A, B>; // 1 | 2 | 3
   *   y?: IfDefKey<'y', A, B>; // number
   * };
   */
  export type IfDefKey<
    TKey extends keyof TBase,
    TBase extends {} | Function,
    TExtended extends TBase,
    TElse = TBase[TKey]
  > = IfDef<TBase[TKey], TExtended[TKey], TElse>;
}
