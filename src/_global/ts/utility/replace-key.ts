namespace AVStantso.TS {
  /**
   * @summary Replace key `TKey` in object or function `TBase` for `TReplacement`
   */
  export type ReplaceKey<
    TKey extends keyof TBase,
    TBase extends {} | Function,
    TReplacement,
    TOptional extends boolean = false
  > = Omit<TBase, TKey> &
    (TOptional extends true
      ? { [Key in TKey]?: TReplacement }
      : { [Key in TKey]: TReplacement });

  /**
   * @summary Replace key `TKey` in object or function `TBase` for `TReplacement` optionally
   */
  export type ReplaceKeyOpt<
    TKey extends keyof TBase,
    TBase extends {} | Function,
    TReplacement
  > = ReplaceKey<TKey, TBase, TReplacement, true>;
}
