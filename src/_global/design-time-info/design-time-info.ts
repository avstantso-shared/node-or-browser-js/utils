namespace AVStantso {
  /**
   * @summary Used desingn-time only information control
   */
  export namespace DesignTimeInfo {
    /**
     * @summary Used desingn-time only. Provide generics information
     */
    export type Provider<DTI = unknown> = {
      /**
       * @summary Used desingn-time only. Provide generics information
       */
      designTime?: DTI;
    };
  }

  /**
   * @summary Add desingn-time information to `T`
   */
  export type WithDesignTimeInfo<T, DTI> = T & DesignTimeInfo.Provider<DTI>;

  export type DesignTimeInfo<T extends DesignTimeInfo.Provider> =
    T['designTime'];
}
