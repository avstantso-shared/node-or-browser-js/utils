namespace AVStantso {
  /**
   *  @summary Compare two values function
   */
  export type Compare<TBase = unknown> = WithDesignTimeInfo<
    Compare.Func<TBase> & Compare.Extended<TBase>,
    { baseType?: TBase }
  >;

  export namespace Compare {
    /**
     *  @summary Compare two values atomic function
     */
    export type Func<TBase = unknown> = <
      A extends TBase = TBase,
      B extends TBase = TBase
    >(
      a: A,
      b: B
    ) => number;

    /**
     * @summary `Compare` extended properties & methods
     */
    export type Extended<TBase = unknown> = {
      /**
       * @summary Cast `Compare` function basetype
       */
      as?<R extends TBase>(): Compare<R>;

      /**
       * @summary Create descending `Compare` function
       */
      desc?: Compare<TBase>;
    };

    /**
     *  @summary Compare provider
     */
    export type Provider = AVStantso.Provider.Strict.KeyValue<
      'Compare',
      Compare
    >;

    /**
     *  @summary Compare union
     */
    export type Union = AVStantso.Provider.Union<Provider>;

    /**
     *  @summary Compare value of `U`
     */
    export type Value<U extends Union> = AVStantso.Provider.Value<U, Provider>;
  }
}
