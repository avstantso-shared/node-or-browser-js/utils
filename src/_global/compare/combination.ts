namespace AVStantso {
  export namespace Compare {
    type _Combination<
      TCompares extends Union[],
      R = unknown,
      I extends number = 0,
      C extends TCompares[I] = TCompares[I]
    > = C extends undefined
      ? Compare<{ [K in keyof R]: R[K] }>
      : _Combination<
          TCompares,
          R & Extract<DesignTimeInfo<Value<C>>['baseType'], object>,
          TS.Increment<I>
        >;

    /**
     * @summary Create combined structural `Compare` by array of structural compares
     * @param TCompares Array of structural compares
     * @returns Combined structural `Compare`
     * @example
     * type cmpId = AVStantso.Provider.Compare.Make<'id', string>;
     * type cmpName = AVStantso.Provider.Compare.Make<'name', string>;
     * type cmpVersion = AVStantso.Provider.Compare.Make<'version', number>;
     *
     * type c = Combination<[cmpId, cmpName, cmpVersion]>;
     *
     * type t = CheckType<c, Compare<{ id: string; name: string; version: number; }>>
     */
    export type Combination<TCompares extends Union[]> =
      _Combination<TCompares>;
  }

  export namespace Code {
    export interface Compare {
      /**
       * @summary Create combined structural `Compare` by array of structural compares
       * @param R Result
       * @param TCompares Array of structural compares
       * @returns Combined structural `Compare` as `R`
       * @example
       * const cmpId = Provider.Compare('id', 'string');
       * const cmpName = Provider.Compare('name', 'string');
       * const cmpVersion = Provider.Compare('version', 'number');
       *
       * const c = Provider.Compare.Combine(cmpId, cmpName, cmpVersion);
       *
       * type t = CheckType<typeof c, Compare<{id: string; name: string; version: number;}>>
       */
      Combine<
        R extends AVStantso.Compare.Combination<TCompares>,
        TCompares extends AVStantso.Compare.Union[] = AVStantso.Compare.Union[]
      >(
        ...compares: TCompares
      ): R;
    }
  }

  avstantso._reg.Compare('Combine', ({ JS, Compare: { Extend } }) => {
    function Combine(...compares: any[]): any {
      function combined(a: any, b: any) {
        for (let l = compares.length, i = 0; i < l; i++) {
          const cRaw = compares[i] as Compare.Union;
          const c: Compare<any> = JS.is.function(cRaw) ? cRaw : cRaw.Compare;

          const r = c(a, b);

          if (0 !== r) return r;
        }

        return 0;
      }

      return Extend(combined);
    }

    return Combine;
  });
}
