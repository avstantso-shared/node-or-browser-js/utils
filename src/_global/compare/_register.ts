namespace AVStantso {
  export namespace Code {
    /**
     * @summary Compare helpers utility
     */
    export interface Compare {
      /**
       * @summary Low-lewel extend compare function. Add `Compare.Extended<TBase>`
       * @param compareFunc Function to extend
       */
      Extend<TBase>(
        compareFunc: AVStantso.Compare.Func<TBase>
      ): AVStantso.Compare<TBase>;

      /**
       * @summary Create low-lewel sort-part function for `undefined`, `null`, 'NaN'
       * @param allowNaN Allow check 'NaN'
       * @param nullsFirst If `true` — `undefined`, `null`, 'NaN' has higher priority
       */
      ToNulls(allowNaN?: boolean, nullsFirst?: boolean): (x: unknown) => number;

      /**
       * @summary Select first non-zero comparison
       * @returns First non-zero comparison or `0` if its`s not found
       */
      Select(...comparisons: AVStantso.Perform<number>[]): number;
    }
  }

  export interface Code {
    /**
     * @summary Compare helpers utility
     */
    Compare: Code.Compare;
  }

  export const Compare = avstantso._reg('Compare', ({ Perform }) => {
    function Extend<TBase>(
      compareFunc: AVStantso.Compare.Func<TBase>
    ): AVStantso.Compare<TBase> {
      return Object.defineProperties(compareFunc as AVStantso.Compare<TBase>, {
        as: { value: () => compareFunc },
        desc: { value: (a: any, b: any) => compareFunc(b, a) },
      });
    }

    function ToNulls(allowNaN?: boolean, nullsFirst?: boolean) {
      return (x: any) =>
        undefined === x
          ? -3
          : null === x
          ? -2
          : allowNaN && isNaN(x)
          ? -1
          : nullsFirst
          ? 0
          : -4;
    }

    function Select(...comparisons: AVStantso.Perform<number>[]): number {
      for (let l = comparisons.length, i = 0; i < l; i++) {
        const r = Perform(comparisons[i]);
        if (r !== 0) return r;
      }

      return 0;
    }

    return { Extend, ToNulls, Select };
  });
}
