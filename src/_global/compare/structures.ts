namespace AVStantso {
  export namespace Code {
    export interface Compare {
      /**
       * @summary Structures objects compare
       */
      Structures: AVStantso.Compare<object>;
    }
  }

  avstantso._reg.Compare(
    'Structures',
    ({ AtomicObjects: SimpleObjects, JS, Compare: { ToNulls, Extend } }) => {
      const fo: string[] = [JS.function, JS.object];
      const st: string[] = [JS.string, JS.symbol];
      const nm: string[] = [JS.number, JS.bigint];

      function compareStructures(a: any, b: any): number {
        const t = { a: typeof a, b: typeof b };

        let r: number =
          ToNulls(nm.includes(t.a))(a) - ToNulls(nm.includes(t.b))(b);
        if (r !== 0) return r;

        function cmbBools({ a, b }: { a: boolean; b: boolean }) {
          return (a ? 1 : 0) - (b ? 1 : 0);
        }

        const io = {
          a: fo.includes(t.a),
          b: fo.includes(t.b),
        };
        if (io.a !== io.b) return cmbBools(io);

        // Object/Function
        if (io.a) {
          const ia = { a: Array.isArray(a), b: Array.isArray(a) };
          if (ia.a !== ia.b) return cmbBools(ia);

          // Array
          if (ia.a) {
            for (
              let la = a.length, lb = b.length, l = Math.max(la, lb), i = 0;
              i < l;
              i++
            ) {
              if (i >= la) return -1;
              if (i >= lb) return 1;

              r = compareStructures(a[i], b[i]);
              if (r !== 0) return r;
            }
          }

          // not Array
          else {
            const isSmp = { a: SimpleObjects.is(a), b: SimpleObjects.is(b) };
            if (isSmp.a !== isSmp.b) return cmbBools(isSmp);

            if (isSmp.a) return +a - +b;

            for (
              let aa = Object.entries(a),
                ab = Object.entries(b),
                la = aa.length,
                lb = ab.length,
                l = Math.max(la, lb),
                i = 0;
              i < l;
              i++
            ) {
              if (i >= la) return -1;
              if (i >= lb) return 1;

              const [ka, va] = aa[i];
              const [kb, vb] = ab[i];

              r = compareStructures(ka, kb);
              if (r !== 0) return r;

              r = compareStructures(va, vb);
              if (r !== 0) return r;
            }
          }

          return 0;
        }

        // Simple type
        else {
          if (st.includes(t.a) && st.includes(t.b))
            return String(a).localeCompare(String(b));

          if (nm.includes(t.a) && nm.includes(t.b)) return +a - +b;

          if (t.a !== t.b) return t.a.localeCompare(t.b);

          return +a - +b;
        }
      }

      return Extend(compareStructures);
    }
  );
}
