namespace AVStantso {
  export namespace Catch {
    /**
     * @summary Catch handler
     * @param error Error
     */
    export type Handler<T = unknown> = (error: T) => any;

    export namespace BoN {
      export namespace Utils {
        /**
         * @summary `BoN.Utils` catch handler version
         * @param error Error
         */
        export type Handler = Catch.Handler<any>;
      }

      /**
       * @summary `BoN.Utils` catch version.
       *
       * Hendle error by default handler
       */
      export type Utils = Utils.Handler;
    }
  }

  /**
   * @summary Global catch method for errors
   */
  export interface Catch extends Catch.BoN.Utils {}

  export namespace Code {
    export namespace Catch {
      /**
       * @summary Catch filter handler
       * @param error Error
       */
      export type Filter<T = unknown> = (error: T) => boolean;
    }

    /**
     * @summary Catch helpers utility
     */
    export interface Catch extends AVStantso.Catch {
      _addFilter(...filters: [Catch.Filter, ...Catch.Filter[]]): void;
      _default: AVStantso.Catch.Handler;
    }
  }

  export interface Code {
    /**
     * @summary Catch helpers utility
     */
    Catch: Code.Catch;
  }

  const Catch = avstantso._reg('Catch', () => {
    type Handler = Catch.Handler;
    type Filter = Code.Catch.Filter;

    let def: Handler = (error) => {
      throw error;
    };

    const catchFilters: Filter[] = [];

    function DoCatch(error: any) {
      return catchFilters.length && catchFilters.some((f) => f(error))
        ? undefined
        : def(error);
    }

    DoCatch._addFilter = (...filters: [Filter, ...Filter[]]): void => {
      catchFilters.push(...filters);
    };

    Object.defineProperties(DoCatch, {
      _default: {
        get() {
          return def;
        },
        set(value: Handler) {
          def = value;
        },
      },
    });

    return DoCatch;
  });
}
