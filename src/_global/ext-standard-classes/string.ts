declare interface String {
  toCapitalized(): Capitalize<string>;
  toUncapitalized(): Uncapitalize<string>;
}

Object.defineProperty(String.prototype, 'toCapitalized', {
  value() {
    return !this ? this : this.charAt(0).toLocaleUpperCase() + this.slice(1);
  },
});

Object.defineProperty(String.prototype, 'toUncapitalized', {
  value() {
    return !this ? this : this.charAt(0).toLocaleLowerCase() + this.slice(1);
  },
});
