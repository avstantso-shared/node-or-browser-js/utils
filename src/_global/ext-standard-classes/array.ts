namespace AVStantso.Array {
  /**
   * @summary Select `Array<T>` or `ReadonlyArray<T>` by `[W]ritable`
   */
  export type RW<W extends boolean, T = unknown> = W extends true
    ? Array<T>
    : ReadonlyArray<T>;

  /**
   * @summary Key of `RW<W>`
   */
  export type Key<W extends boolean> = keyof {
    [K in keyof RW<W> as `${Extract<K, string>}`]: 0;
  };

  export namespace Key {
    /**
     * @summary Immutable array key
     */
    export type R = Key<false>;

    /**
     * @summary Mutable array key
     */
    export type RW = Key<true>;

    /**
     * @summary Mutation key only
     */
    export type W = Exclude<RW, R>;
  }

  export interface Ex<W extends boolean, T> {
    /**
     * @summary Filter all not `falsy` items
     * @returns New array with all not `falsy` items
     */
    pack(): Array<T>;

    /**
     * @summary Get last item. Array not changes
     * @returns Last item or `undefined`, if array empty. Array not changes
     */
    peek(): T;
  }
}

declare interface ReadonlyArray<T> extends AVStantso.Array.Ex<false, T> {}
declare interface Array<T> extends AVStantso.Array.Ex<true, T> {}

Object.defineProperties(Array.prototype, {
  pack: {
    value() {
      return !this ? this : this.filter((item: any) => item);
    },
  },
  peek: {
    value() {
      return !this ? undefined : this[this.length - 1];
    },
  },
});
