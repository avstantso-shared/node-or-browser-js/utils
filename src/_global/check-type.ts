namespace AVStantso {
  /**
   * @summary Checker for  `TActual` type. Its must extends `TExpected`
   * @param TActual Actual calculated type
   * @param TExpected Expected type
   * @description Used for debug and in examples
   * @example
   * type o = {id: string; version: number};
   *
   * type t = CheckType<o, Pick<o, 'id'>>;
   * type f = CheckType<o, {name: string}>;
   *                    👆⛔ Type 'o' does not satisfy the constraint '{ name: string; }'.
   */
  export type CheckType<TActual extends TExpected, TExpected> = TActual;
}
