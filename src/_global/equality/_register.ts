namespace AVStantso {
  export namespace Code {
    export namespace Equality {
      /**
       * @summary `AVStantso.Equality.Predicate` utility
       */
      export interface Predicate {
        /**
         * @summary Factory function for `AVStantso.Equality.Predicate`
         */
        Factory<T = unknown>(
          equality: AVStantso.Equality<T>
        ): AVStantso.Equality.Predicate.Factory<T>;
      }
    }

    /**
     * @summary `AVStantso.Equality` utility
     */
    export interface Equality {
      /**
       * @summary `AVStantso.Equality.Predicate` utility
       */
      Predicate: Equality.Predicate;

      /**
       * @summary Create equality from compare
       */
      From<T = unknown>(compare: AVStantso.Compare<T>): AVStantso.Equality<T>;
    }
  }

  export interface Code {
    /**
     * @summary `AVStantso.Equality` utility
     */
    Equality: Code.Equality;
  }

  export const Equality = avstantso._reg('Equality', () => {
    const PredicateFactory: Code.Equality.Predicate['Factory'] =
      <T = unknown>(equality: Equality<T>): Equality.Predicate.Factory<T> =>
      (a, notEquals?) =>
      (b) => {
        const r = equality(a, b);
        return notEquals ? !r : !!r;
      };

    const From: Code.Equality['From'] = (compare) => {
      function equality(a: any, b: any) {
        return !compare(a, b);
      }

      equality.as = () => equality;

      return equality;
    };

    return { Predicate: { Factory: PredicateFactory }, From };
  });
}
