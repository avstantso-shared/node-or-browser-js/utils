namespace AVStantso {
  /**
   * @summary Equality function for two values
   */
  export type Equality<TBase = unknown> = WithDesignTimeInfo<
    Equality.Func<TBase> & {
      /**
       * @summary Cast `Equality` function basetype
       */
      as?<T extends TBase>(): Equality<T>;
    },
    { baseType?: TBase }
  >;

  export namespace Equality {
    /**
     * @summary Equality atomic function for two values
     */
    export type Func<TBase = unknown> = <
      A extends TBase = TBase,
      B extends TBase = TBase
    >(
      a: A,
      b: B
    ) => boolean;

    /**
     * @summary Equality function from compare function
     */
    export type From<C extends Compare> = Equality<
      DesignTimeInfo<C>['baseType']
    >;

    /**
     * @summary Predicate of equality function
     */
    export type Predicate<T = unknown> = (b: T) => boolean;

    export namespace Predicate {
      /**
       * @summary Factory function for `AVStantso.Equality.Predicate`
       */
      export type Factory<T = unknown> = (
        a: T,
        notEquals?: boolean
      ) => Predicate<T>;
    }
  }
}
