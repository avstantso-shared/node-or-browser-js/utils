namespace AVStantso {
  /**
   * @summary Force cast as `T`
   */
  export type Generic<T = any> = T & any;

  export namespace Generics {
    export namespace Cast {
      export type Force<TTo extends {}> = <T extends {} = unknown>(
        obj: T
      ) => { [K in keyof TTo]?: TTo[K] };
    }
  }

  export namespace Code {
    export namespace Generics {
      export namespace Cast {
        /**
         * @summary Make force caster to type `TTo`
         */
        export interface Force {
          /**
           * @summary Make force caster to type `TTo`
           * @returns from as TTo
           */
          Factory<TTo extends {}>(): AVStantso.Generics.Cast.Force<TTo>;
        }
      }

      /**
       * @summary Cast for generics
       */
      export interface Cast {
        /**
         * @summary Cast to avoid generics problem
         * @param from as TFrom
         * @returns `from` as `any`
         */
        <TFrom = any>(from: TFrom): any;

        /**
         * @summary Cast to avoid generics problem
         * @param from as TFrom
         * @returns `from` as `TTo`
         */
        To<TTo = any, TFrom = any>(from: TFrom): TTo;

        /**
         * @summary Make force caster to type `TTo`
         */
        Force: Cast.Force;
      }
    }

    /**
     * @summary `AVStantso.Generics` utility
     */
    export interface Generics {
      /**
       * @summary Cast to avoid generics problem
       * @param from as TFrom
       * @returns `from` as `any`
       */
      Cast: Generics.Cast;
    }
  }

  export interface Code {
    /**
     * @summary `AVStantso.Generics` utility
     */
    Generics: Code.Generics;
  }

  export const Generics = avstantso._reg('Generics', () => {
    function Cast<TFrom = any>(from: TFrom): any {
      return from as any;
    }

    Cast.To = <TTo = any, TFrom = any>(from: TFrom): TTo => from as any as TTo;

    Cast.Force = {
      Factory: <TTo extends {}>(): AVStantso.Generics.Cast.Force<TTo> => Cast,
    };

    return { Cast };
  });
}
