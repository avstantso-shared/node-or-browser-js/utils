namespace AVStantso {
  export namespace Code {
    /**
     * @summary Switch helpers utility
     */
    export interface Switch {
      <
        TKey extends AVStantso.TS.Key,
        TOptions extends Switch.Options & { Value?: unknown } = {}
      >(
        getKey: (value: TOptions['Value']) => TKey | string
      ): <TCase, TKnownCases extends {} = Record<TKey, TCase>>(
        value: TOptions['Value'],
        cases: Switch.Cases<
          TKey,
          TOptions & { Case: TCase; KnownCases: TKnownCases }
        >
      ) => TCase;
    }
  }

  export interface Code {
    /**
     * @summary Switch helpers utility
     */
    Switch: Code.Switch;
  }

  export const Switch = avstantso._reg('Switch', () => {
    function KeyRegExp(key: string): RegExp {
      return key && new RegExp(`(^|\\|)${key}($|\\|)`);
    }

    const stdKeys = ['undefined', 'null'];
    const stdRegs = {} as Record<Switch.StdKeys, RegExp>;
    stdKeys.forEach((key) => (stdRegs[key as Switch.StdKeys] = KeyRegExp(key)));

    return <
      TKey extends TS.Key,
      TOptions extends Switch.Options & { Value?: unknown } = {}
    >(
      getKey: (value: TOptions['Value']) => TKey | string
    ) => {
      type Cases<TCase, TKnownCases extends {}> = Switch.Cases<
        TKey,
        TOptions & { Case: TCase; KnownCases: TKnownCases }
      >;

      function findValueByRegExp<TCase, TKnownCases extends {}>(
        regExp: RegExp,
        cases: Cases<TCase, TKnownCases>
      ): TCase {
        return ((regExp &&
          Object.entries(cases).find(([k]) => regExp.test(k))) ||
          [])[1];
      }

      function findValueByKey<TCase, TKnownCases extends {}>(
        key: string,
        cases: Cases<TCase, TKnownCases>
      ): TCase {
        return (
          !stdKeys.includes(key) && findValueByRegExp(KeyRegExp(key), cases)
        );
      }

      function _switch<TCase, TKnownCases extends {} = Record<TKey, TCase>>(
        value: TOptions['Value'],
        cases: Cases<TCase, TKnownCases>
      ): TCase {
        return (
          (undefined === value &&
            findValueByRegExp(stdRegs.undefined, cases)) ||
          (null === value && findValueByRegExp(stdRegs.null, cases)) ||
          findValueByKey(String(getKey(value)), cases) ||
          cases.default
        );
      }

      return _switch;
    };
  });
}
