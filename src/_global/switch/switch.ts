/**
 * @summary Switch case by key of value or default
 */
namespace AVStantso.Switch {
  export type StdKeys = 'undefined' | 'null' | 'default';

  export type Options = {
    Case?: unknown;
    KnownCases?: unknown;
    KeysUnion?: TS.Key;
  };

  type _Cases<
    TKey extends TS.Key,
    TOptions extends Options,
    Case = TOptions['Case'],
    KnownCases = TS.IfNotOverrided<
      Options['KnownCases'],
      TOptions['KnownCases'],
      Record<TKey, Case>
    >,
    KeysUnion extends TS.Key = TS.IfNotOverrided<
      Options['KeysUnion'],
      TOptions['KeysUnion'],
      string
    >
  > = Partial<KnownCases & Record<StdKeys, Case> & Record<KeysUnion, Case>>;

  export type Cases<
    TKey extends TS.Key,
    TOptions extends Options = {}
  > = _Cases<TKey, TOptions>;
}
