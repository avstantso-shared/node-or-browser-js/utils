namespace AVStantso {
  export namespace Extend {
    export namespace ByRecord {
      export type Original = {} | Function;

      export type Extender<
        TOriginal extends Original,
        TExtensionKey extends TS.Key,
        TExtensionItem,
        TExtensionValue = TExtensionKey
      > = (
        value: TExtensionValue,
        original?: TOriginal,
        key?: TExtensionKey
      ) => TExtensionItem;

      export type Func<
        TExtensionKey extends TS.Key,
        TExtensionValue,
        TCustomKey extends TS.Key = TExtensionKey
      > = <
        TOriginal extends Extend.ByRecord.Original,
        TExtensionItem,
        TExtension extends Record<TCustomKey, TExtensionItem>
      >(
        original: TOriginal,
        extender: Extend.ByRecord.Extender<
          TOriginal,
          TExtensionKey,
          TExtensionItem,
          TExtensionValue
        >
      ) => TOriginal & TExtension;

      export type Methods<
        TExtensionKey extends TS.Key,
        TExtensionValue,
        TCustomKey extends TS.Key = TExtensionKey
      > = Func<TExtensionKey, TExtensionValue, TCustomKey> & {
        static: Func<TExtensionKey, TExtensionValue, TCustomKey>;
      };
    }

    /**
     * @summary Add fields by record
     * @param original Entity for extend
     * @param extender Addition item by key
     * @returns Extended entity
     */
    export type ByRecord<
      TExtensionKey extends TS.Key,
      TExtensionValue
    > = Extend.ByRecord.Methods<TExtensionKey, TExtensionValue> & {
      /**
       * @summary Add fields by record with capitalized keys
       * @param original Entity for extend
       * @param extender Addition item by key
       * @returns Extended entity
       */
      Capitalized: Extend.ByRecord.Methods<
        TExtensionKey,
        TExtensionValue,
        Capitalize<Extract<TExtensionKey, string>>
      >;

      /**
       * @summary Add fields by record with uncapitalized keys
       * @param original Entity for extend
       * @param extender Addition item by key
       * @returns Extended entity
       */
      Uncapitalized: Extend.ByRecord.Methods<
        TExtensionKey,
        TExtensionValue,
        Uncapitalize<Extract<TExtensionKey, string>>
      >;
    };

    /**
     * @summary Add arbitrary properties
     * @param original Entity for extend
     * @param extention properties
     * @returns Extended entity
     */
    export type Arbitrary = <TOriginal, TExtension extends {}>(
      original: TOriginal,
      extention: TExtension | (() => TExtension)
    ) => TOriginal & TExtension;
  }

  export namespace Code {
    /**
     * @summary `AVStantso.Extend` utility
     */
    export interface Extend {
      /**
       * @summary Cast function for add extended fields
       * @param from as TFrom
       * @returns from as TTo
       */
      <TTo extends Function = any, TFrom extends Function = any>(
        from: TFrom
      ): TTo;
      /**
       * @summary Cast object for add extended fields
       * @param from as TFrom
       * @returns from as TTo
       */
      <TTo extends {} = any, TFrom extends {} = any>(from: TFrom): TTo;

      /**
       * @summary Add fields by record
       * @param keys Keys for add
       * @param getValue Value by key. If empty, "value"="key"
       * @param original Entity for extend
       * @param extender Addition item by key
       * @returns (original, extender) => <Extended_Entity>
       */
      ByRecord<
        TExtensionKey extends AVStantso.TS.Key,
        TExtensionValue = TExtensionKey
      >(
        keys: TExtensionKey[] | (() => TExtensionKey[]),
        getValue?: (key: TExtensionKey) => TExtensionValue
      ): Extend.ByRecord<TExtensionKey, TExtensionValue>;

      /**
       * @summary Add arbitrary properties
       * @param original Entity for extend
       * @param extention properties
       * @returns Extended entity
       */
      Arbitrary: Extend.Arbitrary;
    }
  }

  export interface Code {
    /**
     * @summary `AVStantso.Extend` utility
     */
    Extend: Code.Extend;
  }

  /**
   * @summary Cast object or function for add extended fields
   * @param from as TFrom
   * @returns from as TTo
   */
  export const Extend = avstantso._reg('Extend', ({ JS, Generics }) => {
    const Extend: Code.Extend = <TTo = any, TFrom = any>(from: TFrom): TTo =>
      Generics.Cast.To<TTo, TFrom>(from);

    function extendByRecord<
      TExtensionKey extends TS.Key,
      TExtensionValue = TExtensionKey
    >(
      keys: TExtensionKey[] | (() => TExtensionKey[]),
      getValue?: (key: TExtensionKey) => TExtensionValue
    ): Extend.ByRecord<TExtensionKey, TExtensionValue> {
      function MKM<TCustomKey extends TS.Key = TExtensionKey>(
        transformKey?: (key: TExtensionKey) => TS.Key
      ): Extend.ByRecord.Methods<TExtensionKey, TExtensionValue, TCustomKey> {
        function MK(
          isStatic?: boolean
        ): Extend.ByRecord.Func<TExtensionKey, TExtensionValue, TCustomKey> {
          const f0: any = <
            TOriginal extends Extend.ByRecord.Original,
            TExtensionItem,
            TExtension extends Record<TCustomKey, TExtensionItem>
          >(
            original: TOriginal,
            extender: Extend.ByRecord.Extender<
              TOriginal,
              TExtensionKey,
              TExtensionItem,
              TExtensionValue
            >
          ): TOriginal & TExtension => {
            (JS.is.function(keys) ? keys() : keys).forEach((key) => {
              if (transformKey && !JS.is.string(key))
                throw new Error(
                  `Extend.ByRecord.Capitalized/Uncapitalized not support "${typeof key}" keys ("${String(
                    key
                  )}")`
                );

              const customKey = (
                transformKey ? transformKey(Generics.Cast.To(key)) : key
              ) as TCustomKey;

              const _extender = () =>
                extender(
                  getValue ? getValue(key) : Generics.Cast.To(key),
                  original,
                  key
                );

              if (isStatic) JS.set.raw(original, customKey, _extender());
              else
                Object.defineProperty(original, customKey, {
                  get() {
                    return _extender();
                  },
                });
            });

            return Generics.Cast.To(original);
          };

          return f0;
        }

        const f1: any = MK();
        f1.static = MK(true);
        return f1;
      }

      const f2: any = MKM();

      f2.Capitalized = MKM<Capitalize<Extract<TExtensionKey, string>>>((key) =>
        String(key).toCapitalized()
      );

      f2.Uncapitalized = MKM<Uncapitalize<Extract<TExtensionKey, string>>>(
        (key) => String(key).toUncapitalized()
      );

      return f2;
    }

    function extendArbitrary<TOriginal, TExtension extends {}>(
      original: TOriginal,
      extention: TExtension | (() => TExtension)
    ): TOriginal & TExtension {
      extention &&
        Object.entries(
          JS.is.function(extention) ? extention() : extention
        ).forEach(([key, value]) => JS.set.raw(original, key, value));

      return Generics.Cast.To(original);
    }

    Extend.ByRecord = extendByRecord;
    Extend.Arbitrary = extendArbitrary;

    return Extend;
  });
}
