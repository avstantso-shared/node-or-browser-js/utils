namespace AVStantso {
  export interface Code {
    /**
     * @summary External state for debugging
     * @description Used in tests, for separate initilizing code and test code
     * @example
     * // in debugging _global file
     * namespace AVStantso {
     *   avstantso._reg('foo', () => () => {
     *     if (avstantso.debugState) console.log(`foo called!`);
     *
     *     return 123;
     *   });
     *
     *   avstantso.foo(); // don`t write to console
     * }
     *
     * // in test file
     * avstantso.debugState = true;
     *
     * avstantso.foo(); // write to console
     */
    debugState: unknown;
  }

  if (!('debugState' in avstantso)) {
    let debugState: unknown;
    Object.defineProperties(avstantso, {
      debugState: {
        get: () => debugState,
        set: (value) => (debugState = value),
      },
    });
  }
}
