namespace AVStantso {
  /**
   * @summary Interface list of atomic objects types
   */
  export interface AtomicObjects {
    Date: Date;
    Buffer: Buffer;
  }

  export namespace Code {
    /**
     * @summary Atomic objects list management
     */
    export interface AtomicObjects {
      classes: ReadonlyArray<new (...args: any[]) => any>;
      register(simpleObjectClass: new (...args: any[]) => any): void;
      is(testObject: object): boolean;
    }
  }

  export interface Code {
    /**
     * @summary Atomic objects list management
     */
    AtomicObjects: Code.AtomicObjects;
  }

  export const AtomicObjects = avstantso._reg('AtomicObjects', () => {
    const classes: (new (...args: any[]) => any)[] = [];

    function register(simpleObjectClass: new (...args: any[]) => any) {
      if (!classes.includes(simpleObjectClass)) classes.push(simpleObjectClass);
    }

    function is(testObject: object): boolean {
      return classes.some((c) => testObject instanceof c);
    }

    return { classes, register, is };
  });
}
