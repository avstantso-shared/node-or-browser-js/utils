namespace AVStantso {
  /**
   * @summary Create structural type with specified field and value
   * @param F First param
   * @param S Second param
   * @returns
   * • If `F` is `TS.Key` — `S` will be interpret as value ⇒ `{[F]:TS.Type.Resolve<S>}`
   *
   * • If `F` is `TS.Type.Arr` — `S` will be ignored ⇒ `{[F[0]]:TS.Type.Resolve<F[1]>}`
   *
   * • If `F` is other array ⇒ `never`
   *
   * • If `F` is `TS.Type.KeyDef` — `S` will be ignored ⇒ `{[F['K']]:TS.Type.Resolve<F>}`
   *
   * • If `F` is other object — `S` will be interpret as `keyof F` ⇒ `Peek<F,S>` or `never` if `S` is bad key
   * @example
   * type result = { id: string;};
   *
   * type key_value = Provider<'id', string>; // =result
   *
   * type bad_array = Provider<[1, 2, 3, 4]>; // never
   * type good_array = Provider<['id', 'string']>;  // =result
   *
   * type keydef_l = Provider<{ K: 'id'; L: 'string' }>; // =result
   * type keydef_t = Provider<{ K: 'id'; T: string }>; // =result
   *
   * type obj = { id: string; name: string; active?: boolean };
   *
   * type bad_obj_field = Provider<obj, 'id1'>; // never
   * type good_obj_field = Provider<obj, 'id'>; // =result
   */
  export type Provider<
    F extends Provider.First,
    S = undefined
  > = F extends object
    ? F extends any[]
      ? F extends TS.Type.Arr<infer K, infer L>
        ? Provider<K, L>
        : never
      : F extends TS.Type.KeyDef
      ? Provider<F['K'], F>
      : [Extract<S, keyof F>] extends [never]
      ? never
      : Provider<Extract<S, keyof F>, F[Extract<S, keyof F>]>
    : {
        [K in Extract<F, TS.Key>]: TS.Type.Resolve<S>;
      };

  export namespace Provider {
    /**
     * @summary Provider `F` type parameter constraint
     */
    export type First = object | TS.Key;

    /**
     * @summary Constraint for type parameter extends `Provider`
     */
    export type Constraint = Provider<TS.Key, any>;
  }
}
