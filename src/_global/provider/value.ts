namespace AVStantso.Provider {
  /**
   * @summary Value from `Provider` or `Provider.Union`
   * @param U `Provider.Union`
   * @param P `Provider`
   * @returns If `U` is `never` — value from `P`, else — value from `U`
   * @see Provider type
   * @example
   * type p = Provider<'id', string>;
   * type v_key_value_u1 = Value<'abc', p>; // "abc"
   * type v_key_value_u2 = Value<{ id: 'defg' }, p>; // "defg"
   */
  export type Value<
    U extends Union<P>,
    P extends Provider.Constraint
  > = U extends P ? U[keyof P] : U;

  export namespace Value {
    /**
     * @summary Make value from `Provider` or `Provider.Union` created by `Provider` params
     * @param U `Union` for extract value
     * @param F First `Provider` param
     * @param S Second `Provider` param
     * @see Provider type
     * @example
     * type u_key_value = Value.Make<{id: 'abc'},  'id', string>; // "abc"
     */
    export type Make<
      U extends Union<Provider<F, S>>,
      F extends First,
      S = unknown
    > = Value<U, Provider<F, S>>;
  }
}
