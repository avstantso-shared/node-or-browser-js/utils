namespace AVStantso.Provider {
  /**
   * @summary Stricted `Provider` constructors
   */
  export namespace Strict {
    /**
     * @summary Create `Provider` by key `K` and value `V`
     */
    export type KeyValue<K extends TS.Key, V> = Provider<K, V>;

    /**
     * @summary Create `Provider` by `TS.Type.Arr`
     */
    export type Arr<A extends TS.Type.Arr> = Provider<A>;

    /**
     * @summary Create `Provider` by `TS.Type.KeyDef`
     */
    export type KeyDef<KD extends TS.Type.KeyDef> = Provider<KD>;

    /**
     * @summary Create `Provider` by structure `O` and strict key `K`
     */
    export type Structure<O extends object, K extends keyof O> = Provider<O, K>;

    export namespace Structure {
      /**
       * @summary Create `Provider` by structure `O` and NOT strict key `K`
       */
      export type Raw<O extends object, K extends TS.Key> = Provider<O, K>;
    }
  }
}
