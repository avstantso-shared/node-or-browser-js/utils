namespace AVStantso {
  export namespace Provider {
    /**
     * @summary Compare two `Provider.Value` from `Provider.Union` function
     * @param P `Provider`
     * @see Provider type
     */
    export type Compare<P extends Provider.Constraint> = AVStantso.Compare<
      Union<P>
    >;

    export namespace Compare {
      /**
       * @summary Make compare two `Provider.Value` from `Provider.Union` function
       * @param F First `Provider` param
       * @param S Second `Provider` param
       * @see Provider type
       */
      export type Make<F extends First, S = undefined> = Compare<
        Provider<F, S>
      >;
    }
  }

  export namespace Code {
    export namespace Provider {
      export namespace Compare {
        /**
         * @summary `AVStantso.Provider.Compare` utility options
         */
        export type Options<
          A extends AVStantso.TS.Type.Arr = AVStantso.TS.Type.Arr
        > = {
          /**
           * @summary Extract value method
           */
          extract?: AVStantso.Provider.Extract.Make<A>;
        };
      }

      /**
       * @summary `AVStantso.Provider.Compare` utility
       */
      export interface Compare {
        <K extends AVStantso.TS.Key, L extends AVStantso.TS.Type.Literal>(
          key: K,
          type?: L,
          options?: Compare.Options<AVStantso.TS.Type.Arr<K, L>>
        ): AVStantso.Provider.Compare.Make<K, L>;

        <Arr extends AVStantso.TS.Type.Arr>(
          def: Arr,
          options?: Compare.Options<Arr>
        ): AVStantso.Provider.Compare.Make<Arr>;
      }
    }

    export interface Provider {
      /**
       * @summary `AVStantso.Provider.Compare` utility
       */
      Compare: Provider.Compare;
    }
  }

  avstantso._reg.Provider(
    'Compare',
    ({ TS, Compare: { Extend, ToNulls }, Provider: { Extract } }) => {
      function Compare(...params: any[]) {
        const L = TS.Type.Literal;

        const [key, type] = TS.Type.Arr.Parse(params);

        const { extract } = (params[Array.isArray(params[0]) ? 1 : 2] || {
          extract: Extract(key, type),
        }) as Code.Provider.Compare.Options;

        function compare(a: any, b: any) {
          const ea: any = extract(a);
          const eb: any = extract(b);

          const toNulls = ToNulls(type === L.number);
          {
            const nr = toNulls(ea) - toNulls(eb);
            if (nr) return nr;
          }

          if (L.Buffer === type && a instanceof Buffer && b instanceof Buffer)
            return a.toString('hex').localeCompare(b.toString('hex'));

          if (L.string !== type && L.symbol !== type) return +ea - +eb;

          const sa = String(ea || '');
          const sb = String(eb || '');

          return sa.localeCompare(sb);
        }

        return Extend(compare);
      }

      return Compare;
    }
  );
}
