namespace AVStantso {
  export namespace Code {
    /**
     * @summary `AVStantso.Provider` utility
     */
    export interface Provider {}
  }

  export interface Code {
    /**
     * @summary `AVStantso.Provider` utility
     */
    Provider: Code.Provider;
  }

  export const Provider = avstantso._reg('Provider', {});
}
