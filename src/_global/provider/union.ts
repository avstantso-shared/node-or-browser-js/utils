namespace AVStantso.Provider {
  /**
   * @summary Union: `Provider | Value<Provider>`
   * @param P `Provider`
   * @see Provider type
   * @example
   * type u_key_value = Union<Provider<'id', string>>; // string | {id: string;}
   */
  export type Union<P extends Provider.Constraint> = P | P[keyof P];

  export namespace Union {
    /**
     * @summary Make union `Provider | Value<Provider>` from `Provider` params
     * @param F First `Provider` param
     * @param S Second `Provider` param
     * @see Provider type
     * @example
     * type u_key_value = Union.Make<'id', string>; // string | {id: string;}
     */
    export type Make<F extends First, S = undefined> = Union<Provider<F, S>>;
  }
}
