namespace AVStantso {
  export namespace Provider {
    /**
     * @summary A function signature of: `candidate` is `Provider` object
     * @param P `Provider`
     * @see Provider type
     */
    export type Is<P extends Provider.Constraint> = <T extends P = P>(
      candidate: unknown
    ) => candidate is T;

    export namespace Is {
      /**
       * @summary Make a function signature of: `candidate` is `Provider` object
       * @param F First `Provider` param
       * @param S Second `Provider` param
       * @see Provider type
       */
      export type Make<F extends First, S = undefined> = Is<Provider<F, S>>;
    }
  }

  export namespace Code {
    export namespace Provider {
      /**
       * @summary `AVStantso.Provider.Is` utility
       */
      export interface Is {
        <K extends AVStantso.TS.Key, L extends AVStantso.TS.Type.Literal>(
          key: K,
          type?: L
        ): AVStantso.Provider.Is.Make<K, L>;

        <Arr extends AVStantso.TS.Type.Arr>(
          def: Arr
        ): AVStantso.Provider.Is.Make<Arr>;
      }
    }

    export interface Provider {
      /**
       * @summary `AVStantso.Provider.Is` utility
       */
      Is: Provider.Is;
    }
  }

  avstantso._reg.Provider('Is', ({ TS, JS }) => (...params: any[]) => {
    const [key, type] = TS.Type.Arr.Parse(params);

    function is<P extends object>(candidate: unknown): candidate is P {
      if (null === candidate || !JS.is.object(candidate) || !(key in candidate))
        return false;

      const value = JS.get.raw(candidate, key);

      return !type || TS.Type.isLiteralValue(type, value);
    }

    return is;
  });
}
