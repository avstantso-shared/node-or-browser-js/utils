namespace AVStantso {
  export namespace Provider {
    /**
     * @summary Extract `Provider.Value` from `Provider.Union` function
     * @param P `Provider`
     * @see Provider type
     */
    export type Extract<P extends Provider.Constraint> = <
      T extends Value<Union<P>, P>
    >(
      union: Union<P>
    ) => T;

    export namespace Extract {
      /**
       * @summary Make extract `Provider.Value` from `Provider.Union` function
       * @param F First `Provider` param
       * @param S Second `Provider` param
       * @see Provider type
       */
      export type Make<F extends First, S = undefined> = Extract<
        Provider<F, S>
      >;
    }
  }

  export namespace Code {
    export namespace Provider {
      /**
       * @summary `AVStantso.Provider.Extract` utility
       */
      export interface Extract {
        <K extends AVStantso.TS.Key, L extends AVStantso.TS.Type.Literal>(
          key: K,
          type?: L
        ): AVStantso.Provider.Extract.Make<K, L>;

        <Arr extends AVStantso.TS.Type.Arr>(
          def: Arr
        ): AVStantso.Provider.Extract.Make<Arr>;
      }
    }

    export interface Provider {
      /**
       * @summary `AVStantso.Provider.Extract` utility
       */
      Extract: Provider.Extract;
    }
  }

  avstantso._reg.Provider('Extract', ({ TS, JS }) => (...params: any[]) => {
    const [key, type] = TS.Type.Arr.Parse(params);

    function extract(union: unknown) {
      return undefined === union ||
        null === union ||
        TS.Type.isLiteralValue(type, union)
        ? union
        : JS.is.object(union)
        ? JS.get.raw(union, key)
        : undefined;
    }

    return extract;
  });
}
