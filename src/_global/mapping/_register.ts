namespace AVStantso {
  export namespace Code {
    export namespace Mapping {
      /**
       * @summary Mapping factoru function
       */
      export interface Factory<
        TMap extends object,
        TProviderFiled extends AVStantso.TS.Key
      > {
        <
          TTemplate extends object,
          TValueBase = TMap[keyof TMap],
          TValueUnionBase extends AVStantso.Provider.Union.Make<
            TProviderFiled,
            TValueBase
          > = AVStantso.Provider.Union.Make<TProviderFiled, TValueBase>
        >(
          template: TTemplate
        ): AVStantso.Mapping<
          TMap,
          TTemplate,
          TProviderFiled,
          TValueBase,
          TValueUnionBase
        >;
      }
    }

    /**
     * @summary Mapping helpers utility
     */
    export interface Mapping {
      /**
       * @summary Create finded in `template` of the specified type a property with a name corresponding to `TMap` type
       * @param TMap Type of map for create `Mapping.Factory` function
       * @param TProviderFiled Type of provider field
       * @param field Provider field. If `undefined` — provider values not supports
       * @param keyFromValue Get key from value function
       * @param map Source of `TMap` type parameter. Not used in runtime
       */
      <
        TMap extends object,
        TProviderFiled extends AVStantso.TS.Key = undefined
      >(
        field: TProviderFiled,
        keyFromValue: AVStantso.Mapping.KeyFromValue<TMap>,
        map?: TMap
      ): Mapping.Factory<TMap, TProviderFiled>;

      /**
       * @summary Create finded in `template` of the specified type a property with a name corresponding to `TMap` type
       * @param TMap Type of map for create `Mapping.Factory` function
       * @param field Provider field. If `undefined` — provider values not supports
       * @param keyFromValue Get key from value function
       * @param map Source of `TMap` type parameter. Not used in runtime
       */
      <
        TMap extends object,
        TProviderFiled extends AVStantso.TS.Key = undefined
      >(
        keyFromValue: AVStantso.Mapping.KeyFromValue<TMap>,
        map?: TMap
      ): Mapping.Factory<TMap, TProviderFiled>;
    }
  }

  export interface Code {
    /**
     * @summary Mapping helpers utility
     */
    Mapping: Code.Mapping;
  }

  export const Mapping = avstantso._reg(
    'Mapping',
    ({ JS }) =>
      (...params: any[]) => {
        const field = JS.is.string(params[0]) ? params[0] : undefined;
        const keyFromValue = params[field ? 1 : 0];

        return (template: any) => (value: any) =>
          JS.get(template, keyFromValue(value));
      }
  );
}
