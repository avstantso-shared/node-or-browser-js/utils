namespace AVStantso {
  export namespace Mapping {
    /**
     * @summary Get key by value
     */
    export type KeyFromValue<
      TMap extends object,
      TKey extends keyof TMap = keyof TMap,
      TValue extends TMap[TKey] = TMap[TKey]
    > = (value: TValue) => TKey;
  }

  type _Result<
    TMap extends object,
    TTemplate extends object,
    TProviderFiled extends TS.Key,
    TValueBase,
    TValue extends Provider.Union.Make<TProviderFiled, TValueBase>,
    TExclude extends Provider.Union.Make<TProviderFiled, TValueBase>,
    VF extends Provider.Value.Make<
      TValue,
      TProviderFiled,
      TValueBase
    > = Provider.Value.Make<TValue, TProviderFiled, TValueBase>,
    EF extends Provider.Value.Make<
      TExclude,
      TProviderFiled,
      TValueBase
    > = Provider.Value.Make<TExclude, TProviderFiled, TValueBase>,
    V extends Exclude<VF, EF> = Exclude<VF, EF>,
    M extends TS.Structure.Reverse<TMap> = TS.Structure.Reverse<TMap>,
    MK extends Extract<V, keyof M> = Extract<V, keyof M>,
    TK extends Extract<M[MK], keyof TTemplate> = Extract<M[MK], keyof TTemplate>
  > = TTemplate[TK];

  /**
   * @summary Find in `TTemplate` of the specified type a property with a name corresponding to `TMap` type
   */
  export type Mapping<
    TMap extends object,
    TTemplate extends object,
    TProviderFiled extends TS.Key = undefined,
    TValueBase = TMap[keyof TMap],
    TValueUnionBase extends Provider.Union.Make<
      TProviderFiled,
      TValueBase
    > = Provider.Union.Make<TProviderFiled, TValueBase>
  > = TS.And<[TS.IfStructure<TMap>, TS.IfStructure<TTemplate>]> extends true
    ? <
        TValue extends TValueUnionBase,
        TExclude extends TValueUnionBase = never
      >(
        value: TValue,
        exclude?: TExclude
      ) => _Result<
        TMap,
        TTemplate,
        TProviderFiled,
        TValueBase,
        TValue,
        TExclude
      >
    : never;
}
