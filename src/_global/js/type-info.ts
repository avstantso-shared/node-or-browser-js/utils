namespace AVStantso {
  export namespace JS {
    export namespace TypeInfo {
      export type Provider = {
        /**
         * @summary Runtime type information for objects
         */
        typeInfo: TypeInfo;
      };
    }

    /**
     * @summary Runtime type information for objects
     */
    export type TypeInfo = NodeJS.ReadOnlyDict<Type>;
  }

  export namespace Code {
    export interface JS {
      /**
       * @summary Check exists `typeInfo` in `object` or `Function`
       */
      hasTypeInfo(
        candidate: unknown
      ): candidate is AVStantso.JS.TypeInfo.Provider;

      /**
       * @summary Try get `typeInfo` from `object` or `Function` if it exists
       */
      tryTypeInfo(candidate: unknown): AVStantso.JS.TypeInfo;
    }
  }

  avstantso._reg.JS(
    'hasTypeInfo',
    ({ JS: { is, get } }) =>
      (candidate: unknown): candidate is JS.TypeInfo.Provider =>
        candidate &&
        (is.function(candidate) || is.object(candidate)) &&
        // Instead of `'typeInfo' in candidate` for Proxy cases
        get.raw(candidate, 'typeInfo')
  );

  avstantso._reg.JS(
    'tryTypeInfo',
    ({ JS: { hasTypeInfo } }) =>
      (candidate: unknown) =>
        hasTypeInfo(candidate) as Generic<JS.TypeInfo>
  );
}
