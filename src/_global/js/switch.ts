namespace AVStantso {
  export namespace JS {
    /**
     * @summary Switch by `JS.Types`
     */
    export namespace Switch {
      // @ts-ignore fix CI test TS2589: Type instantiation is excessively deep and possibly infinite
      export type KeysUnion = TS.Key.Union<
        Type | 'null',
        [
          'string',
          'number',
          'bigint',
          'boolean',
          'symbol',
          'undefined',
          'object',
          'function',
          'null'
        ]
      >;

      export namespace Case {
        export type Func<TResult, TT> = <T extends TT = TT>(
          value?: T,
          ...params: any[]
        ) => TResult extends false | true ? boolean : TResult;
      }

      export type Case<TResult, TT> = TResult extends Function
        ? Case.Func<TResult, TT>
        : Case.Func<TResult, TT> | TResult;

      export type KnownCases<TResult> = {
        [K in AVStantso.JS.Type]?: Case<TResult, AVStantso.TS.Type.Resolve<K>>;
      };

      export type Cases<TResult> = AVStantso.Switch.Cases<
        Type,
        {
          Case: Case<TResult, unknown>;
          KeysUnion: KeysUnion;
          KnownCases: KnownCases<TResult>;
        }
      >;
    }
  }

  export namespace Code {
    const _switchType = avstantso.Switch<AVStantso.JS.Type>(
      (value) => typeof value
    );

    const _switch = <TResult = unknown, TValue = any>(
      value: TValue,
      cases: AVStantso.JS.Switch.Cases<TResult>
    ): TResult => {
      const _case: unknown = _switchType(value, cases);
      return 'function' === typeof _case ? _case(value) : _case;
    };

    _switch.extract = <TResult = unknown, TValue = any>(
      list: ReadonlyArray<TValue>,
      cases: AVStantso.JS.Switch.Cases<TResult>
    ) => {
      for (let i = 0; i < list.length; i++) {
        const r = _switch(list[i], cases);
        if (undefined !== r) return r;
      }

      return undefined;
    };

    export interface JS {
      /**
       * @summary Switch value by typeof key function or default key
       */
      switch: typeof _switch;
    }

    avstantso._reg.JS('switch', () => _switch);
  }
}
