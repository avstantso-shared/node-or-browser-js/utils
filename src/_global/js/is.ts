namespace AVStantso {
  export namespace Code {
    export namespace JS {
      export namespace Is {
        export type Func<B> = <T extends B = B>(
          candidate: unknown,
          set?: T[]
        ) => candidate is T;

        export type TypeFunc = <T>(
          type: AVStantso.JS.Type,
          candidate: unknown,
          set?: T[]
        ) => candidate is T;

        export type ClassFunc<B extends object = object> = <T extends B = B>(
          objClass: new (...args: any) => T,
          candidate: unknown,
          set?: (new (...args: any) => T)[]
        ) => candidate is T;

        export type ByTypes = {
          [K in AVStantso.JS.Type]: Func<AVStantso.TS.Type.Resolve<K>>;
        };

        export type ByClasses = {
          class: ClassFunc;
          error: ClassFunc<Error>;
        };
      }

      export type Is = Is.TypeFunc & Is.ByTypes & Is.ByClasses;
    }

    export interface JS {
      /**
       * @summary Wrapper on `typeof XXX === YYY` and `instanceof`
       */
      is: JS.Is;
    }
  }

  avstantso._reg.JS('is', () => {
    const is: Code.JS.Is.TypeFunc & Code.JS.Is.ByClasses = <T>(
      type: JS.Type,
      candidate: unknown,
      set?: T[]
    ): candidate is T =>
      type === typeof candidate && (!set || set.includes(candidate as T));

    const isClass: Code.JS.Is.ClassFunc = <T extends object>(
      objClass: new (...args: any) => T,
      candidate: unknown,
      set?: (new (...args: any) => T)[]
    ): candidate is T => {
      try {
        return (
          'object' === typeof candidate &&
          null != candidate &&
          candidate instanceof objClass &&
          (!set || set.includes(candidate as new (...args: any) => T))
        );
      } catch (e) {
        e.candidate = candidate;
        throw e;
      }
    };

    is.class = isClass;
    is.error = isClass;

    Object.values(avstantso.JS.Types).forEach((type) =>
      Object.defineProperty(is, type, {
        value: <T>(candidate: unknown, set?: T[]) => is(type, candidate, set),
        writable: false,
      })
    );

    return is;
  });
}
