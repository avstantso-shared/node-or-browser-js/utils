namespace AVStantso {
  /**
   * @summary JavaScript helpers
   */
  export namespace JS {
    /**
     * @summary JS types (available resault of `typeof`).
     *
     * Equals `TS.Type.Literal` except names of known classes
     */
    export type Type = keyof {
      [K in TS.Type.Literal as K extends Capitalize<K> ? never : K]: 0;
    };
  }

  export namespace Code {
    export namespace JS {
      /**
       * @summary Map of JS types (available resault of `typeof`).
       */
      export type Types = Pick<
        AVStantso.Code.TS.Type.Literal,
        AVStantso.JS.Type
      >;
    }

    /**
     * @summary JavaScript helpers utility
     */
    export interface JS extends JS.Types {
      /**
       * @summary Map of JS types (available resault of `typeof`).
       */
      Types: JS.Types;
    }
  }

  export interface Code {
    /**
     * @summary JavaScript helpers utility
     */
    JS: Code.JS;
  }

  export const JS = avstantso._reg('JS', ({ TS }) => {
    const Types: Code.JS.Types = { ...TS.Type.Literal };

    Object.keys(Types).forEach((key) => {
      if (key === key.toCapitalized()) delete (Types as any)[key];
    });

    return { ...Types, Types };
  });
}
