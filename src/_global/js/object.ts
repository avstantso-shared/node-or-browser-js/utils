namespace AVStantso {
  export namespace Code {
    export namespace JS {
      type OF = object | Function;
      type Key = AVStantso.TS.Key;

      /**
       * @summary Get object property by `key`
       */
      export type Get = {
        <
          TProperty = unknown,
          TObject extends OF = OF,
          TKey extends keyof TObject = keyof TObject
        >(
          obj: TObject,
          key: TKey
        ): TProperty;

        /**
         * @summary Get object property by `key` `⚠ without typechecks`
         */
        raw<TProperty = any, TObject = any, TKey extends Key = Key>(
          obj: TObject,
          key: TKey
        ): TProperty;
      };

      /**
       * @summary Set object `property` by `key`
       */
      export type Set = {
        <
          TProperty = unknown,
          TObject extends OF = OF,
          TKey extends keyof TObject = keyof TObject
        >(
          obj: TObject,
          key: TKey,
          property: TProperty
        ): TObject;

        /**
         * @summary Set object `property` by `key`.
         * ⚠ Suppress `TypeError: Cannot set property...` error
         */
        safe<
          TProperty = unknown,
          TObject extends OF = OF,
          TKey extends keyof TObject = keyof TObject
        >(
          obj: TObject,
          key: TKey,
          property: TProperty
        ): TObject;

        /**
         * @summary Set object `property` by `key` `⚠ without typechecks`
         */
        raw: {
          <TProperty = any, TObject = any, TKey extends Key = Key>(
            obj: TObject,
            key: TKey,
            property: TProperty
          ): TObject;

          /**
           * @summary Set object `property` by `key` `⚠ without typechecks`
           * ⚠ Suppress `TypeError: Cannot set property...` error
           */
          safe<TProperty = any, TObject = any, TKey extends Key = Key>(
            obj: TObject,
            key: TKey,
            property: TProperty
          ): TObject;
        };
      };

      /**
       * @summary Initialize property by `key` and return it if not exists,
       *
       * else return exists property by `key` and ignore `property`
       */
      export type GetOrSet = {
        <
          TProperty = unknown,
          TObject extends OF = OF,
          TKey extends keyof TObject = keyof TObject
        >(
          obj: TObject,
          key: TKey,
          property: TProperty
        ): TProperty;

        /**
         * @summary Initialize property by `key` and return it if not exists,
         *
         * else return exists property by `key` and ignore `property`.
         *
         * `⚠ without typechecks`
         */
        raw<TProperty = any, TObject = any, TKey extends Key = Key>(
          obj: TObject,
          key: TKey,
          property: TProperty
        ): TProperty;
      };

      /**
       * @summary Set object properties by `patch` object
       */
      export type Patch = {
        <TPatch extends Partial<OF> = Partial<OF>, TObject extends OF = OF>(
          obj: TObject,
          patch: TPatch
        ): TObject;

        /**
         * @summary Set object properties by `patch` object `⚠ without typechecks`
         */
        raw<TPatch = any, TObject = any>(obj: TObject, patch: TPatch): TObject;
      };

      /**
       * @summary Low level clone object or array without classes check
       * @param obj object for clone
       * @param walk recursion walk
       */
      export type Clone = {
        <TTo extends OF = any, TFrom extends OF = any>(
          obj: TFrom,
          walk: (data: any) => any
        ): TTo;
      };
    }

    export interface JS {
      /**
       * @summary Get object property by `key`
       */
      get: JS.Get;

      /**
       * @summary Set object `property` by `key`
       */
      set: JS.Set;

      /**
       * @summary Initialize property by `key` and return it if not exists,
       *
       * else return exists property by `key` and ignore `property`
       */
      getOrSet: JS.GetOrSet;

      /**
       * @summary Set object properties by `patch` object
       */
      patch: JS.Patch;

      /**
       * @summary Low level clone object or array without classes check
       * @param obj object for clone
       * @param walk recursion walk
       */
      clone: JS.Clone;
    }
  }

  if (!avstantso.JS.get) {
    function objectGetProperty(obj: any, key: any): any {
      return obj[key];
    }

    function objectSetProperty(obj: any, key: any, property: any) {
      obj[key] = property;
      return obj;
    }

    objectSetProperty.safe = (obj: any, key: any, property: any) => {
      try {
        obj[key] = property;
      } catch (e) {
        if (!`${e}`.startsWith(`TypeError: Cannot set property`)) throw e;
      }
      return obj;
    };

    function objectGetOrSetProperty(obj: any, key: any, property: any): any {
      if (!obj.hasOwnProperty(key)) obj[key] = property;

      return obj[key];
    }

    function objectPatchProperties(obj: any, patch: any) {
      Object.entries(patch).forEach(([key, property]) => (obj[key] = property));

      return obj;
    }

    function objectClone(obj: any, walk: (data: any) => any): any {
      const result: any = Array.isArray(obj)
        ? obj.map(walk)
        : Object.entries(obj).reduce(
            (r, [k, v]) => objectSetProperty(r, k, walk(v)),
            {}
          );

      return result;
    }

    const withRaw = {
      get: objectGetProperty,
      set: objectSetProperty,
      getOrSet: objectGetOrSetProperty,
      patch: objectPatchProperties,
    };
    Object.values(withRaw).forEach((value: any) => (value.raw = value));

    Object.entries({ ...withRaw, clone: objectClone }).forEach(([key, value]) =>
      Object.defineProperty(avstantso.JS, key, { value, writable: false })
    );
  }
}
