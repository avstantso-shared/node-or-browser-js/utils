namespace AVStantso {
  /**
   * @summary Interface for access to `AVStantso` namespace code through `avstantso` global property
   */
  export interface Code {}

  export namespace Code {
    export namespace _Reg {
      type Initializer<
        T,
        TField extends keyof T,
        P extends Partial<T[TField]> = Partial<T[TField]>,
        F extends (code: Partial<AVStantso.Code>) => P = (
          code: Partial<AVStantso.Code>
        ) => P
      > = P extends Function ? F : F | P;

      export type Method<T = any> = <TField extends keyof T>(
        field: TField,
        initializer: Initializer<T, TField>
      ) => T[TField];

      export type Recursive<T> = Method<T> & {
        [K in keyof T]: AVStantso.TS.IfStructure<T[K]> extends true
          ? Recursive<T[K]>
          : never;
      };
    }

    /**
     * @summary Access to registration proxy structure
     */
    export type _Reg = _Reg.Recursive<Code>;
  }
}

/**
 * @summary Access to `AVStantso` namespace code
 * @description Lowercase uses in `avstantso` for avoid errors in `Jest`
 *
 * ⚠ In all cases usage constants and functions from namespace `AVStantso`  this must be  through `avstantso` global variable
 *
 * ⚠ All exported namespace `AVStantso` constants and functions must be expressed through `avstantso` global variable
 */
var avstantso = {} as AVStantso.Code & {
  /**
   * @summary Access to registration proxy structure
   */
  _reg: AVStantso.Code._Reg;
};

// A.V.Stantso:
//   To bypass `Jest` restrictions on global variables,
//   + rewrite protection
Object.defineProperties(globalThis, {
  avstantso: {
    value: avstantso,
    writable: false,
  },
});

// A.V.Stantso:
//   For use in import in modules code
Object.defineProperties(globalThis, {
  AVStantso: {
    get() {
      return avstantso;
    },
  },
});

Object.defineProperties(avstantso, {
  _reg: {
    get() {
      function level(parent: any): any {
        const reg: AVStantso.Code._Reg.Method = (field, initializer) => {
          const value =
            'function' === typeof initializer
              ? initializer(avstantso)
              : initializer;

          if (!parent.hasOwnProperty(field))
            Object.defineProperty(parent, field, { value, writable: false });

          return value;
        };

        return new Proxy(reg, {
          get(target, p) {
            return target.hasOwnProperty(p)
              ? target[p as keyof typeof target]
              : parent.hasOwnProperty(p)
              ? level(parent[p])
              : undefined;
          },
        });
      }

      return level(avstantso);
    },
  },
});
