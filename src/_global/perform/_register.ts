namespace AVStantso {
  type SysPromise<T> = Promise<T>;
  type NF = TS.Type.Not.Function;

  /**
   * @param T must not be a function
   */
  export type Perform<T extends NF, TParams extends any[] = never> =
    | T
    | ((...params: TParams) => T);

  export namespace Perform {
    /**
     * @summary This type able passed into `Perform`
     */
    export type Able = NF;

    export namespace Options {
      export type Is = <T extends NF, TParams extends any[] = never>(
        candidate: unknown
      ) => candidate is Perform.Options<T, TParams>;
    }

    export interface Options<T extends NF, TParams extends any[] = never> {
      afterCallback?: (r?: T) => unknown;
      params?: TParams;
    }

    export namespace Overload {
      export interface Simple {
        <T extends NF, TParams extends any[] = never>(
          p: Perform<T, TParams>,
          ...params: TParams
        ): T;
        <T extends NF, TParams extends any[] = never>(
          p: Perform<T, TParams>,
          options?: Options<T, TParams>
        ): T;
      }

      export interface List {
        <T extends NF, TParams extends any[] = never>(
          options: Options<T, TParams>,
          ...p: Perform<T | T[], TParams>[]
        ): T[];
        <T extends NF, TParams extends any[] = never>(
          ...p: Perform<T | T[], TParams>[]
        ): T[];
      }
    }

    export namespace Promise {
      /**
       * @param T must not be a function
       */
      export type Sync<T extends NF, TParams extends any[] = never> = Perform<
        T | SysPromise<T>,
        TParams
      >;

      export namespace Overload {
        export interface Simple {
          <T extends NF, TParams extends any[] = never>(
            p: Perform.Promise<T, TParams>,
            ...params: TParams
          ): SysPromise<T>;
          <T extends NF, TParams extends any[] = never>(
            p: Perform.Promise<T, TParams>,
            options?: Options<T, TParams>
          ): SysPromise<T>;
        }

        export interface List {
          <T extends NF, TParams extends any[] = never>(
            options: Options<T, TParams>,
            ...p: Perform.Promise<T | T[], TParams>[]
          ): SysPromise<T[]>;
          <T extends NF, TParams extends any[] = never>(
            ...p: Perform.Promise<T | T[], TParams>[]
          ): SysPromise<T[]>;
        }
      }

      export interface Overload extends Overload.Simple {
        List: Overload.List;
      }
    }

    /**
     * @param T must not be a function
     */
    export type Promise<T extends NF, TParams extends any[] = never> =
      | Promise.Sync<T, TParams>
      | SysPromise<Perform<T, TParams>>;

    export interface Overload extends Overload.Simple {
      Options: { is: Options.Is };

      List: Overload.List;

      Promise: Promise.Overload;
    }
  }

  export namespace Code {
    export namespace Perform {
      export interface Simple {
        <T extends NF, TParams extends any[] = never>(
          p: AVStantso.Perform<T, TParams>,
          ...params: TParams
        ): T;
        <T extends NF, TParams extends any[] = never>(
          p: AVStantso.Perform<T, TParams>,
          options?: AVStantso.Perform.Options<T, TParams>
        ): T;
      }

      export interface List {
        <T extends NF, TParams extends any[] = never>(
          options: AVStantso.Perform.Options<T, TParams>,
          ...p: AVStantso.Perform<T | T[], TParams>[]
        ): T[];
        <T extends NF, TParams extends any[] = never>(
          ...p: AVStantso.Perform<T | T[], TParams>[]
        ): T[];
      }

      export namespace Promise {
        export interface Simple {
          <T extends NF, TParams extends any[] = never>(
            p: AVStantso.Perform.Promise<T, TParams>,
            ...params: TParams
          ): SysPromise<T>;
          <T extends NF, TParams extends any[] = never>(
            p: AVStantso.Perform.Promise<T, TParams>,
            options?: AVStantso.Perform.Options<T, TParams>
          ): SysPromise<T>;
        }

        export interface List {
          <T extends NF, TParams extends any[] = never>(
            options: AVStantso.Perform.Options<T, TParams>,
            ...p: AVStantso.Perform.Promise<T | T[], TParams>[]
          ): SysPromise<T[]>;
          <T extends NF, TParams extends any[] = never>(
            ...p: AVStantso.Perform.Promise<T | T[], TParams>[]
          ): SysPromise<T[]>;
        }
      }

      /**
       * @summary `AVStantso.Perform.Promise` utility
       */
      export interface Promise extends Promise.Simple {
        List: Promise.List;
      }
    }

    /**
     * @summary `AVStantso.Perform` utility
     */
    export interface Perform extends Perform.Simple {
      Options: { is: AVStantso.Perform.Options.Is };

      List: Perform.List;

      /**
       * @summary `AVStantso.Perform.Promise` utility
       */
      Promise: Perform.Promise;
    }
  }

  export interface Code {
    /**
     * @summary `AVStantso.Perform` utility
     */
    Perform: Code.Perform;
  }

  export const Perform = avstantso._reg('Perform', ({ JS, Extend }) => {
    const isOptions: Perform.Options.Is = <
      T extends NF,
      TParams extends any[] = never
    >(
      candidate: unknown
    ): candidate is Perform.Options<T, TParams> =>
      null !== candidate &&
      JS.is.object(candidate) &&
      ('afterCallback' in candidate || 'params' in candidate);

    function parseSimpleOptions<T extends NF, TParams extends any[]>(
      inParams: any[]
    ): Perform.Options<T, TParams> {
      return (
        isOptions<T, TParams>(inParams[0])
          ? { params: [], ...inParams[0] }
          : { params: inParams }
      ) as Perform.Options<T, TParams>;
    }

    const perform: Perform.Overload.Simple = <
      T extends NF,
      TParams extends any[] = never
    >(
      p: Perform<T, TParams>,
      ...inParams: any[]
    ): T => {
      const { afterCallback, params } = parseSimpleOptions<T, TParams>(
        inParams
      );

      const r: T = JS.is.function(p) ? p(...params) : p;

      afterCallback && afterCallback(r);

      return r;
    };

    const performPromise: Perform.Promise.Overload.Simple = async <
      T extends NF,
      TParams extends any[] = never
    >(
      p: Perform.Promise<T, TParams>,
      ...inParams: any[]
    ): Promise<T> => {
      const pR = await Promise.resolve(p);

      const pP = perform(pR, ...inParams);

      return Promise.resolve(pP);
    };

    function parseListOptions<T extends NF, TParams extends any[], TPerform>(
      inParams: any[]
    ): [Perform.Options<T | T[], TParams>, ...TPerform[]] {
      const options: Perform.Options<T | T[], TParams> = isOptions<
        T | T[],
        TParams
      >(inParams[0])
        ? inParams.shift()
        : undefined;

      const params: TPerform[] = Array.isArray(inParams[0])
        ? inParams[0]
        : inParams;

      return [options, ...params];
    }

    const performList: Perform.Overload.List = <
      T extends NF,
      TParams extends any[] = never
    >(
      ...inParams: any[]
    ): T[] => {
      const [options, ...items] = parseListOptions<
        T,
        TParams,
        Perform<T | T[], TParams>
      >(inParams);

      const r: T[] = [];
      for (let i = 0; i < items.length; i++) {
        const item = perform(items[i], options);
        Array.isArray(item) ? r.push(...item) : r.push(item);
      }

      return r;
    };

    const performPromiseList: Perform.Promise.Overload.List = async <
      T extends NF,
      TParams extends any[] = never
    >(
      ...inParams: any[]
    ): Promise<T[]> => {
      const [options, ...items] = parseListOptions<
        T,
        TParams,
        Perform.Promise<T | T[], TParams>
      >(inParams);

      const r: T[] = [];
      for (let i = 0; i < items.length; i++) {
        const item = await performPromise(items[i], options);
        Array.isArray(item) ? r.push(...item) : r.push(item);
      }

      return r;
    };

    const Perform: Perform.Overload = Extend(perform);
    Perform.Options = { is: isOptions };
    Perform.List = performList;

    Perform.Promise = Extend(performPromise);
    Perform.Promise.List = performPromiseList;

    return Perform;
  });
}
