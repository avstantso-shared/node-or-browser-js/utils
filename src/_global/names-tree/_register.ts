namespace AVStantso {
  const BaseKinds = (() => {
    type M = NamesTree.Kinds.Method;

    const name: M = (path) => path.peek();
    const path: M = (path) => path.join('/');
    const url: M = (path) => `/${path.join('/')}`;
    const dot: M = (path) => path.join('.');

    return { name, path, url, i18n: dot, domain: dot };
  })();

  export namespace NamesTree {
    export namespace Kinds {
      export type Method = (path: string[], options?: Options) => string;

      /**
       * @summary List of all allow tree kinds
       */
      export type All = TS.Array.FromKey<keyof Kinds>;
    }

    type BaseKinds = typeof BaseKinds;

    export interface Kinds extends BaseKinds {}

    /**
     * @summary Allow tree kind
     */
    export type Kind = TS.Array.KeyFrom<Kinds.All>;

    //#region Source
    /**
     * @summary Tree node data source
     */
    export type Source = Source.Root | string;

    export namespace Source {
      /**
       * @summary Tree root node data source
       */
      export type Root = object | Function;
    }
    //#endregion

    //#region Node
    type _NodeKinds<
      KDs extends Kind[],
      R = {},
      I extends number = 0
    > = TS.Array.IfEnd<KDs, I> extends true
      ? { [K in keyof R]: R[K] }
      : _NodeKinds<KDs, R & { [K in `_${KDs[I]}`]: string }, TS.Increment<I>>;

    /**
     * @summary Tree node for source `T` with allow kinds `KDs`
     *
     * If `S = true`, allows tails nodes as string
     */
    export type Node<
      T extends Source = Source,
      KDs extends Kind[] = Kinds.All,
      S extends boolean = false
    > = (S extends true ? unknown : _NodeKinds<KDs>) & Node.Data<T, KDs, S>;

    export namespace Node {
      /**
       * @summary Tree node data part for source `T` with allow kinds `KDs`
       *
       * If `S = true`, allows tails nodes as string
       */
      export type Data<
        T extends Source = Source,
        KDs extends Kind[] = Kinds.All,
        S extends boolean = false
      > = T extends Source.Root
        ? (T extends Function ? T : unknown) & {
            [K in keyof T]: Node<Extract<T[K], Source>, KDs, S>;
          }
        : S extends true
        ? T
        : unknown;
    }
    //#endregion

    /**
     * @summary Tree options
     */
    export interface Options {
      prefix?: string;
    }

    /**
     * @summary `NamesTree` kinds presets
     */
    export namespace Presets {
      /**
       * @summary `Names` preset
       */
      export namespace Names {
        /**
         * @summary `Names` preset kinds
         */
        export type Kinds = ['name'];

        /**
         * @summary `Names` preset tree node for source `T`
         *
         * If `S = true`, allows tails nodes as string
         */
        export type Node<
          T extends Source = Source,
          S extends boolean = false
        > = NamesTree.Node<T, Kinds, S>;

        /**
         * @summary `Names` preset `NamesTree` for data `T`
         *
         * If `S = true`, allows tails nodes as string
         */
        export type Tree<
          T extends NamesTree.Source.Root,
          S extends boolean = false
        > = AVStantso.NamesTree<T, Kinds, S>;
      }

      /**
       * @summary `I18ns` preset
       */
      export namespace I18ns {
        /**
         * @summary `I18ns` preset kinds
         */
        export type Kinds = ['i18n', 'name'];

        /**
         * @summary `I18ns` preset tree node for source `T`
         *
         * If `S = true`, allows tails nodes as string
         */
        export type Node<
          T extends Source = Source,
          S extends boolean = false
        > = NamesTree.Node<T, Kinds, S>;

        /**
         * @summary `I18ns` preset `NamesTree` for data `T`
         *
         * If `S = true`, allows tails nodes as string
         */
        export type Tree<
          T extends NamesTree.Source.Root,
          S extends boolean = false
        > = AVStantso.NamesTree<T, Kinds, S>;
      }

      /**
       * @summary `Urls` preset
       */
      export namespace Urls {
        /**
         * @summary `Urls` preset kinds
         */
        export type Kinds = ['name', 'path', 'url'];

        /**
         * @summary `Urls` preset tree node for source `T`
         *
         * If `S = true`, allows tails nodes as string
         */
        export type Node<
          T extends Source = Source,
          S extends boolean = false
        > = NamesTree.Node<T, Kinds, S>;

        /**
         * @summary `Urls` preset `NamesTree` for data `T`
         *
         * If `S = true`, allows tails nodes as string
         */
        export type Tree<
          T extends NamesTree.Source.Root,
          S extends boolean = false
        > = AVStantso.NamesTree<T, Kinds, S>;
      }
    }
  }

  namespace _NamesTree {
    //#region Splitted
    type _Splitted<
      T extends NamesTree.Source.Root,
      KDs extends NamesTree.Kind[],
      R extends any[] = [],
      I extends number = 0
    > = TS.Array.IfEnd<KDs, I> extends true
      ? R
      : _Splitted<
          T,
          KDs,
          [...R, NamesTree<T, [KDs[I]], true>],
          TS.Increment<I>
        >;

    export type Splitted<
      T extends NamesTree.Source.Root,
      KDs extends NamesTree.Kind[]
    > = _Splitted<T, KDs>;
    //#endregion

    export type ExtractByKinds<
      T extends NamesTree.Source.Root,
      KDs extends NamesTree.Kind[],
      R = {},
      I extends number = 0
    > = TS.Array.IfEnd<KDs, I> extends true
      ? R
      : ExtractByKinds<
          T,
          KDs,
          R & { [K in Capitalize<KDs[I]>]: NamesTree<T, [KDs[I]], true> },
          TS.Increment<I>
        >;
  }

  /**
   * @summary Names tree for data `T` with allow kinds `KDs`
   *
   * If `S = true`, allows tails nodes as string
   */
  export type NamesTree<
    T extends NamesTree.Source.Root,
    KDs extends NamesTree.Kind[],
    S extends boolean = false
  > = NamesTree.Node.Data<T, KDs, S> &
    (S extends true
      ? {}
      : {
          /**
           * @summary Get array of splitted trees each with unique kind
           */
          splitted: _NamesTree.Splitted<T, KDs>;

          /**
           * @summary Modify existing tree by additional data
           */
          merge<M extends NamesTree.Source.Root>(
            data: M
          ): NamesTree<T & M, KDs, S>;
        } & _NamesTree.ExtractByKinds<T, KDs>);

  export namespace Code {
    /**
     * @summary `AVStantso.NamesTree` utility
     */
    export interface NamesTree {
      <
        T extends AVStantso.NamesTree.Source.Root,
        K0 extends AVStantso.NamesTree.Kind,
        K1 extends Exclude<AVStantso.NamesTree.Kind, K0> = undefined,
        K2 extends Exclude<AVStantso.NamesTree.Kind, K0 | K1> = undefined,
        K3 extends Exclude<AVStantso.NamesTree.Kind, K0 | K1 | K2> = undefined,
        K4 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3
        > = undefined,
        K5 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3 | K4
        > = undefined,
        K6 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3 | K4 | K5
        > = undefined,
        K7 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3 | K4 | K5 | K6
        > = undefined,
        K8 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3 | K4 | K5 | K6 | K7
        > = undefined,
        K9 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3 | K4 | K5 | K6 | K7 | K8
        > = undefined
      >(
        data: T,
        options: AVStantso.NamesTree.Options,
        kind0: K0,
        kind1?: K1,
        kind2?: K2,
        kind3?: K3,
        kind4?: K4,
        kind5?: K5,
        kind6?: K6,
        kind7?: K7,
        kind8?: K8,
        kind9?: K9
      ): AVStantso.NamesTree<
        T,
        AVStantso.TS.Array.FilterUnique<
          [K0, K1, K2, K3, K4, K5, K6, K7, K8, K9]
        >
      >;

      <
        T extends AVStantso.NamesTree.Source.Root,
        K0 extends AVStantso.NamesTree.Kind,
        K1 extends Exclude<AVStantso.NamesTree.Kind, K0> = undefined,
        K2 extends Exclude<AVStantso.NamesTree.Kind, K0 | K1> = undefined,
        K3 extends Exclude<AVStantso.NamesTree.Kind, K0 | K1 | K2> = undefined,
        K4 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3
        > = undefined,
        K5 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3 | K4
        > = undefined,
        K6 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3 | K4 | K5
        > = undefined,
        K7 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3 | K4 | K5 | K6
        > = undefined,
        K8 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3 | K4 | K5 | K6 | K7
        > = undefined,
        K9 extends Exclude<
          AVStantso.NamesTree.Kind,
          K0 | K1 | K2 | K3 | K4 | K5 | K6 | K7 | K8
        > = undefined
      >(
        data: T,
        kind0: K0,
        kind1?: K1,
        kind2?: K2,
        kind3?: K3,
        kind4?: K4,
        kind5?: K5,
        kind6?: K6,
        kind7?: K7,
        kind8?: K8,
        kind9?: K9
      ): AVStantso.NamesTree<
        T,
        AVStantso.TS.Array.FilterUnique<
          [K0, K1, K2, K3, K4, K5, K6, K7, K8, K9]
        >
      >;

      /**
       * @summary Register external `NamesTree` kinds
       */
      _RegKinds(kinds: NodeJS.Dict<AVStantso.NamesTree.Kinds.Method>): void;

      /**
       * @summary Make `Name` `NamesTree`
       */
      Names<T extends AVStantso.NamesTree.Source.Root>(
        data: T,
        options?: AVStantso.NamesTree.Options
      ): AVStantso.NamesTree.Presets.Names.Tree<T>;

      /**
       * @summary Make `i18n` `NamesTree`
       */
      I18ns<T extends AVStantso.NamesTree.Source.Root>(
        data: T,
        options?: AVStantso.NamesTree.Options
      ): AVStantso.NamesTree.Presets.I18ns.Tree<T>;

      /**
       * @summary Make `urls` `NamesTree`
       */
      Urls<T extends AVStantso.NamesTree.Source.Root>(
        data: T,
        options?: AVStantso.NamesTree.Options
      ): AVStantso.NamesTree.Presets.Urls.Tree<T>;
    }
  }

  export interface Code {
    /**
     * @summary `AVStantso.NamesTree` utility
     */
    NamesTree: Code.NamesTree;
  }

  export const NamesTree = avstantso._reg('NamesTree', ({ JS }) => {
    const kindsMethods: NodeJS.Dict<AVStantso.NamesTree.Kinds.Method> = {
      ...BaseKinds,
    };

    const _RegKinds: Code.NamesTree['_RegKinds'] = (kinds) =>
      JS.patch(kindsMethods, kinds);

    type PathMethods = Record<NamesTree.Kind, () => string>;
    function PathMethods(
      path: string[],
      options?: NamesTree.Options
    ): PathMethods {
      const { prefix = '' } = options || {};

      return Object.entries(kindsMethods).reduce(
        (r, [k, v]) => JS.set.raw(r, k, () => `${prefix}${v(path, options)}`),
        {} as PathMethods
      );
    }

    type NDContext = {
      strTail: boolean;
      kinds: NamesTree.Kind[];
      options: NamesTree.Options;

      dest: NamesTree.Node.Data;
      data: NamesTree.Source;
      path?: string[];
    };
    function NodeData(context: NDContext): NamesTree.Node.Data {
      const { strTail, kinds, options, dest, data, path = [] } = context;

      if (!JS.is.string(data)) {
        Object.entries(data)
          .filter(([k]) => 'toString' !== k)
          .forEach(([k, v]) => {
            const oldDs = JS.get.raw(dest, k);

            const fds = JS.is.function(oldDs);
            const fv = JS.is.function(v);

            if (fds && fv) {
              console.log(`%O`, { oldDs, v, fds, fv, data });

              throw Error(
                `Can't merge NamesTrees functions nodes at "${path.join('.')}"`
              );
            }

            const needReMake = oldDs && !fds && fv;

            const subPath = [...path, k];

            const methods = PathMethods(subPath, options);

            const ds =
              oldDs && !needReMake
                ? oldDs
                : fv
                ? (...args: any[]) => v(...args)
                : strTail && JS.is.string(v)
                ? methods[kinds[0]]()
                : {};

            if (needReMake) JS.patch(ds, oldDs);

            if (!JS.is.string(ds)) {
              ds.toString = methods[kinds[0]];

              if (!strTail)
                Object.entries(methods).forEach(([m, get]) => {
                  const p = `_${m}`;
                  if (p in ds) delete ds[p];

                  Object.defineProperty(ds, p, { get });
                });
            }

            NodeData({ ...context, dest: ds, data: v, path: subPath });

            JS.set.raw(dest, k, ds);
          });
      }

      return dest;
    }

    function MakeTree(
      data: NamesTree.Source.Root,
      strTail: boolean,
      options: NamesTree.Options,
      ...kinds: NamesTree.Kind[]
    ) {
      let subs: any = {};
      function getSub(kind: NamesTree.Kind) {
        if (!subs[kind]) subs[kind] = MakeTree(data, true, options, kind);

        return subs[kind];
      }

      const tree: any = {};

      if (!strTail) {
        Object.defineProperties(tree, {
          splitted: { get: () => kinds.map(getSub) },

          merge: {
            value: (addData: NamesTree.Source.Root) => {
              NodeData({ strTail, kinds, options, dest: tree, data: addData });
              subs = {};
              return tree;
            },
            writable: false,
          },
        });

        kinds.forEach((kind) =>
          Object.defineProperty(tree, kind.toCapitalized(), {
            get: () => getSub(kind),
          })
        );
      }

      NodeData({ strTail, kinds, options, dest: tree, data });

      return tree;
    }

    const NamesTree: Code.NamesTree = (data, ...params: any[]) => {
      const options = !JS.is.string(params[0]) ? params.shift() : undefined;

      return MakeTree(data, false, options, ...params);
    };

    NamesTree._RegKinds = _RegKinds;

    NamesTree.Names = (data, options) => NamesTree(data, options, 'name');

    NamesTree.I18ns = (data, options) =>
      NamesTree(data, options, 'i18n', 'name');

    NamesTree.Urls = (data, options) =>
      NamesTree(data, options, 'name', 'path', 'url');

    return NamesTree;
  });
}
