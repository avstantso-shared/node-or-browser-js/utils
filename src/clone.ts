import { Buffer } from 'buffer';
import { JS } from './js';

export namespace Clone {
  export type Method = (source: any) => any;
  export type Handler = (source: any, method?: Method) => () => any;
}

const copy: Clone.Method = (src) => src;

export function Clone<TResult = unknown, TSource = unknown>(
  source: TSource,
  method?: Clone.Method,
  ...handlers: Clone.Handler[]
) {
  const doClone = method || copy;

  let handler: ReturnType<Clone.Handler>;
  for (let i = 0; i < handlers.length; i++) {
    handler = handlers[i](source, doClone);
    if (handler) return handler();
  }

  return null === source || !JS.is.object(source)
    ? source
    : source instanceof Date
    ? new Date(source)
    : source instanceof Buffer
    ? Buffer.from(source)
    : JS.clone(source, doClone);
}

Clone.Deep = <TResult = unknown, TSource = unknown>(
  source: TSource,
  ...handlers: Clone.Handler[]
): TResult => {
  const recursion: Clone.Method = (src) => Clone(src, recursion, ...handlers);

  return recursion(source);
};
