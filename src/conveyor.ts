export namespace Conveyor {
  export type Transformer<T> = (from: T) => T;
}

export function conveyor<T>(
  original: T,
  ...transformers: Conveyor.Transformer<T>[]
): T {
  let r = { ...original };
  transformers.forEach((t) => (r = t ? t(r) : r));
  return r;
}
