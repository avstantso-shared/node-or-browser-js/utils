import { Clone } from './clone';

export class ValueWrap<TValue = unknown> {
  private _value: TValue;

  constructor(value: TValue) {
    this._value = value;

    Object.setPrototypeOf(this, ValueWrap.prototype);
  }

  public valueOf() {
    return this._value;
  }

  public get value() {
    return this._value;
  }

  public get debugLabel(): string {
    return Object.getPrototypeOf(this).constructor.name;
  }

  /**
   * @summary Create wrap copy. Need override in descendants
   */
  protected copy(): ValueWrap {
    return new ValueWrap(this._value);
  }

  /**
   * @summary Resolve values on cloning
   */
  public static get cloningResolve(): Clone.Handler {
    return (source: any) => source instanceof this && (() => +source);
  }

  /**
   * @summary Copy same wrap on cloning
   */
  public static get cloningSame(): Clone.Handler {
    return (source: any) => source instanceof this && (() => source);
  }

  /**
   * @summary Copy wrap on cloning
   */
  public static get cloningCopy(): Clone.Handler {
    return (source: any) => source instanceof this && (() => source.copy());
  }
}
