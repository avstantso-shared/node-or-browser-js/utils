/**
 * @summary Not `X`, but an oblique cross `❌`. It can be fed to `Promise.then` / `Promise.catch` instead of an empty value
 * @returns `undefined`
 */
export function X<T = any>(): T {
  return undefined;
}
