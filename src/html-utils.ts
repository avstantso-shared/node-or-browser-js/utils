export function toValidId(id: string) {
  return !id
    ? id
    : id
        .replace(/\//g, '--')
        .replace(/@/g, 'at-')
        .replace(/\^/g, 'hat-')
        .replace(/(^[^a-z])|([^a-z0-9_\-])/gi, 'x-')
        .replace(/-+$/, '');
}

export function noTags(html: string) {
  return !html ? html : html.replace(/\<\/?[^\>]*\>/g, '');
}
