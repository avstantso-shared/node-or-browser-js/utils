import type { TS } from './ts';
import { JS } from './js';
import { Perform } from './perform';

export namespace Override {
  export namespace TypeMap {
    export type OrT = TypeMap | TS.Type.Not<Function>;

    export type Parse<T extends OrT> = T extends TypeMap
      ? T
      : { T: T; Ext?: undefined };

    export type IsSimple<
      T extends OrT,
      IfTrue = true,
      IfFalse = false
    > = TypeMap.Parse<T>['Ext'] extends undefined ? IfTrue : IfFalse;
  }

  export type TypeMap = {
    T: TS.Type.Not.Function;
    Ext?: {};
  };

  export type Context<TTypeMapOrT extends TypeMap.OrT> = {
    original: TypeMap.Parse<TTypeMapOrT>['T'];
  } & TypeMap.IsSimple<TTypeMapOrT, {}, TypeMap.Parse<TTypeMapOrT>['Ext']>;

  export type Union<TTypeMapOrT extends TypeMap.OrT> =
    | TypeMap.Parse<TTypeMapOrT>['T']
    | Override<TTypeMapOrT>;

  export namespace Calculator {
    export type Calulate<TTypeMapOrT extends TypeMap.OrT> = (
      override?: Union<TTypeMapOrT>
    ) => TypeMap.Parse<TTypeMapOrT>['T'];
  }

  export type Calculator<TTypeMapOrT extends TypeMap.OrT> = TypeMap.IsSimple<
    TTypeMapOrT,
    (
      context?: Context<TTypeMapOrT> | TypeMap.Parse<TTypeMapOrT>['T']
    ) => Calculator.Calulate<TTypeMapOrT>,
    (context?: Context<TTypeMapOrT>) => Calculator.Calulate<TTypeMapOrT>
  >;
}

export type Override<TTypeMapOrT extends Override.TypeMap.OrT> = (
  context?: Override.Context<TTypeMapOrT>
) => Override.TypeMap.Parse<TTypeMapOrT>['T'];

function isContext<TTypeMapOrT extends Override.TypeMap.OrT = Override.TypeMap>(
  candidate: unknown
): candidate is Override.Context<TTypeMapOrT> {
  return (
    null !== candidate && JS.is.object(candidate) && 'original' in candidate
  );
}

function Calculator<
  TTypeMapOrT extends Override.TypeMap.OrT,
  TContextOrOriginal extends
    | Override.Context<TTypeMapOrT>
    | Override.TypeMap.Parse<TTypeMapOrT>['T']
>(context?: TContextOrOriginal) {
  type EndResult = TContextOrOriginal extends { original: infer R }
    ? R
    : TContextOrOriginal;

  const c: Override.Context<TTypeMapOrT> = isContext<TTypeMapOrT>(context)
    ? context
    : { original: context };

  return (override: Override.Union<TTypeMapOrT>): EndResult => {
    const r: any = undefined === override ? c.original : Perform(override, c);

    return r;
  };
}

export const Override = {
  isContext,
  Calculator,
};
