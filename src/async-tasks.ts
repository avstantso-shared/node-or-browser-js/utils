export function AsyncTasks<B = unknown>(...initial: Promise<B>[]) {
  const tasks: Promise<B>[] = [];

  function add<T extends B = B>(task: Promise<T>): Promise<T> {
    const t = Promise.resolve(task).finally(() => {
      const i = tasks.indexOf(t);
      if (i >= 0) tasks.splice(i, 1);
    });

    tasks.push(t);

    return t;
  }

  async function end(): Promise<void> {
    await Promise.all(tasks);
  }

  if (initial?.length) initial.forEach(add);

  return {
    get length() {
      return tasks.length;
    },
    get tasks() {
      return [...tasks];
    },
    add,
    end,
  };
}
