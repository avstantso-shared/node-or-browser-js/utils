import { Buffer } from 'buffer';
import { ValueWrap } from './value-wrap';

import AtomicObjects = AVStantso.AtomicObjects;

export { AtomicObjects };

AtomicObjects.register(Date);
AtomicObjects.register(Buffer);
AtomicObjects.register(ValueWrap);
