import { Generics } from './generics';

/**
 * @summary Remove $createdAt and $createdBy from data
 */
export type ExtractGeneratedData<T extends {}> = Omit<
  T,
  '$createdAt' | '$createdBy'
>;

/**
 * @summary Remove $createdAt and $createdBy from data
 */
export function extractGeneratedData<T extends {}>(
  data: T
): ExtractGeneratedData<T> {
  const { $createdAt, $createdBy, ...rest } = Generics.Cast(data);
  return rest;
}

export function ExtractGeneratedData<TEx extends {}>(template?: TEx) {
  return extractGeneratedData as <T extends {}>(
    data: T
  ) => ExtractGeneratedData<T> & TEx;
}
