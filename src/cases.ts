import { JS } from './js';
import { TS } from './ts';

const cases = TS.Type.Definer().Arr('camel', 'pascal', 'snake', 'kebab');

export namespace Cases {
  export type Func = (str: string) => string;

  export type Key = TS.Array.KeyFrom<typeof cases>;

  export type Union = Cases.Key | Cases.Func;

  export type Resolve = (caseUnion: Union) => Cases.Func;
}

export type Cases = Record<Cases.Key, Cases.Func> & { Resolve: Cases.Resolve };

type ChangeEachWord = (word: string, index?: number) => string;

function Make(changeEachWord: ChangeEachWord, seporator = ''): Cases.Func {
  return (str) => {
    if (!str) return str;

    // iOS not supports my regex: (?<=...)
    // /^[a-z]|[A-Z]|(?<=[^A-Za-z0-9])[a-z0-9]/g,

    const words = Array.from(str.matchAll(/[A-Z]?[a-z0-9]*/g), (m) => m[0]);

    let r: string;
    for (let i = 0; i < words.length; i++) {
      const w = words[i];
      if (w) r = (!r ? '' : r + seporator) + changeEachWord(w, i);
    }

    return r || '';
  };
}

const cewCap: ChangeEachWord = (word) => word.toCapitalized();
const cewUncap: ChangeEachWord = (word) => word.toUncapitalized();

export const Cases: Cases = {
  camel: Make((word, index) => (!index ? cewUncap(word) : cewCap(word))),
  pascal: Make(cewCap),
  snake: Make(cewUncap, '_'),
  kebab: Make(cewUncap, '-'),

  Resolve: (caseUnion) =>
    !caseUnion
      ? (caseUnion as Cases.Func)
      : JS.is.function(caseUnion)
      ? caseUnion
      : Cases[caseUnion],
};
