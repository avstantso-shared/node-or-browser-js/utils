import type { TS } from './ts';
import { JS } from './js';
import { Extend } from './extend';
import { Perform } from './perform';

type NF = TS.Type.Not.Function;

export namespace Transition {
  /**
   * @summary This type able passed into `Transition` return generics
   */
  export type Able = NF;

  export type SetState<TState> = (newState: TState) => unknown;

  export type Props<TState> = {
    state: Transition.SetState<TState>;
    begin?: TState;
    end: TState;
  };

  export type Execute<TDefaultReturn extends NF = NF> = <
    TReturn extends NF = TDefaultReturn
  >(
    action: Perform.Promise.Sync<TReturn>
  ) => Promise<TReturn>;

  export type Overload = {
    /**
     * @summary Create transition
     * @returns Transition object
     */
    <TState, TDefaultReturn extends NF = NF>(
      state: SetState<TState>,
      end: TState
    ): Transition<TDefaultReturn>;

    /**
     * @summary Create transition
     * @returns Transition object
     */
    <TState, TDefaultReturn extends NF = NF>(
      state: SetState<TState>,
      begin: TState,
      end: TState
    ): Transition<TDefaultReturn>;

    /**
     * @summary Create transition
     * @returns Transition object
     */
    <TState, TDefaultReturn extends NF = NF>(
      props: Props<TState>
    ): Transition<TDefaultReturn>;

    /**
     * @summary Set "state" to "begin" for all transitions -> await "action" -> "state" to "end" for all transitions
     * @returns action awaited result
     */
    Stack: Stack;
  };

  export type Stack = {
    /**
     * @summary Set "state" to "begin" for all transitions -> await "action" -> "state" to "end" for all transitions
     * @returns action awaited result
     */
    <TDefaultReturn extends NF = NF>(
      ...transitions: Transition[]
    ): Transition.Execute<TDefaultReturn>;
  };
}

export type Transition<TDefaultReturn extends NF = NF> =
  Transition.Execute<TDefaultReturn>;

function transition<TDefaultReturn extends NF = NF>(
  ...params: any[]
): Transition<TDefaultReturn> {
  const props: Transition.Props<any> = JS.is.object<Transition.Props<any>>(
    params[0]
  )
    ? params[0]
    : {
        state: params[0],
        end: params[params.length - 1],
        ...(params.length > 2 ? { begin: params[1] } : {}),
      };

  const hasBegin = props.hasOwnProperty('begin');
  const { state, begin, end } = props;

  return async <TReturn extends NF = TDefaultReturn>(
    action: Perform.Promise.Sync<TReturn>
  ): Promise<TReturn> => {
    let r: TReturn;

    if (hasBegin) {
      state(begin);
      // console.log(`${state.name}: set %O`, begin);
    }
    try {
      r = await Perform.Promise(action);

      // console.log(`${state.name} action %O awaited`, action);
    } finally {
      state(end);
      // console.log(`${state.name}: set %O`, end);
    }

    return r;
  };
}

export const transitionStack: Transition.Stack = <
  TDefaultReturn extends NF = NF
>(
  ...transitions: Transition[]
): Transition.Execute<TDefaultReturn> => {
  return <TReturn extends NF = TDefaultReturn>(
    action: Perform.Promise.Sync<TReturn>
  ) => {
    function exec(i: number): Promise<TReturn> {
      const next = transitions.length - 1 === i ? action : () => exec(i + 1);

      const execute = transitions[i];

      return execute(next);
    }

    return exec(0);
  };
};

/**
 * @summary Create transition
 * @returns Transition object
 */
export const Transition: Transition.Overload = Extend(transition);
Transition.Stack = transitionStack;
