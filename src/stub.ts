import { Generic, Generics } from './generics';
import { JS } from './js';
import { X } from './X';

export namespace Stub {
  export type One = Overload<false>;

  export type Overload<TAllowMultiple extends boolean = true> = {
    /**
     * @summary Stub function to prevent call undefined
     * @param func Original function for call
     * @returns func or stub
     */
    <TFunc extends Function>(func: TFunc): TFunc;
  } & (TAllowMultiple extends true
    ? {
        /**
         * @summary Stub functions to prevent call undefined
         * @param ...funcs Original functions for call
         * @returns (func or stub)[]
         */
        <TFuncs extends Function[]>(...funcs: TFuncs): TFuncs;
      }
    : {});
}

export type Stub = Stub.Overload & {
  /**
   * @summary Stub by custom function
   */
  Custom(stub: Stub.One): Stub.Overload;

  /**
   * @summary Stub any props of `source` as empty function or `undefied` non function if not `condition`
   */
  Props<T extends NodeJS.Dict<any>>(source?: T, condition?: boolean): T;
};

function customStub<T extends Stub.Overload>(stub: Stub.One): T {
  return Generics.Cast.To((...params: any[]): any =>
    1 === params.length ? stub(params[0]) : params.map(stub)
  );
}

export const Stub: Stub = customStub((func) => func || Generics.Cast.To(X));

Stub.Custom = customStub;

Stub.Props = (source?, condition?) =>
  new Proxy(source, {
    get: (target, p) => {
      if (!(p in target)) return undefined;

      const src = JS.get.raw(target, p);

      return condition ? src : JS.is.function(src) ? X : undefined;
    },
  });
