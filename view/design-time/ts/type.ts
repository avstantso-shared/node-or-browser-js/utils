import { TS } from '../../../src';

type t = TS.Type.Union<number>;

type un = TS.Type.Union<never>; // never
type u0 = TS.Type.Union<string | 'bigint'>; // string
type u1 = TS.Type.Union<number, 'string' | 'bigint'>; // number | "string" | "bigint"
type u2 = TS.Type.Union<number, TS.Type.Not<'string' | 'bigint'>>; // number | "number" | "boolean" | "symbol" | "undefined" | "object" | "function"
type u3 = TS.Type.Union<{ T: Function; L: 'boolean' }>; // "boolean" | Function
type u4 = TS.Type.Union<object>; // object | "string" | "number" | "bigint" | "boolean" | "symbol" | "undefined" | "object" | "function"

type rn = TS.Type.Resolve<never>; // never
type r0 = TS.Type.Resolve<string | 'function'>; // string
type r1 = TS.Type.Resolve<'bigint' | 'string'>; // string | bigint
type r2 = TS.Type.Resolve<bigint>; // bigint
type r3 = TS.Type.Resolve<'bigint' | 'string' | boolean>; // string | bigint | boolean
type r4 = TS.Type.Resolve<{ T: Function; L: 'boolean' }>; // boolean | Function
type r5 = TS.Type.Resolve<{}>; // {}
type r6 = TS.Type.Resolve<{ T: string; L: 'string' }>; // string
type r7 = TS.Type.Resolve<{ T: string; L: 'function' }>; // string | Function
type r8 = TS.Type.Resolve<{ L: 'function' }>; // Function
type r9 = TS.Type.Resolve<{ T: Function }>; // Function
type r10 = TS.Type.Resolve<{ L: 'Date' }>; // Date
type r11 = TS.Type.Resolve<{ L: 'Buffer' }>; // Buffer

type r_u0 = TS.Type.Resolve<u0>;
type r_u1 = TS.Type.Resolve<u1>;
type r_u2 = TS.Type.Resolve<u2>;
type r_u3 = TS.Type.Resolve<u3>;
type r_u4 = TS.Type.Resolve<u4>;

type n0 = TS.Type.Not<boolean | 'boolean'>; // never
type n1 = TS.Type.Not<string>; // number | bigint | boolean | symbol | object | Function
type n2 = TS.Type.Not<string | 'boolean'>; // number | bigint | boolean | symbol | object | Function
type n3 = TS.Type.Not<'boolean' | 'string'>; //"number" | "bigint" | "symbol" | "undefined" | "object" | "function"
