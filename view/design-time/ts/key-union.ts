import { TS } from '../../../src';

// RangeError: Maximum call stack size exceeded
// in tsc.js:51744
// on compilation

// @ts-ignore Type instantiation is excessively deep and possibly infinite.
type TestUnion<
  N extends number,
  L extends any[] = TS.Array.Letters<N>
> = TS.Key.Union<TS.Array.KeyFrom<L>, L>;

type l16 = TS.Array.Letters<16>;

type a00 = TestUnion<0>;
type a01 = TestUnion<1>;
type a02 = TestUnion<2>;
type a03 = TestUnion<3>;
type a04 = TestUnion<4>;
type a05 = TestUnion<5>;
type a06 = TestUnion<6>;
type a07 = TestUnion<7>;
type a08 = TestUnion<8>;
type a09 = TestUnion<9>;
type a10 = TestUnion<10>;
type a11 = TestUnion<11>;
type a12 = TestUnion<12>;
type a13 = TestUnion<13>;
type a14 = TestUnion<14>;
type a15 = TestUnion<15>;

// @ts-ignore Type instantiation is excessively deep and possibly infinite.
type a16_max = TS.Key.Union<
  | 'A'
  | 'B'
  | 'C'
  | 'D'
  | 'E'
  | 'F'
  | 'G'
  | 'H'
  | 'I'
  | 'J'
  | 'K'
  | 'L'
  | 'M'
  | 'N'
  | 'O'
  | 'P',
  [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P'
  ]
>;

type a17_exceeded = TestUnion<17>;
