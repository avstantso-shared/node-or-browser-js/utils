const path = require('path');
const fs = require('fs');
const { execSync } = require('child_process');

const { writeSourceFile } = require('@avstantso/node-js--code-gen');

const scriptName = 'ts-numeric-gen';

const [limit, replaceIfSmall, gitAssumeUnchanged] = (() => {
  const argv = process.argv.slice(2);
  const l = argv.indexOf('-l');
  return [
    l < 0 ? 0 : argv[l + 1],
    argv.indexOf('-r') >= 0,
    argv.indexOf('-g') >= 0,
  ];
})();

const resultFile = path.resolve(
  __dirname,
  `../src/_global/ts/numeric/domain/generated.ts`
);

const stats = fs.existsSync(resultFile) ? fs.statSync(resultFile) : undefined;
if (stats && stats?.size > 512 && replaceIfSmall) return;

async function main() {
  const arr = Array.from({ length: limit }, (v, i) => i + 1);

  const pw = {};
  for (let x = 2; x <= 10; x++) {
    const xpw = {};

    for (let e = 2, p = Math.pow(x, e); p < limit; e++, p = Math.pow(x, e))
      xpw[e] = p;

    if (!Object.keys(xpw).length) break;

    pw[x] = xpw;
  }

  const template = [
    (!limit
      ? [``, `This is stub file. Use`, `yarn ${scriptName}`, `or`, `yarn build`]
      : []
    ).join('\r\n// '),

    `namespace AVStantso.TS.Numeric.Domain {`,
    `  export type Positive = [${arr.join(',')}]\r\n`,
    `  export type Negative = [${arr.map((n) => `-${n}`).join(',')}]\r\n`,
    `  export type Transformation = {${arr
      .map((n) => `'${n}': {p: ${n}; n: -${n};}`)
      .join(';')}}\r\n`,
    `  export type Power = ${JSON.stringify(pw)}}`,
  ].join('\r\n');

  await writeSourceFile(resultFile, template, scriptName);

  if (gitAssumeUnchanged)
    execSync(`git update-index --assume-unchanged ${resultFile}`, {
      shell: true,
    });

  console.log('\x1b[32mSuccess!\x1b[0m');
}

main();
