const path = require('path');
const { writeSourceFile } = require('@avstantso/node-js--code-gen');

const scriptName = 'ts-extends-table-gen';

const resultFile = path.resolve(
  __dirname,
  `../view/design-time/ts/extends-table.ts`
);

async function main() {
  const types = [
    'never',
    'any',
    'unknown',

    'undefined',
    'null',

    'string',
    'number',
    'bigint',
    'boolean',
    'symbol',
    'object',
    'Function',
  ];

  const source = types
    .map((from) =>
      types
        .map(
          (to) => `type ${from}_${to} = ${from} extends ${to} ? true : false;`
        )
        .join(`\r\n`)
    )
    .join(`\r\n\r\n`);

  await writeSourceFile(resultFile, source, scriptName);

  console.log('\x1b[32mSuccess!\x1b[0m');
}

main();
